import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:basic_utils/basic_utils.dart';
import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart';
import 'package:dart_des/dart_des.dart';
import 'package:encrypt/encrypt.dart';
import 'package:fast_gbk/fast_gbk.dart';
import 'package:flutter_js/flutter_js.dart';
import 'package:flutter_js/javascriptcore/flutter_jscore.dart';
import 'package:flutter_js/javascriptcore/jscore_runtime.dart';
import 'package:qn_read_rule/book_provide/base.dart';
import 'package:qn_read_rule/book_provide/base_cache.dart';
import 'package:qn_read_rule/book_provide/base_model.dart';
import 'package:qn_read_rule/utils/NeworkHelper.dart';
import 'package:qn_read_rule/utils/b_utils.dart';
import 'package:qn_read_rule/utils/hash_m.dart';
import 'package:qn_read_rule/utils/logger.dart';
import 'package:qn_read_rule/utils/qn_utils.dart';

class GDataTagM {
  dynamic data;
  List<String>? splitList;
}

class GDataTagCache {
  static int key = 10000;

  static final Map<int, GDataTagM> dataTagCache = {};

  static int addData(dynamic data) {
    key = key + 1;
    final m = GDataTagM();
    m.data = data;
    dataTagCache[key] = m;
    return key;
  }

  static dynamic getData(dynamic key) {
    if (key == null) {
      return null;
    }
    if (key is int) {
      return dataTagCache[key]?.data;
    } else {
      final kk = key.toString();
      if (kk.contains('-')) {
        var split = kk.split('-');
        return dataTagCache[int.parse(split[0])]
            ?.splitList?[int.parse(split[1])];
      } else {
        return dataTagCache[int.parse(kk)]?.data;
      }
    }
  }

  static void addSplitData(List<String> splitList, dynamic key) {
    Logger.debug(() => "dynamic key --> $key");
    if (key is int) {
      dataTagCache[key]?.splitList = splitList;
    } else {
      dataTagCache[int.parse(key)]?.splitList = splitList;
    }
  }
}

class GlobalNativeProxy {
  /// 异步方法集合
  static final Map<String, dynamic> asyncHandlerMap = {
    // 网络请求
    // argsMap 为 模型对象
    //  { url: '' , method : '', 'requestData':'',}
    // 包含如下字段：
    // url 字符串，请求地址
    // method，简写 d  方法支持get/post/delete/header/put等等
    // data，简写 d 请求字符串，请自行拼接 form表单 或者 json字符串 获取Map
    // headers, 简写 h，对象，{'xx': 'yy','zz':'cc'}格式，请求头
    // cacheTime 数据缓存时间,单位为毫秒
    // ext 扩展对象，支持以下字段：
    //      webview 只有为 1 或者 '1'时，才为webview请求，默认为空
    //      webviewTime,指定webview请求下的等待时 webview为开启（为1）时生效，单位为毫秒，默认500毫秒
    //      needCookies,是否需要Cookies注入，默认为1，如无需cookies，手动设置为0
    //      needRspHeader,是否需要响应头，默认为0，如需RspHeaders，手动设置为1
    //      needRealData,是否需要真实的响应内容，在JS中调用默认为0，其余默认为1
    // 返回数据： Response对象，包含 header/ statusCode/ data / dataTag
    "network": (dynamic map) async {
      var url = map['url'];
      var method = map['m'] ?? map['method'];
      Map<String, String> headers = {};
      var requestData = map['d'] ?? map['requestData'];
      if (method == 'post' || method == 'POST') {
        if (requestData != null && requestData is Map) {
          headers['Content-Type'] = 'application/json';
          requestData = jsonEncode(requestData);
        } else if (requestData.startsWith('{') || requestData.startsWith('[')) {
          headers['Content-Type'] = 'application/json';
        } else {
          headers['Content-Type'] = 'application/x-www-form-urlencoded';
        }
      }
      final mHeaders = map['h'] ?? map['headers'];
      if (mHeaders != null && mHeaders is Map) {
        for (MapEntry entry in mHeaders.entries) {
          headers[entry.key.toString()] = entry.value.toString();
        }
      }
      var responseCharset = map['rCharset'] ?? map['responceCharset'];
      final ext = map['ext'] ?? {};
      var webview = ext['webview'];
      var waitTime = ext['webviewTime'];
      var needCookies = ext['needCookies'] ?? 1;
      var cacheTime = map['cacheTime'] ?? 0;
      var response = await NetworkHelper.excutorRequestWithResponse(url,
          method: method,
          headers: headers,
          requestData: requestData,
          responseCharset: responseCharset,
          isWebView: webview == 1,
          waitTime: waitTime,
          needCookies: needCookies == 1,
          cacheTime: cacheTime);
      // if (response == null) {
      //   return {
      //     'statusCode': -99,
      //   };
      // }
      // final Map<String, dynamic> resultData;
      final dynamic data;
      if (responseCharset == null ||
          responseCharset.toString().isEmpty ||
          responseCharset == 'utf8' ||
          responseCharset == 'utf-8') {
        data = utf8.decode(response.data ?? [], allowMalformed: true);
      } else if (responseCharset == 'gbk') {
        data = gbk.decode(response.data ?? [], allowMalformed: true);
      } else {
        data = response.data;
      }
      final resultData = {};
      final needResponseHeader = ext['needRspHeader'] == 1;
      if (needResponseHeader) {
        resultData['headers'] = response.headers.map;
      }
      final needDataTag = ext['needDataTag'] == 1;
      if (needDataTag) {
        resultData['dataTag'] = GDataTagCache.addData(data);
      } else {
        resultData['data'] = data;
      }
      return resultData;
    },
    "networkStr": (dynamic map) async {
      final url = map['url'];
      var indexOf = url.indexOf(',');
      if (indexOf == -1) {
        return await asyncHandlerMap['network']!({'url': url});
      } else {
        var extraJson = url.substring(indexOf + 1);
        var jsonMap = json.decode(extraJson);
        jsonMap['url'] = url.substring(0, indexOf);
        return await asyncHandlerMap['network']!(jsonMap);
      }
    }
  };

  /// 同步方法集合
  static final Map<String, dynamic> syncHandlerMap = {
    'putKey': (map) {
      final bookId = map['bookId'];
      if (bookId == null) {
        return;
      }
      final key = map['k'] ?? '';
      final value = map['v'] ?? '';
      BUtils.qn_Put(
          (bookId is int) ? bookId : (int.tryParse(bookId.toString()) ?? -99),
          key,
          value);
    },

    'getKey': (map) {
      final bookId = map['bookId'];
      if (bookId == null) {
        return "";
      }
      final key = map['k'] ?? '';
      return BUtils.qn_Get(
          (bookId is int) ? bookId : (int.tryParse(bookId.toString()) ?? -99),
          key);
    },

    'getExplorePage': (map) {
      return GlobalPrev.currentPage;
    },

    'getSearchKey': (map) {
      return GlobalPrev.currentSearchKey;
    },

    'getPreV': (map) {
      return GlobalPrev.currentV;
    },

    'networkData': (map) {
      return GlobalPrev.currentNetworkData;
    },

    'getBook': (map) {
      final bookId = map['bookId'];
      if (bookId == null) {
        return null;
      }
      var qnBookModel = BookSourceCache.i.get(int.parse(bookId.toString()));
      return qnBookModel?.toMap();
    },

    'getChapter': (map) {
      var innerToc =
          BookSourceCache.i.getInnerToc(map['chapterUrl'].toString());
      return innerToc.toMap();
    },

    'log': (map) {
      // if(map['level'])
      final level = map['level'];
      if (level == 'error' || level == 'ERROR') {
        Logger.error(map['log'], tag: 'JS内部调用日志');
      } else {
        Logger.debug(() => map['log'], tag: 'JS内部调用日志');
      }
      return '';
    },

    'xpathQuerySingle': (map) {
      var rule = map['rule'] ?? '';
      var node = map['node'] ?? '';
      var join = map['join'] ?? '';
      var nodeTag = map['nodeTag'];
      if (nodeTag != null && nodeTag != '') {
        return BNativeUtils.qn_querySingle(
            withRule: rule,
            node: GDataTagCache.getData(nodeTag),
            model: QNBookModel.empty,
            join: join,
            addParams: {});
      } else {
        return BNativeUtils.qn_querySingle(
            withRule: rule,
            node: node,
            model: QNBookModel.empty,
            join: join,
            addParams: {});
      }
    },

    'xpathQueryList': (map) {
      var rule = map['rule'] ?? '';
      var node = map['node'] ?? '';
      var nodeTag = map['nodeTag'];
      if (nodeTag != null && nodeTag != '') {
        return BUtils.qn_queryList(
            withRule: rule, node: GDataTagCache.getData(nodeTag));
      } else {
        return BUtils.qn_queryList(withRule: rule, node: node);
      }
    },

    // Xpath解析列表 tagCount ,高性能查询
    // rule为规则
    // node为 数据节点
    // nodeTag 数据节点标识，用来优化查询性能
    // 返回 解析列表总数，后续 tag 可使用 for循环拼装
    'xpathQueryListTagCount': (map) {
      var rule = map['rule'] ?? '';
      var nodeTag = map['nodeTag'];
      if (nodeTag != null && nodeTag != '') {
        final list = BUtils.qn_queryList(
            withRule: rule, node: GDataTagCache.getData(nodeTag));
        int count = list.length;
        GDataTagCache.addSplitData(list, nodeTag);
        return count;
      } else {
        var node = map['node'] ?? '';
        final list = BUtils.qn_queryList(withRule: rule, node: node);
        int count = list.length;
        GDataTagCache.addSplitData(list, nodeTag);
        return count;
      }
    },

    // 通过 nodeTag 获取 真实的数据源
    'queryDataByNodeTag': (map) {
      return GDataTagCache.getData(map['tag']);
    },

    // 获取 hashCode
    'hashCode': (map) {
      return HashM.hashCodeGet(map['str'] ?? '').toString();
    },

    // 获取时间戳
    'timestamp': (map) => DateTime.now().millisecondsSinceEpoch,

    // str转bytes
    // str为字符串
    // charset为编码方式，支持 utf-8/gbk
    // 输出 uint8Array，输出字节数组
    "strToBytes": (map) {
      if (map['charset'] == 'utf-8') {
        return utf8.encode(map['str']);
      } else if (map['charset'] == 'gbk') {
        return gbk.encode(map['str']);
      } else {
        return [];
      }
    },

    // bytes转字符串
    // bytes为字节数组
    // charset为编码方式，支持 utf-8/gbk
    // 输出 支付串
    "bytesToStr": (map) {
      final bytes = map['bytes'];

      if (bytes == null) {
        return '';
      }
      if (bytes is! List) {
        return '';
      }

      List bbytes = bytes;
      if (map['charset'] == 'utf-8') {
        return utf8.decode(bbytes.map((e) => e as int).toList(growable: false),
            allowMalformed: true);
      } else if (map['charset'] == 'gbk') {
        return gbk.decode(bbytes.map((e) => e as int).toList(growable: false),
            allowMalformed: true);
      } else {
        return 'charset只支持utf-8或gbk';
      }
    },

    /**
     * Base64字符串 转 字符串
     * 输入为 base64字符串 和 字符编码(支持 utf-8,gbk)
     * 输出 解码字符串
     */
    "base64Decode": (map) {
      final base64 = map['base64'];
      final charset = map['charset'];
      var uint8list = const Base64Decoder().convert(base64);
      if (charset == 'utf-8') {
        return utf8.decode(uint8list.toList(growable: false),
            allowMalformed: true);
      } else if (charset == 'gbk') {
        return gbk.decode(uint8list.toList(growable: false),
            allowMalformed: true);
      } else {
        return 'charset只支持utf-8或gbk';
      }
    },

    // base64转 bytes数组
    "base64DecodeToByteArray": (map) {
      final base64 = map['base64'];
      var uint8list = const Base64Decoder().convert(base64);
      return uint8list.toList(growable: false);
    },

    // base64 加密,输出 base64字符串
    "base64Encode": (map) {
      final source = map['str'];
      return const Base64Encoder().convert(utf8.encode(source));
    },

    // base64 加密，输入为 bytes数组,输出 base64字符串
    "base64EncodeBytes": (map) {
      final bytes = map['bytes'];
      if (bytes == null) {
        return '';
      }
      if (bytes is! List) {
        return '';
      }
      return const Base64Encoder()
          .convert(bytes.map((e) => e as int).toList(growable: false));
    },

    // hex 转 bytes数组
    "hexDecodeToByteArray": (map) {
      final hexStr = map['hex'];
      return hex.decode(hexStr);
    },

    // hex 转 字符串, charset为编码格式
    "hexDecodeToString": (map) {
      final hexStr = map['hex'];
      final charset = map['charset'];
      var list = hex.decode(hexStr);
      if (charset == 'utf-8') {
        return utf8.decode(list, allowMalformed: true);
      } else if (charset == 'gbk') {
        return gbk.decode(list, allowMalformed: true);
      } else {
        return 'charset只支持utf-8或gbk';
      }
    },

    //  utf8 编码为hexString
    "strEncodeToHex": (map) {
      final str = map['str'];
      final list = utf8.encode(str);
      return hex.encode(list);
    },

    // utf8编码转gbk编码，返回字符串
    "utf8ToGbk": (map) {
      final str = map['str'];
      final list = utf8.encode(str);
      return gbk.decode(list, allowMalformed: true);
    },

    // url编码
    // str为待编码字符串
    // enc 为 TODO 待调整，暂未实现enc
    "encodeURI": (map) {
      final str = map['str'];
      return Uri.encodeComponent(str);
    },

    // url解密
    // str为待解码字符串
    // enc 为 TODO 待调整，暂未实现enc
    "decodeURI": (map) {
      final str = map['str'];
      return Uri.decodeComponent(str);
    },

    // t2s
    // TODO 待调整，t2s暂未实现
    "t2s": (map) {
      return '';
    },

    // s2t
    // TODO 待调整，s2t暂未实现
    "s2t": (map) {
      return '';
    },

    /**
     * toast提示，调用 App toast
     */
    "toast": (map) {
      // final text = map['text'];
      // TODO 待实现
      // return '';
    },

    /**
     * toast提示，调用 App toast
     */
    "longToast": (map) {
      // final text = map['text'];
      // TODO 待实现
      // return '';
    },

    //
    //
    // /// unicode解码 ，输入字符串，输出字符串
    // "unicode_decode": (args) => AppInnerUtils.unicodeDecode(args),

    // md5签名
    // 32位小写字符串
    "md5": (map) {
      final str = map['str'];
      var list = utf8.encode(str);
      var digest = md5.convert(list);
      return hex.encode(digest.bytes);
    },

    // md5签名
    // 32位小写字符串
    "md5String": (map) {
      final str = map['str'];
      var list = utf8.encode(str);
      var digest = md5.convert(list);
      return hex.encode(digest.bytes);
    },

    // 3DES解密
    // 传入 secretStr， key，iv
    // 其中 key可以为 utf8编码的字符串 或者 为 uint8List
    // 其中 iv可以为 utf8编码的字符串 或者 为 uint8List
    // return utf8字符串
    "DESedeDecode": (map) {
      final source = map['secretStr'];
      final key = map['key'];
      final iv = map['iv'];
      final List<int> kkey;
      if (key is String) {
        kkey = utf8.encode(key);
      } else {
        kkey = (key as List).map((e) => e as int).toList(growable: false);
      }
      final List<int> iiv;
      if (iv is String) {
        iiv = utf8.encode(iv);
      } else {
        iiv = (iv as List).map((e) => e as int).toList(growable: false);
      }
      var uint8list = const Base64Decoder().convert(source);
      var list = uint8list.toList();
      DES3 desECB = DES3(
          key: kkey,
          mode: DESMode.ECB,
          iv: iiv,
          paddingType: DESPaddingType.PKCS7);
      var decrypt = desECB.decrypt(list);
      return utf8.decode(decrypt);
    },

    // 3DES加密
    // 传入 source， key，iv
    // 其中 key可以为 utf8编码的字符串 或者 为 uint8List
    // 其中 iv可以为 utf8编码的字符串 或者 为 uint8List
    // return utf8字符串
    "DESedeEncode": (map) {
      final source = map['source'];
      final key = map['key'];
      final iv = map['iv'];
      final List<int> kkey;
      if (key is String) {
        kkey = utf8.encode(key);
      } else {
        kkey = (key as List).map((e) => e as int).toList(growable: false);
      }
      final List<int> iiv;
      if (iv is String) {
        iiv = utf8.encode(iv);
      } else {
        iiv = (iv as List).map((e) => e as int).toList(growable: false);
      }
      DES3 desECB = DES3(
          key: kkey,
          mode: DESMode.ECB,
          iv: iiv,
          paddingType: DESPaddingType.PKCS7);
      var encrypt = desECB.encrypt(utf8.encode(source));
      return const Base64Encoder().convert(encrypt);
    },

    /// ras加密，
    /// source 待加密字符串
    /// key 秘钥，支持 Utf8字符串 或者 bytes数组
    /// type 编码类型： PKCS1 或 OAEP
    /// return 加密的bytes数组
    "rsaEncode": (map) {
      final source = map['source'];
      final key = map['key'];
      final type = map['type'];

      final List<int> ssource;
      if (source is String) {
        ssource = utf8.encode(source);
      } else {
        ssource = source;
      }

      final List<int> kkey;
      if (key is String) {
        kkey = utf8.encode(key);
      } else {
        kkey = key;
      }

      final rsaKey =
          CryptoUtils.rsaPublicKeyFromDERBytes(Uint8List.fromList(kkey));
      final pem = CryptoUtils.encodeRSAPublicKeyToPemPkcs1(rsaKey);
      final publicKey = RSAKeyParser().parse(pem) as RSAPublicKey;
      final encrypter = Encrypter(
        RSA(
            publicKey: publicKey,
            encoding: type == 'PKCS1' ? RSAEncoding.PKCS1 : RSAEncoding.OAEP),
      );
      final encrypted = encrypter.encryptBytes(ssource);
      return encrypted.bytes;
    },

    /// aes加密，String source,String key,String iv,String padding,String mode
    /// source 字符串
    /// key 加密Key
    /// iv 加密IV
    /// padding 加密Padding类型，例如 PKCS5 / PKCS7
    /// mode 加密模式,例如 ecb,sic
    /// return utf8字符串
    "aesEncode": (map) {
      final source = map['source'];
      final key = map['key'];
      final iv = (map['iv'] ?? '');
      final mode = (map['mode'] ?? '');
      final padding = (map['padding'] ?? '');
      return AppInnerUtils.aesEncrypt(source, key, iv, padding, mode);
    },

    /// aes解密，String secretStr,String key,String iv,String padding,String mode
    /// secretStr 待解密字符串
    /// key 加密Key
    /// iv 加密IV
    /// padding 加密Padding类型，例如 PKCS5 / PKCS7
    /// mode 加密模式,例如 ecb,sic
    /// return utf8字符串
    "aesDecode": (map) {
      final source = map['source'];
      final key = map['key'];
      final iv = map['iv'];
      final List<int> kkey;
      if (key is String) {
        kkey = utf8.encode(key);
      } else {
        kkey = (key as List).map((e) => e as int).toList(growable: false);
      }
      final List<int> iiv;
      if (iv is String) {
        iiv = utf8.encode(iv);
      } else {
        iiv = (iv as List).map((e) => e as int).toList(growable: false);
      }
      final mode = (map['mode'] ?? '');
      final padding = (map['padding'] ?? '');
      return AppInnerUtils.aesDecrypt(source, kkey, iiv, padding, mode);
    },

    "addInnerToc": (map) {
      final innerToc = map['innerToc'];
      BookSourceCache.i.addInnerToc(QNTocInnerModel.init(
          title: innerToc['title'],
          url: innerToc['url'],
          needVip: innerToc['needVip']));
    },

    "addContent": (map) {
      final contentMap = map['content'];
      final List<ComicPicModel> picList = [];
      final comicPicLists = contentMap['comicPicList'];
      if (comicPicLists != null && comicPicLists is List) {
        for (var comic in comicPicLists) {
          final Map<String, String> headers = {};
          final comicH = comic['headers'];
          if (comicH != null && comicH is Map) {
            for (var entry in comicH.entries) {
              headers[entry.key.toString()] = entry.value.toString();
            }
          }
          picList.add(ComicPicModel.init(
              url: comic['url'] ?? '',
              w: comic['w'] ?? 0,
              h: comic['h'] ?? 0,
              headers: headers));
        }
      }

      var musicContent = const MusicContentModel.init(url: 'url');
      final musicData = contentMap['musicContent'];
      if (musicData != null && musicData is Map) {
        final Map<String, String> headers = {};
        final muicH = musicData['headers'];
        if (muicH != null && muicH is Map) {
          for (var entry in muicH.entries) {
            headers[entry.key.toString()] = entry.value.toString();
          }
        }
        musicContent = MusicContentModel.init(
            url: musicData['url'],
            headers: headers,
            duration: musicData['duration'] ?? 0);
      }

      final contentModel = QNContentModel.init(
          contentKey: contentMap['contentKey'],
          bookContent: contentMap['bookContent'] ?? '',
          musicContent: musicContent,
          comicPicList: picList);
      BookSourceCache.i.addContent(contentModel);
    },

    "addAllBook": (mmap) {
      final list = mmap['list'];
      if (list != null && list is List) {
        for (var bookMap in list) {
          final book = _transformBookModelFromMap(bookMap);
          BookSourceCache.i.add(book);
        }
      }
    },

    "addBook": (map) {
      final bookModel = map['book'];
      final book = _transformBookModelFromMap(bookModel);
      BookSourceCache.i.add(book);
    },

    "addBookToc": (mmap) {
      final map = mmap['tocModel'];
      final dataMapList = map['dataList'];
      final List<QNTocInnerModel> innerList = [];
      if (dataMapList != null && dataMapList is List) {
        for (var innerToc in dataMapList) {
          innerList.add(QNTocInnerModel.init(
              title: innerToc['title'],
              url: innerToc['url'],
              needVip: innerToc['needVip']));
        }
      }
      var bookId = map['bookId'];
      if (bookId is int) {
        final tocModel = QNTocModel.init(
            bookId: map['bookId'],
            updateTime: map['updateTime'],
            dataList: innerList);
        BookSourceCache.i.addToc(tocModel);
      } else {
        final tocModel = QNTocModel.init(
            bookId: int.tryParse(bookId) ?? -99,
            updateTime: map['updateTime'],
            dataList: innerList);
        BookSourceCache.i.addToc(tocModel);
      }
    },

    "qn_querySingle": (map) {
      return BNativeUtils.qn_querySingle(
          withRule: map['withRule'],
          node: map['node'] ?? '',
          model: _transformBookModelFromMap(map['model']),
          join: map['join'] ?? '',
          chapterModel: _transformChapterModelFromMap(map['chapterModel']),
          addParams: {});
    },

    "qn_queryList": (map) {
      return BNativeUtils.qn_queryList(
          withRule: map['withRule'],
          node: map['node'] ?? '',
          model: _transformBookModelFromMap(map['model']),
          returnString: map['returnString'] ?? false);
    },

    "setCurrentNetworkData": (map) {
      final data = map['data'];
      GlobalPrev.currentNetworkData = data;
      return '';
    },

    "setCurrentPage": (map) {
      final page = map['page'];
      GlobalPrev.currentPage = page;
      return '';
    },

    "setCurrentSearchKey": (map) {
      final key = map['key'];
      GlobalPrev.currentSearchKey = key;
      return '';
    },
  };
}

QNTocInnerModel? _transformChapterModelFromMap(dynamic chapterModel) {
  if (chapterModel == null) {
    return null;
  }
  return QNTocInnerModel.init(
      title: chapterModel['title'],
      url: chapterModel['url'],
      needVip: chapterModel['needVip'],
      wordCount: chapterModel['wordCount']);
}

QNBookModel _transformBookModelFromMap(dynamic bookMap) {
  if (bookMap == null) {
    return QNBookModel.empty;
  }

  if (bookMap['siteId'] == null) {
    return QNBookModel.empty;
  }

  final tagList = bookMap['tags'];
  List<String> tags = [];
  if (tagList != null && tagList is List) {
    tags = tagList.map((e) => e.toString()).toList(growable: false);
  }

  return QNBookModel.init(
    siteId: bookMap['siteId'],
    bookName: bookMap['bookName'],
    channelId: bookMap['channelId'] ?? '',
    siteVersion: bookMap['siteVersion'],
    encode: bookMap['encode'] ?? '',
    detailUrl: bookMap['detailUrl'],
    desc: bookMap['desc'] ?? '',
    tocUrl: bookMap['tocUrl'] ?? '',
    cover: bookMap['cover'] ?? '',
    wordCount: int.tryParse((bookMap['wordCount'] ?? 0).toString()) ?? 0,
    star: double.tryParse((bookMap['star'] ?? 8.5).toString()) ?? 8.5,
    tags: tags,
    newestChapter: bookMap['newestChapter'] ?? '',
    author: bookMap['author'] ?? '',
  );
}

const String jsInit = r"""
let GLOBAL_RULE_MAP={};function sync_proxy(method,args){return sendMessage(method,JSON.stringify(args));}
async function async_proxy(method,args){return await sendMessage(method,JSON.stringify(args));}
function toDateString(date,format="yyyy-MM-dd HH:mm:ss#SSS"){function digit(value,length=2){if(typeof value==="undefined"||value===null||String(value).length>=length){return value;}
return(Array(length).join("0")+value).slice(-length);}
const ymd=[digit(date.getFullYear(),4),digit(date.getMonth()+1),digit(date.getDate()),];const hms=[digit(date.getHours()),digit(date.getMinutes()),digit(date.getSeconds()),digit(date.getMilliseconds(),3),];return format.replace(/yyyy/g,ymd[0]).replace(/MM/g,ymd[1]).replace(/dd/g,ymd[2]).replace(/HH/g,hms[0]).replace(/mm/g,hms[1]).replace(/ss/g,hms[2]).replace(/SSS/g,hms[3]);}
class QBAsync{static async network(argsMap){if(isEmpty(argsMap.ext)){argsMap.ext={needDataTag:1}}
if(isEmpty(argsMap.ext.needDataTag)){argsMap.ext.needDataTag=1}
return await async_proxy('network',argsMap);}
static async networkStr(allInOneStr){let result=await async_proxy('networkStr',{url:allInOneStr});return result.data}}
class QB{static _enableLog=true
static enableLogCostTime=true
static configLog(logConfig){this._enableLog=logConfig.enable||true
this.enableLogCostTime=logConfig.logCostTime}
static log(log,level){if(!this._enableLog){return;}
if(isEmpty(level)){level='DEBUG'}
return sync_proxy('log',{log:log,level:level,});}
static timestamp(){return new Date().getTime();}
static putKey(bookId,k,v){return sync_proxy('putKey',{bookId:bookId,k:k,v:v,})}
static getKey(bookId,k){return sync_proxy('getKey',{bookId:bookId,k:k,})}
static getExplorePage(){return sync_proxy('getExplorePage',{})}
static getSearchKey(){return sync_proxy('getSearchKey',{})}
static getPreV(){return sync_proxy('getPreV',{})}
static networkData(){return sync_proxy('networkData',{})}
static getBook(bookId){return sync_proxy('getBook',{bookId:bookId})}
static getChapter(chapterUrl){return sync_proxy('getChapter',{chapterUrl:chapterUrl})}
static xpathQuerySingle(rule,node,join,nodeTag){return sync_proxy('xpathQuerySingle',{rule:rule,node:node,join:join,nodeTag:nodeTag})}
static xpathQueryList(rule,node,nodeTag){return sync_proxy('xpathQueryList',{rule:rule,node:node,nodeTag:nodeTag})}
static xpathQueryListTagCount(rule,node,nodeTag){return sync_proxy('xpathQueryListTagCount',{rule:rule,node:node,nodeTag:nodeTag})}
static queryDataByNodeTag(tag){return sync_proxy('qDtag',{tag:tag,})}
static md5String(str){return sync_proxy('md5String',{str:str});}
static md5(str){return sync_proxy('md5',{str:str});}
static DESedeDecode(secretStr,key,iv){return sync_proxy('DESedeDecode',{secretStr:secretStr,key:key,iv:iv,});}
static DESedeEncode(source,key,iv){return sync_proxy('DESedeEncode',{source:source,key:key,iv:iv,});}
static aesEncode(source,key,iv,padding,mode){return sync_proxy('aesEncode',{source:source,key:key,iv:iv,padding:padding,mode:mode,});}
static aesDecode(secretStr,key,iv,padding,mode){return sync_proxy('aesDecode',{source:secretStr,key:key,iv:iv,padding:padding,mode:mode,});}
static rasEncode(source,key,type){return sync_proxy('aesEncode',{source:source,key:key,type:type,});}
static hashCode(str){return sync_proxy('hashCode',{str:str})}
static strToBytes(str,charset){return sync_proxy('strToBytes',{str:str,charset:charset})}
static bytesToStr(bytes,charset){return sync_proxy('bytesToStr',{bytes:bytes,charset:charset})}
static base64Decode(base64,charset){return sync_proxy('base64Decode',{base64:base64,charset:charset})}
static base64DecodeToByteArray(base64){if(isEmpty(base64)){return[]}
return sync_proxy('base64DecodeToByteArray',{base64:base64})}
static base64Encode(str){return sync_proxy('base64Encode',{str:str})}
static base64EncodeBytes(bytes){return sync_proxy('base64EncodeBytes',{bytes:bytes})}
static hexDecodeToByteArray(hex){return sync_proxy('hexDecodeToByteArray',{hex:hex})}
static hexDecodeToString(hex,charset){return sync_proxy('hexDecodeToString',{hex:hex,charset:charset})}
static strEncodeToHex(str){return sync_proxy('strEncodeToHex',{str:str})}
static utf8ToGbk(str){return sync_proxy('utf8ToGbk',{str:str})}
static encodeURI(str,enc){return sync_proxy('encodeURI',{str:str,enc:enc})}
static decodeURI(str,enc){return sync_proxy('decodeURI',{str:str,enc:enc})}
static t2s(text){return sync_proxy('t2s',{text:text})}
static s2t(text){return sync_proxy('s2t',{text:text})}
static toast(text){return sync_proxy('toast',{text:text})}
static longToast(text){return sync_proxy('longToast',{text:text})}
static getFile(path){}
static readFile(path){}
static readTxtFile(path,charset){return""}
static deleteFile(path){}
static unzipFile(zipPath){return'';}
static un7zFile(zipPath){return'';}
static unrarFile(rarPath){return''}
static unArchiveFile(zipPath){return''}
static addInnerToc(innerToc){return sync_proxy('addInnerToc',{innerToc:innerToc})}
static addContent(qnContentModel){return sync_proxy('addContent',{content:qnContentModel})}
static addAllBook(list){return sync_proxy('addAllBook',{list:list})}
static addBook(qnBookModel){return sync_proxy('addBook',{book:qnBookModel})}
static addBookToc(tocModel){return sync_proxy('addBookToc',{tocModel:tocModel})}
static qn_querySingle(withRule,node,model,join,chapterModel){return sync_proxy('qn_querySingle',{withRule:withRule,node:node,model:model,join:join,chapterModel:chapterModel})}
static qn_queryList(withRule,node,model){return sync_proxy('qn_queryList',{withRule:withRule,node:node,model:model,returnString:true})}
static setCurrentNetworkData(data){return sync_proxy('setCurrentNetworkData',{data:data,})}
static setCurrentPage(page){return sync_proxy('setCurrentPage',{page:page,})}
static setCurrentSearchKey(key){return sync_proxy('setCurrentSearchKey',{key:key,})}}
function isEmpty(obj){return obj===undefined||obj===''||obj==null;}
function getPreV(){return QB.getPreV()}
""";

class JSUtils {
  static bool hasInit = false;

  static JavascriptRuntime? runtime;

  static bool isJavascriptCoreRuntime = false;

  static Future<void> init({bool needLog = false}) async {
    if (hasInit) {
      return;
    }
    hasInit = true;
    JavascriptRuntime javascriptRuntime = getJavascriptRuntime(xhr: false);
    if (Platform.isAndroid || Platform.isWindows) {
      isJavascriptCoreRuntime = false;
    } else {
      isJavascriptCoreRuntime = true;
    }
    JavascriptRuntime.debugEnabled = true;
    javascriptRuntime.setInspectable(true);
    javascriptRuntime.evaluate("$jsInit\n");
    for (MapEntry entry in GlobalNativeProxy.syncHandlerMap.entries) {
      javascriptRuntime.onMessage(entry.key, (args) => entry.value(args));
    }

    for (MapEntry entry in GlobalNativeProxy.asyncHandlerMap.entries) {
      javascriptRuntime.onMessage(entry.key, (args) async {
        return await entry.value(args);
      });
    }
    runtime = javascriptRuntime;
    if (!needLog) {
      runtime!.evaluate('QB.configLog({logCostTime: false, enable:false})');
    } else {
      runtime!.evaluate('QB.configLog({logCostTime: true, enable:true})');
    }
  }

  static Future<dynamic> evaluateAsync(String js) async {
    var result = await runtime!.evaluateAsync(js);
    runtime!.executePendingJob();
    var jsEvalResult = await runtime!.handlePromise(result);
    if (isJavascriptCoreRuntime) {
      return jsonDecode(jsEvalResult.stringResult);
    } else {
      return jsEvalResult.rawResult;
    }
  }

  static dynamic evaluateSync(String js) {
    // js = js.replaceAll('\n', '');
    // Logger.debug(() => "js --> $js");
    // Logger.debug(() => "js转义脚本:\n$rule\n执行结果:\n$result");
    var result = runtime!.evaluate(js);
    if (isJavascriptCoreRuntime) {
      JSValue jsValue = JSValuePointer(result.rawResult)
          .getValue((runtime as JavascriptCoreRuntime).context);
      final type = jsValue.type;

      var returnResult;

      switch (type) {
        case JSType.kJSTypeUndefined:
          returnResult = null;
          break;
        case JSType.kJSTypeNull:
          returnResult = null;
          break;
        case JSType.kJSTypeBoolean:
          returnResult = jsValue.toBoolean;
          break;
        case JSType.kJSTypeNumber:
          returnResult = jsValue.toNumber();
          break;
        case JSType.kJSTypeString:
          returnResult = jsValue.string ?? '';
          break;
        case JSType.kJSTypeObject:
          returnResult = jsonDecode(jsValue.createJSONString().string ?? '{}');
          break;
        case JSType.kJSTypeSymbol:
          returnResult = '未支持 kJSTypeSymbol';
          break;
        default:
          returnResult = null;
          break;
      }
      // Logger.debug(() => "js转义脚本:\n$js\n执行结果:\n$returnResult");
      return returnResult;
    } else {
      final newResult = result.rawResult;
      // Logger.debug(() => "js转义脚本:\n$js\n执行结果:\n$newResult");
      return newResult;
    }
  }
}
