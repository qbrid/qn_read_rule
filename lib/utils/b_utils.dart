import 'dart:convert';
import 'dart:math' as math;

import 'package:dio/dio.dart';
import 'package:fast_gbk/fast_gbk.dart';
import 'package:html/dom.dart';
import 'package:html/parser.dart' as html_parse;
import 'package:json_path/json_path.dart';
import 'package:qn_read_rule/book_provide/base_cache.dart';
import 'package:qn_read_rule/book_provide/base_model.dart';
import 'package:qn_read_rule/utils/NeworkHelper.dart';
import 'package:qn_read_rule/utils/hash_m.dart';
import 'package:qn_read_rule/utils/js_utils.dart';
import 'package:qn_read_rule/utils/logger.dart';
import 'package:qn_read_rule/utils/qn_utils.dart';
import 'package:xpath_selector_html_parser/xpath_selector_html_parser.dart';

class BNRuleConstants {
  // 拼接在规则前方，代表数组反向
  static const List_Reserve = '-';

  // 多个数组取 合集
  static const List_And = '&&';

  // 多个数组取 或值，当前一个数组存在值（不为空），则后续不进行计算
  static const List_Or = '||';

  //%%会依次取数，如三个列表，先取列表1的第一个，再取列表2的第一个，再取列表3的第一个，再取列表1的第二个，再取列表2的第二个
  // %% 不可与 && 及 || 同时使用
  static const List_EachGet = '%%';

  static const List<String> List_Rules = [List_And, List_Or, List_EachGet];

  static final RegExp List_RulesReg = RegExp(r'\|\||&&|%%');

  static final Map<String, List<RuleListSplitModel>> List_RuleCaches = {};

  // 取单个数组时，如前值不为空，则后续不进行计算
  static const String Single_Or = '||';

  // 上一步计算的 值
  static const String Single_Pre_Value = r'qnPre';

  static final RegExp Single_Math_Reg = RegExp(r'{{(.*?)}}');

  static const String Single_PreV = 'preV';

  static final Map<String, List<RuleSingleMethod>> Single_RuleCaches = {};
}

class GlobalPrev {
  static String currentV = '';
  static String currentNetworkData = '';
  static String currentSearchKey = '';
  static int currentPage = 1;
  static String? currentSearchGroupKey;

  static CancelToken? cancelToken;
}

enum RuleListSpitType { rule, and, or, eachGet }

enum RuleSingleSpitType {
  string,

  // 规则
  xpath,
  jsonPath,

  // 或
  or,

  // 字符串相连结 ，等价于 +
  connect,

  // 内置方法
  nativeFunc
}

class RuleListSplitModel {
  final String? rule;

  final RuleListSpitType type;

  const RuleListSplitModel({required this.type, required this.rule});
}

/// 以 ,,, 分段的规则
///
class RuleSingleSplitModel {
  final String? rule;

  final RuleSingleSpitType type;

  final List<RuleSingleSplitModel>? params;

  const RuleSingleSplitModel(
      {required this.type, required this.rule, required this.params});
}

enum RuleSingleParamsType {
  string,
  xpath,
  jsonPath,
  css,

  // 上一个值
  // preV,
  regSingle,
  regAll,
  nativeFunc,
  dynamicMethod,
  js,
}

enum RuleSingleMethodType {
  // 取 或
  or,

  // 取 加
  add,

  // // 调用本地方法
  // nativeFunc,
  reg,
  put,
  emptyReturn,
  js,
  unknown
}

abstract class RuleSingleParamsInterface {
  RuleSingleParamsType get cType;

  String math(QNBookModel model, dynamic data, Map<String, String> tmpMap,
      QNTocInnerModel? chapterModel, String? join, bool isQueryContent);
}

class RuleSingleParams extends RuleSingleParamsInterface {
  final String rule;

  @override
  RuleSingleParamsType get cType => type;

  final RuleSingleParamsType type;

  RuleSingleParams({required this.type, required this.rule});

  @override
  String math(QNBookModel model, dynamic data, Map<String, String> tmpMap,
      QNTocInnerModel? chapterModel, String? join, bool isQueryContent) {
    if (type == RuleSingleParamsType.jsonPath) {
      if (rule == r'$0') {
        // Logger.debug(() => "rule ->$rule RuleSingleParams math tmpMap ->$tmpMap");
        return rule;
      } else if (rule == r'$1') {
        // Logger.debug(() => "rule ->$rule  RuleSingleParams math tmpMap ->$tmpMap");
        return rule;
      } else if (rule == r'$2') {
        // Logger.debug(() => "rule ->$rule  RuleSingleParams math tmpMap ->$tmpMap");
        return rule;
      } else if (rule == r'$3') {
        // Logger.debug(() => "rule ->$rule  RuleSingleParams math tmpMap ->$tmpMap");
        return rule;
      }
      var result = '';
      if (tmpMap.isNotEmpty) {
        result = BNativeUtils._qn_JsonPathSingle(rule, tmpMap, model,
            join: join ?? '');
      }
      if (result.isNotEmpty) {
        return result;
      }
      return BNativeUtils._qn_JsonPathSingle(rule, data, model,
          join: join ?? '');
    } else if (type == RuleSingleParamsType.css) {
      return BNativeUtils._qn_CssSingle(rule, data, model,
          join: join ?? '', isQueryContent: isQueryContent);
    } else if (type == RuleSingleParamsType.xpath) {
      return BNativeUtils._qn_XpathSingle(rule, data, model,
          join: join ?? '', isQueryContent: isQueryContent);
    } else {
      if (rule == BNRuleConstants.Single_PreV) {
        return tmpMap[BNRuleConstants.Single_PreV]!;
      } else if (rule.startsWith('G_')) {
        if (tmpMap.containsKey('@put.${rule.substring(2)}')) {
          return tmpMap['@put.${rule.substring(2)}'].toString();
        }
        final getV = BookSourceCache.i.qnGet(model.bookId, rule.substring(2));
        if (getV == null) {
          throw Exception('不存在的属性值 $rule,请先使用@put进行赋值');
        }
        return getV;
      } else if (rule.startsWith('book.')) {
        if (tmpMap.containsKey(rule)) {
          return tmpMap[rule].toString();
        }
        return model.queryBy(rule.substring(5));
      } else if (rule.startsWith('chapter.')) {
        if (tmpMap.containsKey(rule)) {
          return tmpMap[rule].toString();
        }
        return chapterModel?.queryBy(rule.substring(8)) ?? 'error';
      } else if (rule == 'page') {
        if (tmpMap.containsKey(rule)) {
          return tmpMap[rule].toString();
        }
        if (data is Map) {
          return data['page'].toString();
        } else {
          throw Exception('page字段未定义，请先定义page字段');
        }
      } else if (rule.startsWith('g.')) {
        if (tmpMap.containsKey(rule.substring(2))) {
          return tmpMap[rule.substring(2)].toString();
        }
        throw Exception('$rule字段未定义，请先定义$rule字段字段');
      } else {
        return rule;
      }
    }
  }
}

class RuleSingleNativeFuncParams extends RuleSingleParamsInterface {
  @override
  RuleSingleParamsType get cType => RuleSingleParamsType.nativeFunc;

  final List<RuleSingleParamsInterface> params = [];

  final String method;

  RuleSingleNativeFuncParams(this.method);

  void addParam(RuleSingleParamsInterface param) {
    // Logger.debug(() => "param ->$param");
    if (param.cType == RuleSingleParamsType.string &&
        (param as RuleSingleParams).rule.isEmpty) {
    } else {
      params.add(param);
    }
  }

  @override
  String math(QNBookModel model, dynamic data, Map<String, String> tmpMap,
      QNTocInnerModel? chapterModel, String? join, bool isQueryContent) {
    return QNUtilsProxy.handlerSync(
        method,
        params
            .map((e) =>
                e.math(model, data, tmpMap, chapterModel, join, isQueryContent))
            .toList(growable: false));
  }
}

class RuleSingleMethodParams extends RuleSingleParamsInterface {
  @override
  RuleSingleParamsType get cType => RuleSingleParamsType.dynamicMethod;

  RuleSingleMethod singleMethod =
      RuleSingleMethod(method: RuleSingleMethodType.unknown);

  RuleSingleMethodParams();

  @override
  String math(QNBookModel model, dynamic data, Map<String, String> tmpMap,
      QNTocInnerModel? chapterModel, String? join, bool isQueryContent) {
    return singleMethod.math(
        model, data, tmpMap, chapterModel, join, isQueryContent);
  }
}

class RuleSingleMethod {
  final List<RuleSingleParamsInterface> params;

  final RuleSingleMethodType method;

  RuleSingleMethod({required this.method}) : params = [];

  void addPrams(RuleSingleParamsInterface param) {
    if (param.cType == RuleSingleParamsType.string &&
        param is RuleSingleParams) {
      if (param.rule.isNotEmpty) {
        params.add(param);
      }
    } else {
      params.add(param);
    }
  }

  String math(QNBookModel model, dynamic data, Map<String, String> tmpMap,
      QNTocInnerModel? chapterModel, String? join, bool isQueryContent) {
    switch (method) {
      case RuleSingleMethodType.add:
        return params
            .map((e) =>
                e.math(model, data, tmpMap, chapterModel, join, isQueryContent))
            .join();
      case RuleSingleMethodType.or:
        int length = params.length;
        for (int i = 0; i < length; i++) {
          var p = params[i];
          var result = p
              .math(model, data, tmpMap, chapterModel, join, isQueryContent)
              .trim();
          if (result.isNotEmpty) {
            return result;
          }
        }
        return '';
      case RuleSingleMethodType.emptyReturn:
        return tmpMap[BNRuleConstants.Single_PreV] ?? '';
      case RuleSingleMethodType.reg:
        var preV = tmpMap[BNRuleConstants.Single_PreV] ?? '';
        bool isSingleReg = params[0].cType == RuleSingleParamsType.regSingle;
        final regRule = params[1]
            .math(model, data, tmpMap, chapterModel, join, isQueryContent);
        final replaceRule = params[2]
            .math(model, data, tmpMap, chapterModel, join, isQueryContent);
        var newRegStr = regRule;
        List<String> keyList = [];
        {
          var pos = 1;
          var index = -1;
          var indexPos = 0;
          while ((index = newRegStr.indexOf("(", indexPos)) != -1) {
            if (index > 0) {
              if (newRegStr[index - 1] == '\\') {
                indexPos = index + 1;
                continue;
              }
            }
            newRegStr = newRegStr.replaceFirst("(", "(?<\$$pos>", indexPos);
            keyList.add('\$$pos');
            pos = pos + 1;
            indexPos = index + 1;
          }
        }
        RegExp regExp = RegExp(newRegStr);
        if (isSingleReg) {
          final result =
              replaceSingle(regExp, keyList, replaceRule, preV, tmpMap);
          tmpMap[BNRuleConstants.Single_PreV] = result;
          GlobalPrev.currentV = result;
          return result;
        } else {
          final result = replaceAll(regExp, keyList, replaceRule, preV, tmpMap);
          tmpMap[BNRuleConstants.Single_PreV] = result;
          GlobalPrev.currentV = result;
          return result;
        }
      case RuleSingleMethodType.put:
        BookSourceCache.i.qnPut(
            model.bookId,
            (params[0] as RuleSingleParams).rule,
            params[1]
                .math(model, data, tmpMap, chapterModel, join, isQueryContent));
        return tmpMap[BNRuleConstants.Single_PreV]!;
      case RuleSingleMethodType.js:
        var rule = (params[0] as RuleSingleParams).rule;
        String register = '';
        if (model.detailUrl != emptyDetailUrl) {
          register = '$register let bookId = ${model.bookId};';
        }
        if (chapterModel != null) {
          register = "$register let chapterId=`${chapterModel.url}`;";
        }
        if (data != null && data is Map) {
          if (data.containsKey('page')) {
            register = '$register let page=${GlobalPrev.currentPage};';
          }
          if (data.containsKey('key')) {
            register = '$register let key=`${GlobalPrev.currentSearchKey}`;';
          }
          if (data.containsKey('tocUrl')) {
            register = '$register let tocUrl=`${data['tocUrl']}`;';
          }
          if (data.containsKey('contentUrl')) {
            register = '$register let contentUrl=`${data['contentUrl']}`;';
          }
        }
        register =
            '$register var result=`${tmpMap[BNRuleConstants.Single_PreV] ?? ''}`;';
        rule = '(function(){ $register $rule}())';
        var result = JSUtils.evaluateSync(rule);
        if (result is Map) {
          result = jsonEncode(result);
        }
        tmpMap[BNRuleConstants.Single_PreV] = result;
        GlobalPrev.currentV = result;
        return result;
      case RuleSingleMethodType.unknown:
        throw Exception('不存在unknown的场景');
    }
  }

  String replaceAll(RegExp regExp, List<String> keyList, String replaceRule,
      String preV, Map<String, String> tmpMap) {
    var temp = preV;
    var allMatch = regExp.allMatches(temp);

    int _start = 0;

    StringBuffer stringBuffer = StringBuffer();
    int length = temp.length;
    for (var match in allMatch) {
      // match.
      var start = match.start;
      var end = match.end;

      if (start > _start) {
        stringBuffer.write(temp.substring(_start, start));
      }

      // 待替换文本
      var matchValue = temp.substring(start, end);
      for (var element in keyList) {
        tmpMap[element] = match.namedGroup(element) ?? '';
      }
      tmpMap['\$0'] = matchValue;
      if (replaceRule.isEmpty) {
        stringBuffer.write('');
      } else {
        var tempResult = replaceRule;
        for (var entry in tmpMap.entries) {
          tempResult = tempResult.replaceAll(entry.key, entry.value);
        }
        stringBuffer.write(tempResult);
      }
      _start = end;
    }
    if (_start < length) {
      stringBuffer.write(temp.substring(_start));
    }
    return stringBuffer.toString();
  }

  String replaceSingle(
    RegExp regExp,
    List<String> keyList,
    String replaceRule,
    String preV,
    Map<String, String> tmpMap, {
    _FlagTemp? flag,
  }) {
    var temp = preV;
    var firstMatch = regExp.firstMatch(temp);
    if (firstMatch != null) {
      for (var element in keyList) {
        tmpMap[element] = firstMatch.namedGroup(element) ?? '';
      }
      var matchValue = temp.substring(firstMatch.start, firstMatch.end);
      tmpMap['\$0'] = matchValue;
      if (replaceRule.isEmpty) {
        temp = temp.replaceRange(firstMatch.start, firstMatch.end, '');
      } else {
        var tempResult = replaceRule;
        for (var entry in tmpMap.entries) {
          tempResult = tempResult.replaceAll(entry.key, entry.value);
        }
        temp = temp.replaceRange(firstMatch.start, firstMatch.end, tempResult);
      }
      if (flag != null) {
        flag.flag = true;
      }
    } else {
      if (flag != null) {
        flag.flag = false;
      }
    }
    return temp;
  }
}

class _FlagTemp {
  bool flag;

  _FlagTemp(this.flag);
}

class BNativeUtils {
  static Future<Response<List<int>>> qn_response_Network(
      String url, String? globalHeaders) async {
    Map<String, String> headerMap = {};
    if (globalHeaders != null && globalHeaders.trim().isNotEmpty) {
      try {
        Map map = jsonDecode(globalHeaders);
        headerMap.addAll(map
            .map((key, value) => MapEntry(key.toString(), value.toString())));
      } catch (_) {}
    }
    var indexOf = url.indexOf(',');
    if (indexOf == -1) {
      final response = await NetworkHelper.excutorRequestWithResponse(url,
          headers: headerMap);
      response.extra.putIfAbsent('qn_charset', () => 'utf8');
      return response;
    } else {
      var extraJson = url.substring(indexOf + 1);
      var jsonMap = json.decode(extraJson);
      final method = (jsonMap['m'] ?? jsonMap['method'] ?? 'get').toString();
      Map headers = jsonMap['h'] ?? jsonMap['headers'] ?? {};
      String requestData;
      if (jsonMap['d'] != null) {
        final d = jsonMap['d'];
        if (d is Map) {
          requestData = jsonEncode(d);
        } else {
          requestData = d.toString();
        }
      } else if (jsonMap['requestData'] != null) {
        final d = jsonMap['requestData'];
        if (d is Map) {
          requestData = jsonEncode(d);
        } else {
          requestData = d.toString();
        }
      } else {
        requestData = '';
      }
      bool isWebViewRequest;
      if (jsonMap['webview'] != null) {
        isWebViewRequest = jsonMap['webview'] == '1' || jsonMap['webview'] == 1;
      } else if (jsonMap['ext']?['webview'] != null) {
        isWebViewRequest = jsonMap['ext']?['webview'] == '1' ||
            jsonMap['ext']?['webview'] == 1;
      } else {
        isWebViewRequest = false;
      }
      int waitTime;
      if (jsonMap['waitTime'] != null) {
        waitTime = int.tryParse(jsonMap['waitTime'].toString()) ?? 500;
      } else if (jsonMap['ext']?['webviewTime'] != null) {
        waitTime =
            int.tryParse((jsonMap['ext']?['webviewTime'] ?? 0).toString()) ??
                500;
      } else {
        waitTime = 500;
      }

      bool needCookies;
      if (jsonMap['needCookies'] != null) {
        needCookies = jsonMap['needCookies'];
      } else if (jsonMap['ext']?['needCookies'] != null) {
        needCookies = jsonMap['ext']?['needCookies'] ?? false;
      } else {
        needCookies = false;
      }
      for (var entry in headers.entries) {
        headerMap[entry.key.toString()] = entry.value.toString();
      }
      final responseCharset =
          (jsonMap['rCharset'] ?? jsonMap['responseCharset'] ?? 'utf-8')
              .toString();
      var response = await NetworkHelper.excutorRequestWithResponse(
        url.substring(0, indexOf),
        method: method,
        headers: headerMap,
        requestData: requestData,
        responseCharset: responseCharset,
        isWebView: isWebViewRequest,
        waitTime: waitTime,
        needCookies: needCookies,
        cacheTime: jsonMap['cacheTime'] ?? 0,
      );
      response.extra['qn_charset'] = responseCharset;
      return response;
    }
  }

  static Future<String> qn_str_Network(
      String url, String? globalHeaders) async {
    final responseData = await qn_response_Network(url, globalHeaders);
    final charset = responseData.extra['qn_charset'];
    final data = (charset == 'gbk')
        ? gbk.decode(responseData.data ?? [], allowMalformed: true)
        : utf8.decode(responseData.data ?? [], allowMalformed: true);
    return data;
  }

  static List<dynamic> qn_queryList(
      {required String withRule,
      required dynamic node,
      required QNBookModel model,
      bool returnString = false}) {
    if (withRule.isEmpty) {
      return [];
    }
    bool reverse = false;
    if (withRule.startsWith("-")) {
      reverse = true;
      withRule = withRule.substring(1);
    }
    var splitRuleList = _splitRuleList(withRule);
    final List<dynamic> resultList = [];

    int length = splitRuleList.length;
    for (int i = 0; i < length; i++) {
      var m = splitRuleList[i];
      switch (m.type) {
        case RuleListSpitType.rule:
          resultList.addAll(_queryList(m.rule!, node, model, returnString));
          break;
        case RuleListSpitType.and:
          i = i + 1;
          m = splitRuleList[i];
          resultList.addAll(_queryList(m.rule!, node, model, returnString));
          break;
        case RuleListSpitType.or:
          if (resultList.isNotEmpty) {
            break;
          }
          i = i + 1;
          m = splitRuleList[i];
          resultList.addAll(_queryList(m.rule!, node, model, returnString));
          break;
        case RuleListSpitType.eachGet:
          throw UnimplementedError('%%不支持此类型操作');
      }
    }
    if (reverse) {
      return resultList.reversed.toList();
    } else {
      return resultList;
    }
  }

  static List<dynamic> _queryList(
      String withRule, dynamic node, QNBookModel model, bool returnString) {
    bool needReserve = false;
    if (withRule.startsWith(BNRuleConstants.List_Reserve)) {
      needReserve = true;
      withRule = withRule.substring(1);
    }
    List<dynamic> result;
    if (withRule.startsWith(r'$')) {
      result = _qn_JsonPathList(withRule, node, model, returnString);
    } else if (withRule.startsWith('/')) {
      result = _qn_XpathList(withRule, node, model, returnString);
    } else if (withRule.startsWith('css:')) {
      result = _qn_CSSList(withRule, node, model, returnString);
    } else {
      throw UnimplementedError('未实现的规则');
    }
    return needReserve ? result.reversed.toList(growable: false) : result;
  }

  static List<RuleListSplitModel> _splitRuleList(String rule) {
    var cache = BNRuleConstants.List_RuleCaches[rule];
    if (cache != null) {
      return cache;
    }

    final reg = BNRuleConstants.List_RulesReg;
    var allMatches = reg.allMatches(rule);
    int tmp = 0;
    final List<RuleListSplitModel> modelList = [];
    for (var m in allMatches) {
      if (tmp < m.start) {
        var substring = rule.substring(tmp, m.start);
        modelList.add(
            RuleListSplitModel(type: RuleListSpitType.rule, rule: substring));
      }
      var substring = rule.substring(m.start, m.end);
      if (substring == BNRuleConstants.List_And) {
        modelList.add(
            const RuleListSplitModel(type: RuleListSpitType.and, rule: null));
      } else if (substring == BNRuleConstants.List_Or) {
        modelList.add(
            const RuleListSplitModel(type: RuleListSpitType.or, rule: null));
      } else if (substring == BNRuleConstants.List_EachGet) {
        modelList.add(const RuleListSplitModel(
            type: RuleListSpitType.eachGet, rule: null));
      }
      tmp = m.end;
    }
    if (tmp != rule.length - 1) {
      modelList.add(RuleListSplitModel(
          type: RuleListSpitType.rule, rule: rule.substring(tmp)));
    }
    BNRuleConstants.List_RuleCaches[rule] = modelList;
    return modelList;
  }

  static List<dynamic> _qn_CSSList(
      String rule, dynamic node, QNBookModel model, bool returnString) {
    final newRule = rule.replaceFirst('css:', '').trim();

    dynamic doc;
    if (node is String) {
      doc = html_parse.parse(node);
    } else if (node is Element) {
      doc = node;
    } else {
      throw Exception('不支持的node格式');
    }
    List<Element> selectorAll =
        doc.querySelectorAll(newRule).toList(growable: false);
    if (returnString) {
      if (selectorAll.isNotEmpty) {
        var sublist = selectorAll.sublist(0, math.min(3, selectorAll.length));
        for (var item in sublist) {
          _DataHolder.addData(item.outerHtml, item);
        }
      }
      return selectorAll.map((e) => e.outerHtml).toList(growable: false);
    } else {
      return selectorAll;
    }
  }

  static String _qn_CssSingle(String rule, dynamic node, QNBookModel model,
      {String join = '', required bool isQueryContent}) {
    var newRule = rule.replaceFirst('css:', '').trim();

    String queryAttr = 'text';
    var lastIndexOf = newRule.lastIndexOf('@');
    if (lastIndexOf == -1) {
    } else {
      queryAttr = newRule.substring(lastIndexOf + 1);
      newRule = newRule.substring(0, lastIndexOf);
    }
    Element? element;
    if (node is String) {
      final holder = _DataHolder.getData(node);
      if (holder == null) {
        final doc = html_parse.parse(node);
        element = doc.querySelector(newRule);
      } else {
        if (holder is Element) {
          element = holder.querySelector(newRule);
        } else if (holder is HtmlNodeTree) {
          element = holder.element.querySelector(newRule);
        } else {
          throw Exception('不支持的holder类型  -- > $holder');
        }
      }
    } else if (node is Element) {
      element = node.querySelector(newRule);
    } else if (node is HtmlNodeTree) {
      element = node.element.querySelector(newRule);
    } else {
      throw Exception('不支持的node类型  -- > $node');
    }
    if (element == null) {
      return '';
    }
    if (queryAttr == 'text') {
      return element.text;
    } else {
      var attributes = element.attributes;

      return attributes[queryAttr] ?? '';
    }
  }

  static List<dynamic> _qn_XpathList(
      String rule, dynamic node, QNBookModel model, bool returnString) {
    final HtmlXPath htmlXPath;
    if (node is String) {
      htmlXPath = HtmlXPath.html(node);
    } else {
      htmlXPath = HtmlXPath.node(node);
    }
    var queryXPath = htmlXPath.queryXPath(rule);
    if (returnString) {
      final resultList = queryXPath.nodes.toList(growable: false);
      if (resultList.isNotEmpty) {
        var sublist = resultList.sublist(0, math.min(3, resultList.length));
        for (var item in sublist) {
          _DataHolder.addData((item as HtmlNodeTree).element.outerHtml, item);
        }
      }
      return resultList.map((e) {
        var n = e.node;
        if (n is Element) {
          return n.outerHtml;
        } else {
          return '';
        }
      }).toList();
    } else {
      return queryXPath.nodes.toList(growable: false);
    }
  }

  static List<dynamic> _qn_JsonPathList(
      String rule, dynamic jsonMap, QNBookModel model, bool returnString) {
    dynamic decodeMap;
    if (jsonMap is String) {
      if (jsonMap.startsWith('[') || jsonMap.startsWith('{')) {
        decodeMap = json.decode(jsonMap);
      } else {
        return [];
      }
    } else {
      decodeMap = jsonMap;
    }
    var values = JsonPath(rule).readValues(decodeMap);
    if (returnString) {
      return values.map((e) {
        if (e == null) {
          return "{}";
        } else {
          return json.encode(e);
        }
      }).toList(growable: false);
    } else {
      return values.toList(growable: false);
    }
  }

  /// -------------

  ///  https://www.baidu.com_{{$.id}}
  ///
  /// $.id,,,@put(bid,1),,,#.*#,{}
  ///
  /// https://www.baidu.com_{{//div[@class=\"test\"]/text()}}
  /// http://www.baidu.com/{{qbird.gbk($.id)}},,,#.*##,test
  ///
  /// #.*#{{$0}},,,htt

  static String qn_querySingle(
      {required String? withRule,
      required dynamic node,
      required QNBookModel model,
      String join = '',
      QNTocInnerModel? chapterModel,
      required Map<String, String> addParams}) {
    if (withRule == null) {
      return '';
    }
    withRule = withRule.trim();
    if (withRule.isEmpty) {
      return '';
    }
    final ruleList = _analySingleRuleList(withRule, node, model, chapterModel);
    return handlerRules(ruleList, node, model,
        join: join, chapterModel: chapterModel, addParams: addParams);
  }

  static String qn_queryContent(
      {required String? withRule,
      required dynamic node,
      required QNBookModel model,
      String join = '\n',
      QNTocInnerModel? chapterModel,
      addParams = const <String, String>{}}) {
    if (withRule == null) {
      return '';
    }
    withRule = withRule.trim();
    if (withRule.isEmpty) {
      return '';
    }
    final ruleList = _analySingleRuleList(withRule, node, model, chapterModel);
    return handlerRules(ruleList, node, model,
        join: join,
        chapterModel: chapterModel,
        isQueryContent: true,
        addParams: addParams);
  }

  static String handlerRules(
      List<RuleSingleMethod> rules, dynamic node, QNBookModel model,
      {String? join,
      QNTocInnerModel? chapterModel,
      bool isQueryContent = false,
      Map<String, String> addParams = const <String, String>{}}) {
    Map<String, String> tempMap = {BNRuleConstants.Single_PreV: ''};
    tempMap.addAll(addParams);
    String result = '';
    int length = rules.length;
    for (int i = 0; i < length; i++) {
      var rule = rules[i];
      result =
          rule.math(model, node, tempMap, chapterModel, join, isQueryContent);
      if (rule.method == RuleSingleMethodType.emptyReturn) {
        if (result.isEmpty) {
          break;
        }
      }
      if (i != length - 1) {
        tempMap[BNRuleConstants.Single_PreV] = result;
        GlobalPrev.currentV = result;
      }
    }
    GlobalPrev.currentV = '';
    return result;
  }

  static List<RuleSingleMethod> _analySingleRuleList(String withRule,
      dynamic node, QNBookModel model, QNTocInnerModel? chapterModel) {
    final cache = BNRuleConstants.Single_RuleCaches[withRule];
    if (cache != null) {
      // return cache;
    }
    List<RuleSingleMethod> partList = [];
    var partArray = withRule.split(',,,');
    for (var part in partArray) {
      part = part.trim();
      if (part.isEmpty) {
        continue;
      }

      /// 分段解析
      if (part.startsWith('#')) {
        // 正则规则
        //  #正则#替换#， #正则#替换## 全部替换 ，多个正则 以 && 相连
        final s = part;
        RuleSingleMethod singleMethod =
            RuleSingleMethod(method: RuleSingleMethodType.reg);
        partList.add(singleMethod);
        var firstStart = s.indexOf('#');
        var spitIndex = s.indexOf('#', firstStart + 1);
        var regRule = s.substring(firstStart + 1, spitIndex);
        var endIndex = s.indexOf('#', spitIndex + 1);
        var replaceRule = s.substring(spitIndex + 1, endIndex);

        singleMethod.addPrams(RuleSingleParams(
            type: (endIndex == s.length - 2 && s[endIndex + 1] == '#')
                ? RuleSingleParamsType.regAll
                : RuleSingleParamsType.regSingle,
            rule: ''));
        {
          RuleSingleMethodParams param = RuleSingleMethodParams();
          param.singleMethod = _analyStringRule(regRule);
          singleMethod.addPrams(param);
        }
        {
          RuleSingleMethodParams param = RuleSingleMethodParams();
          param.singleMethod = _analyStringRule(replaceRule);
          singleMethod.addPrams(param);
        }
      } else if (part.startsWith('/') ||
          part.startsWith('\$') ||
          part.startsWith('css:')) {
        var split = part.split('||').map((e) => e.trim());
        RuleSingleMethod singleMethod =
            RuleSingleMethod(method: RuleSingleMethodType.or);
        partList.add(singleMethod);
        for (var s in split) {
          if (s.startsWith('/')) {
            singleMethod.addPrams(RuleSingleParams(
              type: RuleSingleParamsType.xpath,
              rule: s,
            ));
          } else if (s.startsWith('\$')) {
            singleMethod.addPrams(RuleSingleParams(
              type: RuleSingleParamsType.jsonPath,
              rule: s,
            ));
          } else if (s.startsWith('css:')) {
            singleMethod.addPrams(RuleSingleParams(
              type: RuleSingleParamsType.css,
              rule: s,
            ));
          } else {
            if (s.contains('{{')) {
              RuleSingleMethodParams param = RuleSingleMethodParams();
              param.singleMethod = _analyStringRule(s);
              singleMethod.addPrams(param);
            } else {
              singleMethod.addPrams(RuleSingleParams(
                type: RuleSingleParamsType.string,
                rule: s,
              ));
            }
          }
        }
      } else if (part.startsWith('@put')) {
        RuleSingleMethod singleMethod =
            RuleSingleMethod(method: RuleSingleMethodType.put);
        partList.add(singleMethod);
        var indexOf = part.indexOf('(');
        var indexOf2 = part.indexOf(',', indexOf);
        var key = part.substring(indexOf + 1, indexOf2).trim();
        singleMethod.addPrams(
            RuleSingleParams(type: RuleSingleParamsType.string, rule: key));
        var lastIndexOf = part.lastIndexOf(")");
        var value = part.substring(indexOf2 + 1, lastIndexOf);
        if (value.startsWith('/')) {
          singleMethod.addPrams(
              RuleSingleParams(type: RuleSingleParamsType.xpath, rule: value));
        } else if (value.startsWith('\$')) {
          singleMethod.addPrams(RuleSingleParams(
              type: RuleSingleParamsType.jsonPath, rule: value));
        } else if (value.startsWith('css:')) {
          singleMethod.addPrams(
              RuleSingleParams(type: RuleSingleParamsType.css, rule: value));
        } else {
          singleMethod.addPrams(
              RuleSingleParams(type: RuleSingleParamsType.string, rule: value));
        }
      } else if (part.startsWith('@emptyReturn')) {
        // 字符串
        // 开始判断表达式
        partList
            .add(RuleSingleMethod(method: RuleSingleMethodType.emptyReturn));
      } else if (part.startsWith('@js:')) {
        var jsMethod = RuleSingleMethod(method: RuleSingleMethodType.js);
        jsMethod.addPrams(RuleSingleParams(
            type: RuleSingleParamsType.js, rule: part.substring(4)));
        partList.add(jsMethod);
      } else {
        partList.add(_analyStringRule(part));
      }
    }
    BNRuleConstants.Single_RuleCaches[withRule] = partList;
    return partList;
  }

  static RuleSingleMethod _analyStringRule(String part) {
    var allMatches = BNRuleConstants.Single_Math_Reg.allMatches(part);
    RuleSingleMethod singleMethod =
        RuleSingleMethod(method: RuleSingleMethodType.add);
    int tmp = 0;
    for (var m in allMatches) {
      if (tmp < m.start) {
        var substring = part.substring(tmp, m.start);
        singleMethod.addPrams(RuleSingleParams(
            type: RuleSingleParamsType.string, rule: substring));
      }
      var substring = part.substring(m.start + 2, m.end - 2);
      if (substring.startsWith('/')) {
        singleMethod.addPrams(RuleSingleParams(
            type: RuleSingleParamsType.xpath, rule: substring));
      } else if (substring.startsWith('css:')) {
        singleMethod.addPrams(
            RuleSingleParams(type: RuleSingleParamsType.css, rule: substring));
      } else if (substring.startsWith('\$')) {
        singleMethod.addPrams(RuleSingleParams(
            type: RuleSingleParamsType.jsonPath, rule: substring));
      } else if (substring.startsWith('QB.')) {
        singleMethod.addPrams(_analyQBMethod(substring));
      } else {
        singleMethod.addPrams(RuleSingleParams(
            type: RuleSingleParamsType.string, rule: substring));
      }
      tmp = m.end;
    }
    if (tmp != part.length) {
      singleMethod.addPrams(RuleSingleParams(
          type: RuleSingleParamsType.string, rule: part.substring(tmp)));
    }
    return singleMethod;
  }

  static RuleSingleNativeFuncParams _analyQBMethod(String qbRule) {
    var indexOf = qbRule.indexOf('.');
    var indexOf2 = qbRule.indexOf('(');
    var method = qbRule.substring(indexOf + 1, indexOf2).trim();
    var methodParams = RuleSingleNativeFuncParams(method);
    var lastIndexOf = qbRule.lastIndexOf(')');
    var paramsStr = qbRule.substring(indexOf2 + 1, lastIndexOf);

    if (paramsStr.contains("QB")) {
      List<String> posList = [];
      int pos = 0;
      while (pos != -1) {
        final newPos = paramsStr.indexOf("QB", pos);
        if (newPos == -1) {
          posList.add('s${pos}|${paramsStr.length}');
          break;
        } else {
          // paramsStr.substring(pos,newPos);
          posList.add('s${pos}|${newPos}');
          pos = newPos;
          var pos2 = paramsStr.indexOf(")", newPos) + 1;
          var tempStr = paramsStr.substring(pos, pos2);
          var count1 = countSingleWord(tempStr, ")");
          var count2 = countSingleWord(tempStr, "(");

          while (count1 != count2) {
            pos2 = paramsStr.indexOf(")", pos2) + 1;
            tempStr = paramsStr.substring(pos, pos2);
            count1 = countSingleWord(tempStr, ")");
            count2 = countSingleWord(tempStr, "(");
          }
          posList.add('m${pos}|${pos2}');
          pos = pos2;
        }
      }
      for (var p in posList) {
        if (p.startsWith('m')) {
          final args =
              p.substring(1).split('|').map((e) => int.parse(e)).toList();
          methodParams
              .addParam(_analyQBMethod(paramsStr.substring(args[0], args[1])));
        } else if (p.startsWith("s")) {
          final args =
              p.substring(1).split('|').map((e) => int.parse(e)).toList();
          var ss = paramsStr.substring(args[0], args[1]).split(',,');
          for (var sss in ss) {
            var sTem = sss.trim();
            if (sTem.startsWith('/')) {
              methodParams.addParam(RuleSingleParams(
                  type: RuleSingleParamsType.xpath, rule: sTem));
            } else if (sTem.startsWith('\$')) {
              methodParams.addParam(RuleSingleParams(
                  type: RuleSingleParamsType.jsonPath, rule: sTem));
            } else if (sTem.startsWith('css:')) {
              methodParams.addParam(
                  RuleSingleParams(type: RuleSingleParamsType.css, rule: sTem));
            } else {
              methodParams.addParam(RuleSingleParams(
                  type: RuleSingleParamsType.string, rule: sss));
            }
          }
        } else {
          Logger.error("不正确的场景 ->${p}");
        }
      }

      return methodParams;
    } else {
      // "A,,B,,C,,D"
      var ss = paramsStr.split(',,');
      for (var sss in ss) {
        var sTem = sss.trim();
        if (sTem.startsWith('/')) {
          methodParams.addParam(
              RuleSingleParams(type: RuleSingleParamsType.xpath, rule: sTem));
        } else if (sTem.startsWith('css:')) {
          methodParams.addParam(
              RuleSingleParams(type: RuleSingleParamsType.css, rule: sTem));
        } else if (sTem.startsWith('\$')) {
          methodParams.addParam(RuleSingleParams(
              type: RuleSingleParamsType.jsonPath, rule: sTem));
        } else {
          methodParams.addParam(
              RuleSingleParams(type: RuleSingleParamsType.string, rule: sss));
        }
      }
      return methodParams;
    }
  }

  static int countSingleWord(String str, String s) {
    int count = 0;
    int tmp = 0;
    while (tmp != -1) {
      tmp = str.indexOf(s, tmp);
      if (tmp >= 0) {
        count++;
        tmp++;
      }
    }
    return count;
  }

  static String _qn_XpathSingle(String rule, dynamic node, QNBookModel model,
      {String join = '', required bool isQueryContent}) {
    if (rule == '/') {
      return '/';
    }
    final HtmlXPath htmlXPath;
    if (node is String) {
      final holder = _DataHolder.getData(node);

      if (holder == null) {
        htmlXPath = HtmlXPath.node(html_parse.parseFragment(node));
      } else {
        if (holder is HtmlNodeTree) {
          htmlXPath = HtmlXPath(holder);
        } else if (holder is Element) {
          // css 兼容
          htmlXPath = HtmlXPath.node(holder);
        } else {
          throw Exception('不支持的类型 -> $node');
        }
      }
    } else if (node is HtmlNodeTree) {
      htmlXPath = HtmlXPath(node);
    } else if (node is Element) {
      // css 兼容
      htmlXPath = HtmlXPath.node(node);
    } else {
      throw Exception('不支持的类型 -> $node');
    }
    if (isQueryContent) {
      var queryXPath = htmlXPath.queryXPath(rule);
      if (rule.contains('/@') || rule.contains('/text()')) {
        final String result;
        if (queryXPath.nodes.length > 1) {
          result = queryXPath.attrs.join(join);
        } else {
          result = queryXPath.attr ?? '';
        }
        return result;
      } else {
        List<String> sList = [];

        for (var n in queryXPath.nodes) {
          final childList = n.node.nodes;
          for (var c in childList) {
            sList.add(c.text ?? '');
          }
        }
        return sList.join(join);
      }
    } else {
      var queryXPath = htmlXPath.queryXPath(rule);
      final String result;
      if (queryXPath.nodes.length > 1) {
        result = queryXPath.attrs.join(join);
      } else {
        result = queryXPath.attr ?? '';
      }
      return result;
    }
  }

  static String _qn_JsonPathSingle(
      String rule, dynamic jsonMap, QNBookModel model,
      {String join = ""}) {
    if (jsonMap is String) {
      if (jsonMap == '') {
        return '';
      }
      var jsonD = jsonDecode(jsonMap);
      return JsonPath(rule).readValues(jsonD).join(join);
    } else {
      return JsonPath(rule).readValues(jsonMap).join(join);
    }
  }
}

class _DataHolder {
  static final Map<int, dynamic> _holder = {};

  static void addData(String html, dynamic data) {
    final id = HashM.hashCodeGet(html);
    _holder[id] = data;
  }

  static dynamic getData(String html) {
    final id = HashM.hashCodeGet(html);
    return _holder[id];
  }
}
