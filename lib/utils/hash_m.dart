class HashM {
  static final Map<String, int> hashCodeMap = {};

  static int hashCodeGet(String key) {
    if (key.isEmpty) {
      return 0;
    }
    var hashCode = hashCodeMap[key];
    if (hashCode != null) {
      return hashCode;
    }
    var codeUnits = key.codeUnits;
    var length = codeUnits.length;
    var h = 0;
    for (int i = 0; i < length; i++) {
      h = 31 * h + codeUnits[i];
    }
    h = h % 0xFFFFFFFF;
    hashCodeMap[key] = h;
    return h;
  }
}
