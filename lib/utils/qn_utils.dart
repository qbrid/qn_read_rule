import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:basic_utils/basic_utils.dart';
import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart';
import 'package:dart_des/dart_des.dart';
import 'package:encrypt/encrypt.dart';
import 'package:fast_gbk/fast_gbk.dart' as fastgbk;
import 'package:qn_read_rule/utils/logger.dart';

typedef AppInnerHandler = dynamic Function(List<dynamic> args);

class QNUtilsProxy {
  /// 异步方法集合
  static final Map<String, AppInnerHandler> asyncHandlerMap = {};

  /// 同步方法集合
  static final Map<String, AppInnerHandler> syncHandlerMap = {
    'timestamp': (List<dynamic> args) =>
        DateTime.now().millisecondsSinceEpoch.toString(),

    /// gbk 编码， 输入为 字符串， 返回字符串 hex16
    "gbk": (List<dynamic> args) => AppInnerUtils.gbk(args[0]),

    /// utf8 编码， 输入为 字符串， 返回字符串 hex16
    "utf8": (List<dynamic> args) => AppInnerUtils.utf(args[0]),

    /// utf8 编码，输入为 字符串， 返回 bytes数组
    "utf8_bytes": (List<dynamic> args) => AppInnerUtils.utf8Bytes(args[0]),

    /// utf8 解码，输入为 bytes数字， 返回 字符串
    "utf8_decode": (List<dynamic> args) {
      if (args[0] is List<int>) {
        return AppInnerUtils.utf8Decode(args[0]);
      } else {
        return AppInnerUtils.utf8Decode(
            (args[0] as List<dynamic>).map((e) => e as int).toList());
      }
    },

    /// utf8 解码，输入为 字符串， 返回 bas64编码
    "base64": (List<dynamic> args) => AppInnerUtils.base64(args[0]),

    /// md5 签名，输入为 字符串， 返回 md5值
    "md5": (List<dynamic> args) => AppInnerUtils.md5String(args[0]),

    /// url编码 ，输入为 字符串， 返回 字符串
    "urlEncode": (List<dynamic> args) => AppInnerUtils.urlEncode(args[0]),

    /// 执行2次url编码 ，输入为 字符串， 返回 字符串
    "urlEncode2": (List<dynamic> args) => AppInnerUtils.urlEncode2(args[0]),

    /// 加法，支持多个字符串，返回字符串，类比  1 + 2 + 3 ，返回 字符串 6
    "plus": (List<dynamic> args) => AppInnerUtils.add(args),

    /// 乘法 ，支持多个字符串，返回字符串，类比  1 * 2 * 3 ，返回 字符串 6
    "mult": (List<dynamic> args) => AppInnerUtils.mult(args),

    /// 类似三目运算符。 args[0] == args[1] ? '' : args[0]，当两值相关，返回''，否则返回args[0]
    "emptyOr": (List<dynamic> args) => AppInnerUtils.emptyOr(args[0], args[1]),

    /// 连接，可实现 左右连接，如 QB.connect(A,,B) 输出 字符串 AB
    "connect": (List<dynamic> args) => AppInnerUtils.connect(args[0], args[1]),

    "slice": (List<dynamic> args) => AppInnerUtils.slice(args[0], args[1]),

    /// unicode解码 ，输入字符串，输出字符串
    "unicode_decode": (List<dynamic> args) =>
        AppInnerUtils.unicodeDecode(args[0]),

    /// base解码 ，输入字符串，输出字符串（utf8）
    "base64Decode": (List<dynamic> args) =>
        AppInnerUtils.base64DecodeUTF8(args[0]),

    /// 对称解密（DESede） ，输入 参数1：加密字符，参数2：秘钥，参数3：iv  输出：字符串(utf8)
    "DESede": (List<dynamic> args) =>
        AppInnerUtils.DESedeDecode(args[0], args[1], args[2]),

    /// 非对称加密（rsa） ，输入 参数1：待加密字符，参数2：秘钥，参数3：类型，支持 PKCS1与OAEP   输出：bytes数组
    "rsaEncrypt": (List<dynamic> args) {
      if (args[1] is List<int>) {
        return AppInnerUtils.rsaEncrypt(args[0], args[1], args[2]);
      } else {
        return AppInnerUtils.rsaEncrypt(args[0],
            (args[1] as List<dynamic>).map((e) => e as int).toList(), args[2]);
      }
    },

    // bytes转Hex输出
    "hexCode": (List<dynamic> args) {
      if (args[0] is List<int>) {
        return AppInnerUtils.hexCode(args[0]);
      } else {
        return AppInnerUtils.hexCode(
            (args[0] as List<dynamic>).map((e) => e as int).toList());
      }
    },

    /// url解密
    "urlDecode": (List<dynamic> args) {
      return AppInnerUtils.urlDecode(args[0]);
    },

    /// aes加密，String source,String key,String iv,String padding,String mode
    /// 参数0 待加密字符串
    /// 参数1 加密Key
    /// 参数2 加密IV
    /// 参数3 加密Padding类型，例如 PKCS5 / PKCS7
    /// 参数4 加密模式,例如 ecb,sic
    "aesEncrypt": (List<dynamic> args) {
      return AppInnerUtils.aesEncrypt(
          args[0], args[1], args[2], args[3], args[4]);
    },

    /// aes解密，String source,String key,String iv,String padding,String mode
    /// 参数0 加密字符串
    /// 参数1 解密Key
    /// 参数2 解密IV
    /// 参数3 解密Padding类型，例如 PKCS5 / PKCS7
    /// 参数4 解密模式,例如 ecb,sic
    "aesDecrypt": (List<dynamic> args) {
      return AppInnerUtils.aesDecrypt(
          args[0], args[1], args[2], args[3], args[4]);
    },
  };

  static dynamic handlerSync(String method, List<dynamic> args) {
    var handler = syncHandlerMap[method];
    if (handler == null) {
      throw Exception("不支持的同步方法 ->$method");
    }
    return handler(args);
  }

  static Future<String> handlerASync(String method, List<dynamic> args) {
    var handler = asyncHandlerMap[method];
    if (handler == null) {
      throw Exception("不支持的异步方法 ->$method");
    }
    return handler(args);
  }
}

class AppInnerUtils {
  static String aesEncrypt(
      String source, dynamic key, dynamic iv, String padding, String mode) {
    var kkey = (key is String)
        ? Key.fromUtf8(key)
        : Key(Uint8List.fromList((key as List).map((e) => e as int).toList()));
    var iiv = (iv is String)
        ? IV.fromUtf8(iv)
        : IV(Uint8List.fromList((iv as List).map((e) => e as int).toList()));
    final AESMode aesMode;
    if (mode == 'sic') {
      aesMode = AESMode.sic;
    } else if (mode == 'cbc') {
      aesMode = AESMode.cbc;
    } else if (mode == 'cfb64') {
      aesMode = AESMode.cfb64;
    } else if (mode == 'ctr') {
      aesMode = AESMode.ctr;
    } else if (mode == 'ecb') {
      aesMode = AESMode.ecb;
    } else if (mode == 'ofb64Gctr') {
      aesMode = AESMode.ofb64Gctr;
    } else if (mode == 'ofb64') {
      aesMode = AESMode.ofb64;
    } else {
      return '不支持的mode';
    }
    // final padding = 'PKCS7';
    var newEncrypter = Encrypter(AES(kkey, mode: aesMode, padding: padding));
    return newEncrypter.encrypt(source, iv: iiv).base64;
  }

  static String aesDecrypt(
      String source, dynamic key, dynamic iv, String padding, String mode) {
    var kkey = (key is String)
        ? Key.fromUtf8(key)
        : Key(Uint8List.fromList((key as List).map((e) => e as int).toList()));
    var iiv = (iv is String)
        ? IV.fromUtf8(iv)
        : IV(Uint8List.fromList((iv as List).map((e) => e as int).toList()));
    final AESMode aesMode;
    if (mode == 'sic') {
      aesMode = AESMode.sic;
    } else if (mode == 'cbc') {
      aesMode = AESMode.cbc;
    } else if (mode == 'cfb64') {
      aesMode = AESMode.cfb64;
    } else if (mode == 'ctr') {
      aesMode = AESMode.ctr;
    } else if (mode == 'ecb') {
      aesMode = AESMode.ecb;
    } else if (mode == 'ofb64Gctr') {
      aesMode = AESMode.ofb64Gctr;
    } else if (mode == 'ofb64') {
      aesMode = AESMode.ofb64;
    } else {
      return '不支持的mode';
    }
    var newEncrypter = Encrypter(AES(kkey, mode: aesMode, padding: padding));
    return newEncrypter.decrypt(Encrypted.fromBase64(source), iv: iiv);
  }

  static String gbk(String v) {
    var list = fastgbk.gbk.encode(v);
    var length = list.length;
    var result = '';
    for (int i = 0; i < length; i++) {
      result = '$result%${list[i].toRadixString(16).toUpperCase()}';
    }
    return result;
  }

  static String utf(String v) {
    var list = utf8.encode(v);
    var length = list.length;
    var result = '';
    for (int i = 0; i < length; i++) {
      result = '$result%${list[i].toRadixString(16).toUpperCase()}';
    }
    return result;
  }

  static List<int> utf8Bytes(String v) {
    var list = utf8.encode(v);
    return list;
  }

  static String base64(List<int> bytes) {
    return base64Encode(bytes);
  }

  static String md5String(String v) {
    var content = utf8.encode(v);
    var digest = md5.convert(content);
    // 这里其实就是 digest.toString()
    return hex.encode(digest.bytes);
  }

  static String urlEncode2(String value) {
    var component = Uri.encodeComponent(value);
    return Uri.encodeComponent(component);
  }

  static String urlEncode(String value) {
    return Uri.encodeComponent(value);
  }

  static String add(List<dynamic> args) {
    int a = 1;
    return args
        .map((e) => int.parse(e.toString().trim()))
        .fold(0, (previousValue, element) => previousValue + element)
        .toString();
  }

  static String unicodeDecode(String value) {
    var split = value.split("&#x");
    StringBuffer stringBuffer = StringBuffer();
    for (var element in split) {
      element = element.replaceAll('&ldquo;', "『").replaceAll("&rdquo;", "』");
      var indexOf = element.indexOf(';');
      if (indexOf < 0) {
        stringBuffer.write(element);
      } else {
        var substring = element.substring(0, indexOf);
        try {
          stringBuffer
              .write(String.fromCharCode(int.parse(substring, radix: 16)));
        } catch (_) {
          stringBuffer.write(substring);
          Logger.debug(() => "解析失败 ->$element");
        }
        // 转码;
        if (indexOf < element.length - 1) {
          stringBuffer.write(element.substring(indexOf + 1));
        }
      }
    }
    var result = stringBuffer.toString();
    // LogUtil.debug(() => "result ->$result");
    return result;
  }

  static String mult(List<dynamic> args) {
    num initV = 1;
    var result = args
        .map((e) => num.parse(e.toString().trim()))
        .fold(initV, (previousValue, element) => previousValue * element);
    if (result % 1 == 0) {
      return result.toInt().toString();
    } else {
      return result.toDouble().toString();
    }
  }

  static String base64DecodeUTF8(String base64) {
    final result = utf8.decode(
        base64Decode(
          base64,
        ),
        allowMalformed: true);
    return result;
  }

  // static String DESedeEncode(String source, String key, String iv) {
  //   // var uint8list = const Base64Decoder().convert(source);
  //   // var list = uint8list.toList();
  //   DES3 desECB = DES3(
  //       key: utf8.encode(key),
  //       mode: DESMode.ECB,
  //       iv: utf8.encode(iv),
  //       paddingType: DESPaddingType.PKCS7);
  //   var encrypt = desECB.encrypt(utf8Bytes(source));
  //   return base64Encode(encrypt);
  // }

  static String DESedeDecode(String source, String key, String iv) {
    var uint8list = const Base64Decoder().convert(source);
    var list = uint8list.toList();
    DES3 desECB = DES3(
        key: utf8.encode(key),
        mode: DESMode.ECB,
        iv: utf8.encode(iv),
        paddingType: DESPaddingType.PKCS7);
    var decrypt = desECB.decrypt(list);
    return utf8.decode(decrypt);
  }

  static String DESedeEncode(String text, String key, String iv) {
    DES3 desECB = DES3(
        key: utf8.encode(key),
        mode: DESMode.ECB,
        iv: utf8.encode(iv),
        paddingType: DESPaddingType.PKCS7);
    var encrypt = desECB.encrypt(utf8Bytes(text));
    return const Base64Encoder().convert(encrypt);
  }

  static List<int> rsaEncrypt(String content, List<int> keys, String type) {
    final key = CryptoUtils.rsaPublicKeyFromDERBytes(Uint8List.fromList(keys));
    final pem = CryptoUtils.encodeRSAPublicKeyToPemPkcs1(key);
    final publicKey = RSAKeyParser().parse(pem) as RSAPublicKey;
    final encrypter = Encrypter(
      RSA(
          publicKey: publicKey,
          encoding: type == 'PKCS1' ? RSAEncoding.PKCS1 : RSAEncoding.OAEP),
    );
    final encrypted = encrypter.encryptBytes(utf8.encode(content));
    return encrypted.bytes;
  }

  static String utf8Decode(List<int> list) {
    return utf8.decode(list);
  }

  static String urlDecode(String v) {
    return Uri.decodeComponent(v);
  }

  static String emptyOr(String v, String key) {
    return v == key ? '' : v;
  }

  static connect(String v, String key) {
    return '$v$key';
  }

  static String hexCode(List<int> bytes) {
    return hex.encode(bytes);
  }

  static String hexDeCode(String encode) {
    var list = hex.decode(encode);
    return utf8.decode(list, allowMalformed: true);
  }

  static String slice(String v, String lengthS) {
    final length = int.tryParse(lengthS) ?? 0;
    if (length == 0) {
      return v;
    }
    if (length == 1) {
      return v.substring(v.length - 1);
    }
    var newString = v.substring(
      v.length - length,
    );
    int a = int.tryParse(newString) ?? 0;
    return a.toString();
  }

// static String aesEncode(
//     String text, String key, String mode, String padding) {
//   var kk = Key.fromUtf8(key);
//   final AESMode aesMode;
//   switch (mode) {
//     case 'ecb':
//       aesMode = AESMode.ecb;
//       break;
//     case 'cbc':
//       aesMode = AESMode.cbc;
//       break;
//     case 'cfb64':
//       aesMode = AESMode.cfb64;
//       break;
//     case 'ctr':
//       aesMode = AESMode.ctr;
//       break;
//     case 'ofb64Gctr':
//       aesMode = AESMode.ofb64Gctr;
//       break;
//     case 'ofb64':
//       aesMode = AESMode.ofb64;
//       break;
//     case 'sic':
//       aesMode = AESMode.sic;
//       break;
//     default:
//       aesMode = AESMode.ecb;
//   }
//   final encrypter = Encrypter(AES(kk, mode: aesMode, padding: padding));
//   var encrypt = encrypter.encrypt(text);
//   return encrypt.base64;
// }
//
// static String aesDecode(
//     String base64, String key, String mode, String padding) {
//   var kk = Key.fromUtf8(key);
//   final AESMode aesMode;
//   switch (mode) {
//     case 'ecb':
//       aesMode = AESMode.ecb;
//       break;
//     case 'cbc':
//       aesMode = AESMode.cbc;
//       break;
//     case 'cfb64':
//       aesMode = AESMode.cfb64;
//       break;
//     case 'ctr':
//       aesMode = AESMode.ctr;
//       break;
//     case 'ofb64Gctr':
//       aesMode = AESMode.ofb64Gctr;
//       break;
//     case 'ofb64':
//       aesMode = AESMode.ofb64;
//       break;
//     case 'sic':
//       aesMode = AESMode.sic;
//       break;
//     default:
//       aesMode = AESMode.ecb;
//   }
//   final encrypter = Encrypter(AES(kk, mode: aesMode, padding: padding));
//   return encrypter.decrypt64(base64);
// }
}
