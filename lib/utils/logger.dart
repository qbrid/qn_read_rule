import 'package:flutter/foundation.dart';

typedef LazyDone = String Function();

typedef LoggerInput = String Function();

typedef ViewLogChangeListener = void Function();

String defaultTag = 'QNRule';

class Logger {
  static final List<LogInterface> _logList = [];

  static void addLog(LogInterface log) {
    _logList.add(log);
  }

  static void removeLog(LogInterface log) {
    int length = _logList.length;
    for (var i = 0; i < length; i++) {
      var l = _logList[i];
      if (l.name == log.name) {
        _logList.removeAt(i);
        return;
      }
    }
  }

  static void debug(LoggerInput input, {String? tag}) {
    for (var element in _logList) {
      element.debug(input, tag: tag);
    }
  }

  static void error(Object object, {String? tag}) {
    for (var element in _logList) {
      element.error(object, tag: tag);
    }
  }

  static void errorStack(Object error, StackTrace? stack, {String? tag}) {
    for (var element in _logList) {
      element.errorStack(error, stack, tag: tag);
    }
  }
}

abstract class LogInterface {
  String get name;

  void debug(LoggerInput input, {String? tag});

  void error(Object object, {String? tag});

  void errorStack(Object error, StackTrace? stack, {String? tag});
}

class ViewLogModel {
  final int time;

  final int level;

  final String tag;

  final String msg;

  final StackTrace? stackTrace;

  const ViewLogModel(
      {required this.time,
      required this.level,
      required this.tag,
      required this.msg,
      this.stackTrace});
}

class ViewLog extends LogInterface {
  static final ViewLog _log = ViewLog();

  static ViewLog get i => _log;

  final List<ViewLogModel> _list = [];

  List<ViewLogModel> get list => _list;

  final List<ViewLogChangeListener> _notifyList = [];

  void clear() {
    _list.clear();
  }

  void addListen(ViewLogChangeListener listener) {
    _notifyList.add(listener);
  }

  void removeListen(ViewLogChangeListener listener) {
    _notifyList.remove(listener);
  }

  void _notify() {
    _notifyList.forEach((element) {
      element();
    });
  }

  @override
  void debug(LoggerInput input, {String? tag}) {
    // TODO: implement debug
    _list.add(ViewLogModel(
        time: DateTime.now().millisecondsSinceEpoch,
        level: 1,
        tag: tag ?? defaultTag,
        msg: input()));
    _notify();
  }

  @override
  void error(Object object, {String? tag}) {
    // TODO: implement error
    _list.add(ViewLogModel(
        time: DateTime.now().millisecondsSinceEpoch,
        level: 2,
        tag: tag ?? defaultTag,
        msg: object.toString()));
    _notify();
  }

  @override
  void errorStack(Object error, StackTrace? stack, {String? tag}) {
    // TODO: implement errorStack
    _list.add(ViewLogModel(
        time: DateTime.now().millisecondsSinceEpoch,
        level: 2,
        tag: tag ?? defaultTag,
        msg: error.toString(),
        stackTrace: stack));
    _notify();
  }

  @override
  // TODO: implement name
  String get name => "ViewLog";
}

class CLog extends LogInterface {
  CLog();

  static String sTag = "#SiteSourceLogger#";

  @override
  String get name => 'ConsoleLog';

  @override
  void debug(LoggerInput input, {String? tag}) {
    var logData = input();
    _printLog(tag, " debug ", logData);
  }

  @override
  void error(Object object, {String? tag}) {
    _printLog(tag, " error ", object);
  }

  @override
  void errorStack(Object error, StackTrace? stack, {String? tag}) {
    _printLog(tag, " error ", 'AppCatchError message:$error,stack$stack');
  }

  static void _printLog(String? tag, String separator, Object obj) {
    tag = tag ?? sTag;
    String objString = obj.toString();

    while (objString.isNotEmpty) {
      if (objString.length > 10240) {
        if (kDebugMode) {
          debugPrint(
              "$tag ${DateTime.now().toIso8601String()} $separator ${objString.substring(0, 10240)}");
        }
        objString = objString.substring(10240, objString.length);
      } else {
        if (kDebugMode) {
          debugPrint(
              "$tag ${DateTime.now().toIso8601String()} $separator $objString");
        }
        break;
      }
    }
  }
}

class FileLog extends LogInterface {
  @override
  // TODO: implement name
  String get name => 'FileLog';

  @override
  void debug(LoggerInput input, {String? tag}) {
    // TODO: implement debug
  }

  @override
  void error(Object object, {String? tag}) {
    // TODO: implement error
  }

  @override
  void errorStack(Object error, StackTrace? stack, {String? tag}) {
    // TODO: implement errorStack
  }
}
