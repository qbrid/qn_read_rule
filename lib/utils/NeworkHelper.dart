import 'dart:async';
import 'dart:convert';
import 'dart:io' as io;
import 'dart:io';
import 'dart:typed_data';

import 'package:cookie_jar/cookie_jar.dart' as cookie_jar_package;
import 'package:dio/dio.dart';
import 'package:dio/io.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart' as cookie_manager;
import 'package:fast_gbk/fast_gbk.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:path_provider/path_provider.dart';
import 'package:qn_read_rule/utils/hash_m.dart';
import 'package:qn_read_rule/utils/logger.dart';

bool trustAllWebsite(io.X509Certificate cert, String host, int port) {
  return true;
}

typedef WrapWebViewRequest = Future<Response<List<int>>> Function(String url,
    {String? method,
    Map<String, String>? headers,
    String? requestData,
    String? responseCharset,
    int? waitTime});

typedef TrustAllWebsiteCallback = bool Function(
    X509Certificate cert, String host, int port)?;

class CacheResponseHelper {
  static final Map<int, Response<List<int>>> _cacheResponseMap = {};

  static int _clearTime = 0;

  // 每一分钟检测一次数据清理
  static const int _clearInterf = 60000;

  static Response<List<int>>? getCache(int key) {
    var currentTime = DateTime.now().millisecondsSinceEpoch;
    _clear(currentTime);
    var result = _cacheResponseMap[key];
    if (result == null) {
      return null;
    }
    if (currentTime > (result.extra['qb_expires'] ?? 0)) {
      _cacheResponseMap.remove(key);
      return null;
    }
    Logger.debug(() => '命中数据缓存 --> $key');
    return result;
  }

  static void setCache(int key, Response<List<int>> value, int cacheTime) {
    value.extra['qb_expires'] =
        DateTime.now().millisecondsSinceEpoch + cacheTime;
    _cacheResponseMap[key] = value;
  }

  static void _clear(int currentTime) {
    if (_clearTime + _clearInterf < currentTime) {
      _cacheResponseMap.removeWhere((key, value) {
        final expiresTime = value.extra['qb_expires'] ?? 0;
        return expiresTime < currentTime;
      });
      _clearTime = currentTime;
    }
  }
}

class NetworkHelper {
  // static

  static Map<String, int> redicetCountMap = {};

  static Map<int, Response<List<int>>> cacheResponseMap = {};

  static final Dio sourceDio = Dio(BaseOptions(
      sendTimeout: const Duration(milliseconds: 2500),
      receiveTimeout: const Duration(milliseconds: 8000),
      connectTimeout: const Duration(milliseconds: 3500)));

  // static final cookie_jar_package.CookieJar cookieJar = cookie_jar_package
  //     .PersistCookieJar();

  static void configProxy(String? proxy) {
    sourceDio.httpClientAdapter = IOHttpClientAdapter(createHttpClient: () {
      final client = HttpClient();
      client.badCertificateCallback = trustAllWebsite;
      if (proxy != null && proxy.isNotEmpty) {
        client.findProxy = (url) {
          return "PROXY $proxy;DIRECT;";
        };
      }
      return client;
    });
  }

  static WrapWebViewRequest? _wrapWebViewRequest;

  static void config(WrapWebViewRequest wrap) {
    _wrapWebViewRequest = wrap;
  }

  static void init(bool needCookies, bool needLog, {String? proxy}) {
    sourceDio.httpClientAdapter = IOHttpClientAdapter(createHttpClient: () {
      final client = HttpClient();
      client.badCertificateCallback = trustAllWebsite;
      if (proxy != null && proxy.isNotEmpty) {
        client.findProxy = (url) {
          return "PROXY $proxy;DIRECT;";
        };
      }
      return client;
    });
    getApplicationDocumentsDirectory().then((value) {
      sourceDio.interceptors.add(QBCookiesManager(
          cookie_jar_package.PersistCookieJar(
              storage: cookie_jar_package.FileStorage(value.path))));
    });
    if (needLog) {
      // sourceDio.interceptors.add(LogInterceptor(
      //     requestBody: true,
      //     responseBody: true,
      //     logPrint: (obj) {
      //       Logger.debug(() => obj.toString(), tag: 'NetworkHelper');
      //     }));
    }
  }

  static Future<Response<List<int>>> _handlerWebviewRequest(String url,
      {String? method,
      Map<String, String>? headers,
      String? requestData,
      String responseCharset = 'utf8',
      int? waitTime}) async {
    if (requestData == '') {
      requestData = null;
    }
    method = (method == null || method.isEmpty) ? 'GET' : method.toUpperCase();
    String? ua;
    if (headers != null && headers.containsKey('User-Agent')) {
      ua = headers['User-Agent'];
    }
    if (Platform.isIOS || Platform.isAndroid) {
      WebUri urlUri = WebUri(url);
      final Completer<void> okForLoadJs = Completer<void>();
      HeadlessInAppWebView web = HeadlessInAppWebView(
          initialUrlRequest: URLRequest(
              url: urlUri,
              method: method,
              headers: headers,
              timeoutInterval: 8,
              body: requestData == null
                  ? null
                  : Uint8List.fromList(utf8.encode(requestData))),
          initialSettings: InAppWebViewSettings(
            useShouldOverrideUrlLoading: true,
            useOnLoadResource: true,
            javaScriptEnabled: true,
            userAgent: ua,
            mediaPlaybackRequiresUserGesture: false,
            javaScriptCanOpenWindowsAutomatically: true,
            allowFileAccess: true,
            allowUniversalAccessFromFileURLs: true,
            preferredContentMode: UserPreferredContentMode.RECOMMENDED,
            allowsInlineMediaPlayback: true,
          ),
          onWebViewCreated: (controller) {},
          onReceivedServerTrustAuthRequest: (controller, challenge) async {
            // LogUtil.debug(() => "onReceivedServerTrustAuthRequest ->$challenge");
            return ServerTrustAuthResponse(
                action: ServerTrustAuthResponseAction.PROCEED);
          },
          onAjaxProgress: (InAppWebViewController controller,
              AjaxRequest ajaxRequest) async {
            // Logger.debug(() => "ajaxRequest ->$ajaxRequest");
            return AjaxRequestAction.PROCEED;
          },
          onLoadStart: (controller, url) {},
          onReceivedError: (InAppWebViewController controller, request, error) {
            Logger.debug(() => "onLoadError message ->$error");
            if (!okForLoadJs.isCompleted) {
              okForLoadJs.complete();
            }
          },
          onReceivedHttpError: (controller, request, error) {
            if (!okForLoadJs.isCompleted) {
              okForLoadJs.complete();
            }
          },
          onLoadStop: (controller, url) {
            if (!okForLoadJs.isCompleted) {
              okForLoadJs.complete();
            }
          });
      if (!web.isRunning()) {
        await web.run();
        await okForLoadJs.future;
      }
      if (waitTime != null && waitTime > 0) {
        await Future.delayed(Duration(milliseconds: waitTime));
      } else {
        await Future.delayed(const Duration(milliseconds: 500));
      }
      var text = await web.webViewController?.getHtml() ?? '';
      await web.dispose();
      return Response<List<int>>(
          requestOptions: RequestOptions(),
          data: utf8.encode(text),
          statusCode: 200);
    } else {
      return _handlerWebviewRequestWithPc(url,
          method: method,
          headers: headers,
          requestData: requestData,
          responseCharset: responseCharset,
          waitTime: waitTime);
    }
  }

  static Future<Response<List<int>>> _handlerWebviewRequestWithPc(String url,
      {String? method,
      Map<String, String>? headers,
      String? requestData,
      String responseCharset = 'utf8',
      int? waitTime}) {
    if (_wrapWebViewRequest == null) {
      return Future(() => Response(
          requestOptions: RequestOptions(),
          data: utf8.encode('未连续手机，无法调试'),
          statusCode: 404));
    } else {
      return _wrapWebViewRequest!(url,
          method: method,
          headers: headers,
          requestData: requestData,
          responseCharset: responseCharset,
          waitTime: waitTime);
    }
  }

  static Future<Response<List<int>>> excutorRequestWithResponse(String url,
      {String? method,
      Map<String, String>? headers,
      String? requestData,
      String? responseCharset,
      bool? isWebView,
      int? waitTime,
      bool? needCookies,
      int cacheTime = 0}) async {
    if (cacheTime <= 0) {
      redicetCountMap.clear();
      final result = await _excutorRequestWithResponse(url,
          method: method,
          headers: headers,
          requestData: requestData,
          responseCharset: responseCharset,
          isWebView: isWebView,
          waitTime: waitTime,
          needCookies: needCookies);
      return result;
    }
    var key = HashM.hashCodeGet(
        '$url-$method-$headers-$requestData-$responseCharset-$isWebView-$waitTime-$needCookies');
    var cacheResponse = CacheResponseHelper.getCache(key);
    if (cacheResponse != null) {
      return cacheResponse;
    }
    redicetCountMap.clear();
    final newResponse = await _excutorRequestWithResponse(url,
        method: method,
        headers: headers,
        requestData: requestData,
        responseCharset: responseCharset,
        isWebView: isWebView,
        waitTime: waitTime,
        needCookies: needCookies);
    if (newResponse.data?.isEmpty == true) {
    } else {
      CacheResponseHelper.setCache(key, newResponse, cacheTime);
    }
    return newResponse;
  }

  static Future<Response<List<int>>> _excutorRequestWithResponse(String url,
      {String? method,
      Map<String, String>? headers,
      String? requestData,
      String? responseCharset,
      bool? isWebView,
      int? waitTime,
      bool? needCookies,
      Map<String, dynamic>? extraMap}) async {
    if (isWebView == true) {
      return await _handlerWebviewRequest(url,
          method: method,
          headers: headers,
          requestData: requestData,
          responseCharset: responseCharset ?? '',
          waitTime: waitTime);
    }

    Map<String, String> requestHeaders = {};
    var options = Options();
    options.sendTimeout = const Duration(milliseconds: 3000);
    options.receiveTimeout = const Duration(milliseconds: 8000);
    options.responseType = ResponseType.bytes;
    // options.headers = headers;
    options.method = method ?? 'get';
    options.extra = {'needCookies': needCookies ?? false};
    options.extra!['charset'] = responseCharset;
    options.followRedirects = false;
    options.maxRedirects = 3;
    if (extraMap != null) {
      options.extra!.addAll(extraMap);
    }
    bool isGet = options.method == 'get';

    {
      if (headers != null) {
        requestHeaders.addAll(headers);
      }
      if (requestHeaders['Content-Type'] ==
          'application/x-www-form-urlencoded') {
        requestHeaders['Content-Length'] =
            utf8.encode(requestData ?? '').length.toString();
      }
      if (requestHeaders['Host'] == null ||
          requestHeaders['Host'].toString().trim().isEmpty) {
        requestHeaders['Host'] = Uri.parse(url).host;
      }
    }
    options.headers = requestHeaders;
    if (requestData == '') {
      requestData = null;
    }
    dynamic data = requestData;
    if (isGet && data != null) {
      if (data is Map) {
      } else if (data.toString().trim().isNotEmpty) {
        var string = data.toString();
        Map<String, String> map = {};
        var split = string.split('&');
        for (var s in split) {
          var split2 = s.split('=');
          map[split2[0]] = split2[1];
        }
        data = map;
      }
    }
    try {
      return await sourceDio.request<List<int>>(url,
          data: isGet ? null : data,
          queryParameters: isGet ? data : null,
          options: options);
    } catch (e) {
      if (e is DioException) {
        final response = e.response;
        if (response == null || response.data == null) {
          return Response(
              data: [],
              requestOptions: response?.requestOptions ?? RequestOptions());
        }
        final statusCode = e.response?.statusCode ?? -1;
        if (statusCode > 300 && statusCode < 310) {
          // 重定向
          var requestPath = response.requestOptions.path;

          List<String> location = response.headers.map['location'] ??
              response.headers.map['Location'] ??
              [];

          if (location.isEmpty) {
            return Response(
              data: (response.data as List)
                  .map((e) => e as int)
                  .toList(growable: false),
              requestOptions: response.requestOptions,
            );
          }

          final trueLocation = location[0].toString();
          var newRequestPath = '';
          if (trueLocation.startsWith("/")) {
            // 拼接域名
            final uri = Uri.parse(requestPath);
            newRequestPath = "${uri.scheme}://${uri.host}$trueLocation";
          } else if (trueLocation.startsWith('http')) {
            newRequestPath = trueLocation;
          } else {
            newRequestPath =
                "${requestPath.substring(0, requestPath.lastIndexOf("/") + 1)}$trueLocation";
          }

          int count = redicetCountMap[newRequestPath] ?? 0;
          redicetCountMap[newRequestPath] = count + 1;

          if (count > 3) {
            redicetCountMap.remove(newRequestPath);
            return Response(
                requestOptions: e.response?.requestOptions ?? RequestOptions(),
                data: [],
                statusCode: statusCode);
          } else {
            return await _excutorRequestWithResponse(newRequestPath,
                method: 'get',
                headers: headers,
                requestData: null,
                responseCharset: responseCharset,
                needCookies: response.requestOptions.extra['needCookies'],
                isWebView: isWebView,
                waitTime: waitTime,
                extraMap: {'redictResponse': response});
          }
        } else {
          // Logger.debug(() => 'e.msg ->$e -> ${e.requestOptions.path}');
          final dataList =
              ((response.data ?? []) as List).map((e) => e as int).toList();
          return Response(
              requestOptions: e.response?.requestOptions ?? RequestOptions(),
              data: dataList,
              statusCode: statusCode);
        }
      } else {
        return Response(
            requestOptions: RequestOptions(), data: [], statusCode: 500);
      }
    }
  }
}

class QBCookiesManager extends cookie_manager.CookieManager {
  QBCookiesManager(super.cookieJar);

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    if (options.extra['needCookies'] == null ||
        options.extra['needCookies'] == false) {
      handler.next(options);
    } else {
      cookieJar.loadForRequest(options.uri).then((cookies) {
        if (cookies.isEmpty) {
          handler.next(options);
        } else {
          super.onRequest(options, handler);
        }
      });
    }
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    if (response.requestOptions.extra['needCookies'] == null ||
        response.requestOptions.extra['needCookies'] == false) {
      handler.next(response);
    } else {
      super.onResponse(response, handler);
    }
  }

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    var response = err.response;
    if (response == null) {
      handler.next(err);
    } else {
      if (response.requestOptions.extra['needCookies'] == null ||
          response.requestOptions.extra['needCookies'] == false) {
        handler.next(err);
      } else {
        super.onError(err, handler);
      }
    }
  }
}

class LogInterceptor extends Interceptor {
  LogInterceptor({
    this.request = true,
    this.requestHeader = true,
    this.requestBody = false,
    this.responseHeader = true,
    this.responseBody = false,
    this.error = true,
    this.logPrint = _debugPrint,
  });

  /// Print request [Options]
  bool request;

  /// Print request header [Options.headers]
  bool requestHeader;

  /// Print request data [Options.data]
  bool requestBody;

  /// Print [Response.data]
  bool responseBody;

  /// Print [Response.headers]
  bool responseHeader;

  /// Print error message
  bool error;

  /// Log printer; defaults print log to console.
  /// In flutter, you'd better use debugPrint.
  /// you can also write log in a file, for example:
  ///```dart
  ///  final file=File("./log.txt");
  ///  final sink=file.openWrite();
  ///  dio.interceptors.add(LogInterceptor(logPrint: sink.writeln));
  ///  ...
  ///  await sink.close();
  ///```
  void Function(Object object) logPrint;

  @override
  void onRequest(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) async {
    logPrint('*** Request ***');
    _printKV('uri', options.uri);
    //options.headers;

    if (request) {
      _printKV('method', options.method);
      _printKV('responseType', options.responseType.toString());
      _printKV('followRedirects', options.followRedirects);
      _printKV('persistentConnection', options.persistentConnection);
      _printKV('connectTimeout', options.connectTimeout);
      _printKV('sendTimeout', options.sendTimeout);
      _printKV('receiveTimeout', options.receiveTimeout);
      _printKV(
        'receiveDataWhenStatusError',
        options.receiveDataWhenStatusError,
      );
      _printKV('extra', options.extra);
    }
    if (requestHeader) {
      logPrint('headers:');
      options.headers.forEach((key, v) => _printKV(' $key', v));
    }
    if (requestBody) {
      logPrint('data:');
      _printAll(options.data);
    }
    logPrint('');

    handler.next(options);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) async {
    logPrint('*** Response ***');
    _printResponse(response);
    handler.next(response);
  }

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) async {
    if (error) {
      logPrint('*** DioException ***:');
      logPrint('uri: ${err.requestOptions.uri}');
      logPrint('$err');
      if (err.response != null) {
        _printResponse(err.response!);
      }
      logPrint('');
    }

    handler.next(err);
  }

  void _printResponse(Response response) {
    _printKV('uri', response.requestOptions.uri);
    if (responseHeader) {
      _printKV('statusCode', response.statusCode);
      if (response.isRedirect == true) {
        _printKV('redirect', response.realUri);
      }

      logPrint('headers:');
      response.headers.forEach((key, v) => _printKV(' $key', v.join('\r\n\t')));
    }
    if (responseBody) {
      logPrint('Response Text:');
      var charset = response.requestOptions.extra['charset'];
      if (charset == '' || charset == 'utf8' || charset == 'utf-8') {
        _printAll(utf8.decode(response.data, allowMalformed: true).length);
      } else if (charset == 'gbk') {
        _printAll(gbk.decode(response.data, allowMalformed: true).length);
      } else {
        _printAll(response.toString().length);
      }
    }
    logPrint('');
  }

  void _printKV(String key, Object? v) {
    logPrint('$key: $v');
  }

  void _printAll(msg) {
    msg.toString().split('\n').forEach(logPrint);
  }
}

void _debugPrint(Object? object) {
  assert(() {
    Logger.debug(() => object.toString());
    return true;
  }());
}
