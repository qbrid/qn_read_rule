let GLOBAL_RULE_MAP={};

function sync_proxy(method, args) {
     return sendMessage(method, JSON.stringify(args));
}


// 异步代理
async function async_proxy(method, args) {
     return await sendMessage(method, JSON.stringify(args));
}



function toDateString(date, format = "yyyy-MM-dd HH:mm:ss#SSS") {
    function digit(value, length = 2) {
        if (
            typeof value === "undefined" ||
            value === null ||
            String(value).length >= length
        ) {
            return value;
        }
        return (Array(length).join("0") + value).slice(-length);
    }

    const ymd = [
        digit(date.getFullYear(), 4),
        digit(date.getMonth() + 1),
        digit(date.getDate()),
    ];
    const hms = [
        digit(date.getHours()),
        digit(date.getMinutes()),
        digit(date.getSeconds()),
        digit(date.getMilliseconds(), 3),
    ];
    return format
        .replace(/yyyy/g, ymd[0])
        .replace(/MM/g, ymd[1])
        .replace(/dd/g, ymd[2])
        .replace(/HH/g, hms[0])
        .replace(/mm/g, hms[1])
        .replace(/ss/g, hms[2])
        .replace(/SSS/g, hms[3]);
}
