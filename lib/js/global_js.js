

class QBAsync {

    // 网络请求
    // argsMap 为 模型对象
    //  { url: '' , method : '', 'requestData':'',}
    // 包含如下字段：
    // url 字符串，请求地址
    // method，简写 d  方法支持get/post/delete/header/put等等
    // requestData，简写 d 请求字符串，请自行拼接 form表单 或者 json字符串
    // headers, 简写 h，对象，{'xx': 'yy','zz':'cc'}格式，请求头,
    // responseCharset, 简写 rCharset，默认自匹配编码，也可手动指定，支持 utf-8 / gbk / bytes
    // ext 扩展对象，支持以下字段：
    //      webview 只有为 1时，才为webview请求，默认为空
    //      webviewTime,指定webview请求下的等待时 webview为开启（为1）时生效，单位为毫秒，默认500毫秒
    //      needCookies,是否需要Cookies注入，默认为1，如无需cookies，手动设置为0
    //      needRspHeader,是否需要响应头，默认为0，如需RspHeaders，手动设置为1
    //      needDataTag,是否需要虚拟的的响应Tag，在JS中调用默认为1，其余默认为0,设置为1时，将不返回真实数据
    // 返回数据： Response对象，包含 header/ statusCode/ data / dataTag
    //
    //
    static async network(argsMap) {
        if (isEmpty(argsMap.ext)) {
            argsMap.ext = {
                needDataTag: 1
            }
        }
        if (isEmpty(argsMap.ext.needDataTag)) {
            argsMap.ext.needDataTag = 1
        }
        return await async_proxy('network', argsMap);
    }


    // 网络请求
    // allInOneStr 字符串，格式为  http://www.baidu.com,{"method":"get","headers":{"Host":"www.baidu.com"}}
    // 具体字体可参考 网络请求 network
    static async networkStr(allInOneStr) {
        let result = await async_proxy('networkStr', {url: allInOneStr});
        return result.data
    }

}


class QB {

    static _enableLog = true
    static enableLogCostTime = true

    // 日志配置
    //  {
    //      "enable":true,
    //       "logCostTime": true
    //    }
    static configLog(logConfig) {
        this._enableLog = logConfig.enable || true
        this.enableLogCostTime = logConfig.logCostTime
    }


    // 输出日志,可在开发工具上查看。
    // log为字符串
    static log(log, level) {
        if (!this._enableLog) {
            return;
        }
        if (isEmpty(level)) {
            level = 'DEBUG'
        }

        return sync_proxy('log', {
            log: log,
            level: level,
        });
    }

    // 时间戳，返回 数字时间戳（毫秒）
    static timestamp() {
        return new Date().getTime();
    }


    // 设置KV
    static putKey(bookId, k, v) {
        return sync_proxy('putKey', {
            bookId: bookId,
            k: k,
            v: v,
        })
    }


    // 通过Key获取Value
    static getKey(bookId, k) {
        return sync_proxy('getKey', {
            bookId: bookId,
            k: k,
        })
    }

    // 获取发现页面，从1开始
    static getExplorePage() {
        return sync_proxy('getExplorePage', {})
    }

    // 获取搜索的关键词
    static getSearchKey() {
        return sync_proxy('getSearchKey', {})
    }

    // 获取当前的PreV
    static getPreV() {
        return sync_proxy('getPreV', {})
    }

    // 获取 本次的NetworkData
    static networkData() {
        return sync_proxy('networkData', {})
    }

    // 通过BookId获取书籍模型对象 BookModel
    static getBook(bookId) {
        return sync_proxy('getBook', {bookId: bookId})
    }


    // 通过chapterUrl获取章节模型对象 ChapterModel
    static getChapter(chapterUrl) {
        return sync_proxy('getChapter', {chapterUrl: chapterUrl})
    }


    // Xpath解析内容
    // rule为规则
    // node为 数据节点
    // join，多行连接符。当通过rule解析到多个匹配对象时，最终输出的结果，通过join进行最后拼装
    // nodeTag 数据节点标识，用来优化查询性能
    static xpathQuerySingle(rule, node, join, nodeTag) {
        return sync_proxy('xpathQuerySingle', {rule: rule, node: node, join: join, nodeTag: nodeTag})
    }

    // Xpath解析列表
    // rule为规则
    // node为 数据节点
    // nodeTag 数据节点标识，用来优化查询性能
    static xpathQueryList(rule, node, nodeTag) {
        return sync_proxy('xpathQueryList', {rule: rule, node: node, nodeTag: nodeTag})
    }

    // Xpath解析列表 tagCount ,高性能查询
    // rule为规则
    // node为 数据节点
    // nodeTag 数据节点标识，用来优化查询性能
    // 返回 解析列表总数，后续 tag 可使用 for循环拼装
    static xpathQueryListTagCount(rule, node, nodeTag) {
        return sync_proxy('xpathQueryListTagCount', {rule: rule, node: node, nodeTag: nodeTag})
    }

    // 通过 nodeTag 获取 真实的数据源
    static queryDataByNodeTag(tag) {
        return sync_proxy('qDtag', {tag: tag,})
    }

    // md5签名
    // 32位小写字符串
    static md5String(str) {
        return sync_proxy('md5String', {str: str});
    }

     // md5签名
        // 32位小写字符串
     static md5(str) {
         return sync_proxy('md5', {str: str});
     }


    // 3DES解密
    // 传入 secretStr， key，iv
    // 其中 key可以为 utf8编码的字符串 或者 为 uint8List
    // 其中 iv可以为 utf8编码的字符串 或者 为 uint8List
    // return utf8字符串
    static DESedeDecode(secretStr, key, iv) {
        return sync_proxy('DESedeDecode', {
            secretStr: secretStr,
            key: key,
            iv: iv,
        });
    }


    // 3DES加密
    // 传入 base， key，iv
    // 其中 key可以为 utf8编码的字符串 或者 为 bytes数组
    // 其中 iv可以为 utf8编码的字符串 或者 为 bytes数组
    // return utf8字符串
    static DESedeEncode(source, key, iv) {
        return sync_proxy('DESedeEncode', {
            source: source,
            key: key,
            iv: iv,
        });
    }

    /// aes加密，String base,String key,String iv,String padding,String mode
    /// base 字符串
    /// key 加密Key
    /// iv 加密IV
    /// padding 加密Padding类型，例如 PKCS5 / PKCS7
    /// mode 加密模式,例如 ecb,sic
    /// return utf8字符串
    static aesEncode(source, key, iv, padding, mode) {
        return sync_proxy('aesEncode', {
            source: source,
            key: key,
            iv: iv,
            padding: padding,
            mode: mode,
        });
    }

    /// aes解密，String secretStr,String key,String iv,String padding,String mode
    /// secretStr 待解密字符串
    /// key 加密Key
    /// iv 加密IV
    /// padding 加密Padding类型，例如 PKCS5 / PKCS7
    /// mode 加密模式,例如 ecb,sic
    /// return utf8字符串
    static aesDecode(secretStr, key, iv, padding, mode) {
        return sync_proxy('aesDecode', {
            source: secretStr,
            key: key,
            iv: iv,
            padding: padding,
            mode: mode,
        });
    }

    /// ras加密，
    /// base 待加密字符串 或者 bytes数组
    /// key 秘钥，支持 Utf8字符串 或者 bytes数组
    /// type 编码类型： PKCS1 或 OAEP
    /// return 加密的bytes数组
    static rasEncode(source, key, type) {
        return sync_proxy('aesEncode', {
            source: source,
            key: key,
            type: type,
        });
    }


    // 获取 hashCode
    static hashCode(str) {
        return sync_proxy('hashCode', {str: str})
    }


    // str转bytes
    // str为字符串
    // charset为编码方式，支持 utf-8/gbk
    // 输出 uint8Array，输出字节数组
    static strToBytes(str, charset) {
        return sync_proxy('strToBytes', {str: str, charset: charset})
    }

    // bytes转字符串
    // bytes为字节数组
    // charset为编码方式，支持 utf-8/gbk
    // 输出 支付串
    static bytesToStr(bytes, charset) {
        return sync_proxy('bytesToStr', {bytes: bytes, charset: charset})
    }

    /**
     * Base64字符串 转 字符串
     * 输入为 base64字符串 和 字符编码(支持 utf-8,gbk)
     * 输出 解码字符串
     */
    static base64Decode(base64, charset) {
        return sync_proxy('base64Decode', {base64: base64, charset: charset})
    }


    // base64转 bytes数组
    static base64DecodeToByteArray(base64) {
        if (isEmpty(base64)) {
            return []
        }
        return sync_proxy('base64DecodeToByteArray', {base64: base64})
    }

    // base64 加密,输出 base64字符串
    static base64Encode(str) {
        return sync_proxy('base64Encode', {str: str})
    }

    // base64 加密，输入为 bytes数组,输出 base64字符串
    static base64EncodeBytes(bytes) {
        return sync_proxy('base64EncodeBytes', {bytes: bytes})
    }

    // hex 转 bytes数组
    static hexDecodeToByteArray(hex) {
        return sync_proxy('hexDecodeToByteArray', {hex: hex})
    }

    // hex 转 字符串, charset为编码格式
    static hexDecodeToString(hex, charset) {
        return sync_proxy('hexDecodeToString', {hex: hex, charset: charset})
    }


    // utf8 编码为hexString
    static strEncodeToHex(str) {
        return sync_proxy('strEncodeToHex', {str: str})
    }


    /**
     * utf8编码转gbk编码，返回字符串
     */
    static utf8ToGbk(str) {
        return sync_proxy('utf8ToGbk', {str: str})
    }


    // url编码
    // str为待编码字符串
    // enc 为 TODO 待调整，暂未实现enc
    static encodeURI(str, enc) {
        return sync_proxy('encodeURI', {str: str, enc: enc})
    }

    // url解密
    // str为待解码字符串
    // enc 为 TODO 待调整，暂未实现enc
    static decodeURI(str, enc) {
        return sync_proxy('decodeURI', {str: str, enc: enc})
    }


    // TODO 待调整
    static t2s(text) {
        return sync_proxy('t2s', {text: text})
    }

    // TODO 待调整
    static s2t(text) {
        return sync_proxy('s2t', {text: text})
    }


    /**
     * toast提示，调用 App toast
     */
    static toast(text) {
        return sync_proxy('toast', {text: text})
    }

    /**
     * toast提示，调用 App toast
     * 时间长
     */
    static longToast(text) {
        return sync_proxy('longToast', {text: text})
    }

//**************** TODO 待实现 ******************//

    /**
     * 获取本地文件
     * @param path 相对路径
     * @return 可读取的绝对路径
     */
    static getFile(path) {

    }


    // 读取文件的 Bytes数组
    static readFile(path) {
    }


    // 读取文件
    // 输入 path路径
    // charset为编码格式
    // 返回文字内容
    static readTxtFile(path, charset) {

        return ""
    }

    /**
     * 删除本地文件，相对路径
     */
    static deleteFile(path) {

    }

    /**
     * js实现Zip压缩文件解压
     * @param zipPath 相对路径
     * @return string 解压的文件路径
     */
    static unzipFile(zipPath) {
        // return unArchiveFile(zipPath)
        return '';
    }

    /**
     * js实现7Zip压缩文件解压
     * @param zipPath 相对路径
     * @return string 解压路径
     */
    static un7zFile(zipPath) {
        return '';
    }

    /**
     * js实现Rar压缩文件解压
     * @param rarPath 相对路径
     * @return string 相对路径
     */
    static unrarFile(rarPath) {
        return ''
    }

    /**
     * js实现压缩文件解压
     * @param zipPath 相对路径
     * @return string 相对路径
     */
    static unArchiveFile(zipPath) {
        return ''
    }


    static addInnerToc(innerToc) {
        return sync_proxy('addInnerToc', {innerToc: innerToc})
    }

    static addContent(qnContentModel) {
        return sync_proxy('addContent', {content: qnContentModel})
    }

    static addAllBook(list) {

        return sync_proxy('addAllBook', {list: list})
    }

    static addBook(qnBookModel) {
        return sync_proxy('addBook', {book: qnBookModel})
    }

    static addBookToc(tocModel) {
        return sync_proxy('addBookToc', {tocModel: tocModel})
    }

    static qn_querySingle(withRule, node, model, join, chapterModel) {
        return sync_proxy('qn_querySingle', {
            withRule: withRule,
            node: node,
            model: model,
            join: join,
            chapterModel: chapterModel
        })
    }

    static qn_queryList(withRule, node, model) {
        return sync_proxy('qn_queryList', {
            withRule: withRule,
            node: node,
            model: model,
            returnString:true
        })
    }

    static setCurrentNetworkData(data) {
        return sync_proxy('setCurrentNetworkData', {
            data: data,
        })
    }

    static setCurrentPage(page) {
        return sync_proxy('setCurrentPage', {
            page: page,
        })
    }

    static setCurrentSearchKey(key) {
        return sync_proxy('setCurrentSearchKey', {
            key: key,
        })
    }
}


function isEmpty(obj) {
    return obj === undefined || obj === '' || obj == null;
}


function getPreV(){
    return QB.getPreV()
}

