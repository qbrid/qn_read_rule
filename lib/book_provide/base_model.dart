// import 'package:dart_eval/dart_eval.dart';
// import 'package:dart_eval/dart_eval_bridge.dart';
// import 'package:dart_eval/stdlib/core.dart';
import 'package:qn_read_rule/utils/hash_m.dart';

// part 'base_model.g.dart';

const String emptyDetailUrl = '_qnE';
const String emptyContentKey = '_qcE';

// final int emptyBookId = HashMProxy.hashCodeGet(emptyDetailUrl);
const int emptyBookId = 2942217;

class QNBaseModel {
  // 站点ID
  final String siteId;

  // 站点类型
  final String type;

  // 显示名称
  final String showName;

  // 站点分组
  final String group;

  // 站点简介
  final String desc;

  // 漫画显示类型： 小、中等、大，仅漫画站点生效
  final String comicShowType;

  // 站点开发类型 ： 分为 json 和 dart
  final bool enable;

  // 更新时间
  final int updateTime;

  // 版本
  final int versionNumb;

  // 支持搜索书名
  final bool supportSearchBookName;

  // 支持发现
  final bool supportExplore;

  final String vpnWebsite;

  final bool isEncrypt;

  String get typeString {
    if (type == 'book') {
      return '书籍';
    } else if (type.toLowerCase() == 'listenbook') {
      return '听书';
    } else if (type == 'comic') {
      return '漫画';
    }
    return '书籍';
  }

  const QNBaseModel.init(
      {required this.siteId,
      required this.type,
      required this.showName,
      required this.group,
      required this.desc,
      required this.comicShowType,
      required this.enable,
      required this.updateTime,
      required this.versionNumb,
      required this.supportExplore,
      required this.vpnWebsite,
      required this.supportSearchBookName,
      required this.isEncrypt});

  @override
  String toString() {
    return 'QNBaseModel{siteId: $siteId, type: $type, showName: $showName, group: $group, desc: $desc, comicShowType: $comicShowType, enable: $enable, updateTime: $updateTime, versionNumb: $versionNumb, supportSearchBookName: $supportSearchBookName, supportExplore: $supportExplore, vpnWebsite: $vpnWebsite}';
  }

  Map toMap() {
    return {
      'siteId': siteId,
      'type': siteId,
      'showName': showName,
      'group': group,
      'desc': desc,
      'comicShowType': comicShowType,
      'enable': enable,
      'updateTime': updateTime,
      'versionNumb': versionNumb,
      'supportExplore': supportExplore,
      'vpnWebsite': vpnWebsite,
      'supportSearchBookName': supportSearchBookName,
      'isEncrypt': isEncrypt,
    };
  }
}

class QNExploreModel {
  final List<QNExploreItem> boyExploreList;
  final List<QNExploreItem> girlExploreList;

  const QNExploreModel.init(
      {required this.boyExploreList, required this.girlExploreList});

  static const QNExploreModel empty =
      QNExploreModel.init(boyExploreList: [], girlExploreList: []);

  static const QNExploreModel error = QNExploreModel.init(
      boyExploreList: [QNExploreItem(tag: 'error', url: 'error')],
      girlExploreList: [QNExploreItem(tag: 'error', url: 'error')]);

  @override
  String toString() {
    return 'QNExploreModel{boyExploreList[0-2]: ${boyExploreList.sublist(0, 2)}, girlExploreList: ${girlExploreList.length > 1 ? girlExploreList[0] : []}';
  }

  Map toMap() {
    List<Map> boyList = [];
    {
      for (var m in boyExploreList) {
        boyList.add(m.toMap());
      }
    }
    List<Map> girlList = [];
    {
      for (var m in girlExploreList) {
        girlList.add(m.toMap());
      }
    }
    return {
      'boyExploreList': boyList,
      'girlExploreList': girlList,
    };
  }
}

class QNExploreItem {
  final String tag;

  final String url;

  const QNExploreItem({required this.tag, required this.url});

  @override
  String toString() {
    return 'QNExploreItem{tag: $tag, url: $url}';
  }

  Map toMap() => {"tag": tag, 'url': url};
}

class QNBookModel {
  static final QNBookModel empty = QNBookModel.init(
      siteId: '',
      channelId: '',
      siteVersion: 0,
      bookName: '',
      encode: '',
      detailUrl: emptyDetailUrl);

  /// bookId
  final int bookId;

  /// 站点ID
  final String siteId;

  /// 站点版本
  final int siteVersion;

  /// 书名
  final String bookName;

  /// 编码：默认为utf8，如为 gbk需声明
  final String encode;

  /// 详情页URL
  final String detailUrl;

  ///  标签
  final List<String> tags;

  /// 评分,大小为 0~ 10，数字类型，默认为8.5
  final double star;

  /// 字数
  final int wordCount;

  /// 最新章节名称
  final String newestChapter;

  /// 目录页URL
  final String tocUrl;

  /// 作者名称
  final String author;

  /// 书籍封面
  final String cover;

  /// 简介
  final String desc;

  final String channelId;

  final int isFinish;

  QNBookModel.init({
    required this.siteId,
    required this.bookName,
    required this.siteVersion,
    required this.encode,
    required this.detailUrl,
    required this.channelId,
    this.tags = const [],
    this.star = 0,
    this.wordCount = 0,
    this.newestChapter = '',
    this.tocUrl = '',
    this.cover = '',
    this.desc = '',
    this.author = '',
    this.isFinish = 0,
  }) : bookId = HashMProxy.hashCodeGet(detailUrl);

  static QNBookModel copyWith(
    QNBookModel old, {
    String newBookname = '',
    int newSiteVersion = 0,
    String newEncode = '',
    List<String> newTags = const [],
    double newStar = 0,
    int newWordcount = 0,
    String newNewestchapter = '',
    String newTocurl = '',
    String newAuthor = '',
    String newCover = '',
    String newDesc = '',
    int newIsFinish = -1,
  }) {
    return QNBookModel.init(
      siteId: old.siteId,
      channelId: old.channelId,
      detailUrl: old.detailUrl,
      siteVersion: newSiteVersion > 0 ? newSiteVersion : old.siteVersion,
      bookName: newBookname.isNotEmpty ? newBookname : old.bookName,
      encode: newEncode.isNotEmpty ? newEncode : old.encode,
      tags: newTags.isNotEmpty ? newTags : old.tags,
      star: newStar > 0 ? newStar : old.star,
      wordCount: newWordcount > 0 ? newWordcount : old.wordCount,
      newestChapter:
          newNewestchapter.isNotEmpty ? newNewestchapter : old.newestChapter,
      tocUrl: newTocurl.isNotEmpty ? newTocurl : old.tocUrl,
      author: newAuthor.isNotEmpty ? newAuthor : old.author,
      cover: newCover.isNotEmpty ? newCover : old.cover,
      desc: newDesc.isNotEmpty ? newDesc : old.desc,
      isFinish: newIsFinish >= 0 ? newIsFinish : old.isFinish,
    );
  }

  @override
  String toString() {
    return 'QNBookModel{bookId: $bookId, siteId: $siteId, siteVersion: $siteVersion, bookName: $bookName, encode: $encode, detailUrl: $detailUrl, tags: $tags, star: $star, wordCount: $wordCount, newestChapter: $newestChapter, tocUrl: $tocUrl, author: $author, cover: $cover, desc: $desc}';
  }

  Map<String, dynamic> toMap() => {
        'siteId': siteId,
        'bookName': bookName,
        'siteVersion': siteVersion,
        'encode': encode,
        'detailUrl': detailUrl,
        'tags': tags,
        'star': star,
        'tocUrl': tocUrl,
        'wordCount': wordCount,
        'cover': cover,
        'newestChapter': newestChapter,
        'desc': desc,
        'author': author,
        'bookId': bookId,
        'channelId': channelId,
        'isFinish': isFinish,
      };

  String queryBy(String rule) {
    switch (rule) {
      case 'siteId':
        return siteId;
      case 'bookName':
        return bookName;
      case 'author':
        return author;
      case 'bookId':
        return bookId.toString();
      case 'siteVersion':
        return siteVersion.toString();
      case 'detailUrl':
        return detailUrl;
      case 'tocUrl':
        return tocUrl;
      case 'tags':
        return tags.join(' ');
      case 'star':
        return star.toString();
      case 'wordCount':
        return wordCount.toString();
      case 'cover':
        return cover;

      case 'newestChapter':
        return newestChapter;
      case 'desc':
        return desc;

      case 'channelId':
        return channelId;
      //
      case 'isFinish':
        return isFinish.toString();

      default:
        throw Exception('不存在的属性->book.$rule');
    }
  }
}

class QNTocInnerModel {
  /// 章节标题
  final String title;

  /// 章节URL
  final String url;

  ///  是否是VIP
  final bool needVip;

  /// 字数
  final int wordCount;

  QNTocInnerModel.init(
      {required this.title,
      required this.url,
      required this.needVip,
      this.wordCount = 0});

  @override
  String toString() {
    return 'QNTocInnerModel{title: $title, url: $url, needVip: $needVip, wordCount: $wordCount}';
  }

  String queryBy(String rule) {
    switch (rule) {
      case 'title':
        return title;
      case 'url':
        return url;
      case 'needVip':
        return needVip ? '0' : '1';
      case 'wordCount':
        return wordCount.toString();
      default:
        throw Exception('不存在的属性 chapter.$rule');
    }
  }

  Map<String, dynamic> toMap() => {
        'title': title,
        'url': url,
        'needVip': needVip,
        'wordCount': wordCount,
      };
}

class QNTocModel {
  static const QNTocModel empty = QNTocModel.init(
      bookId: emptyBookId, updateTime: -1, dataList: <QNTocInnerModel>[]);

  /// 书籍ID
  final int bookId;

  /// 更新时间
  final int updateTime;

  /// 章节数据
  final List<QNTocInnerModel> dataList;

  const QNTocModel.init(
      {required this.bookId, required this.updateTime, required this.dataList});

  @override
  String toString() {
    return 'QNTocModel{bookId: $bookId, updateTime: $updateTime, dataList[0~3]: ${dataList.sublist(0, 3)}}';
  }

  Map toMap() {
    return {
      'bookId': bookId,
      'updateTime': updateTime,
      'dataList': dataList.map((e) => e.toMap()).toList(growable: false)
    };
  }
}

class QNContentModel {
  static final QNContentModel empty = QNContentModel.init(
    contentKey: emptyContentKey,
  );

  final int id;

  final String contentKey;

  /// 书籍数据 （ 书籍有效 ）
  final String bookContent;

  final List<ComicPicModel> comicPicList;

  final MusicContentModel musicContent;

  QNContentModel.init(
      {required this.contentKey,
      this.bookContent = '',
      this.comicPicList = const [],
      this.musicContent = const MusicContentModel.init(url: '')})
      : id = HashMProxy.hashCodeGet(contentKey);

  @override
  String toString() {
    return 'QNContentModel{id: $id, contentKey: $contentKey, bookContent: $bookContent, comicPicList: $comicPicList, musicContent: $musicContent}';
  }

  Map toMap() => {
        'id': id,
        'contentKey': contentKey,
        'bookContent': bookContent,
        'comicPicList':
            comicPicList.map((e) => e.toMap()).toList(growable: false),
        'musicContent': musicContent.toMap()
      };
}

class MusicContentModel {
  /// url链接
  final String url;

  ///  部分请求需要 设置请求头
  final Map<String, String> headers;

  /// 时长
  final int duration;

  const MusicContentModel.init(
      {required this.url, this.headers = const {}, this.duration = 0});

  @override
  String toString() {
    return 'MusicContentModel{url: $url, headers: $headers, duration: $duration}';
  }

  Map<String, dynamic> toMap() => {
        'url': url,
        'headers': headers,
        'duration': duration,
      };
}

class ComicPicModel {
  /// url链接
  final String url;

  ///  部分请求需要 设置请求头
  final Map<String, String> headers;

  final int w;

  final int h;

  const ComicPicModel.init(
      {required this.url, this.headers = const {}, this.w = 0, this.h = 0});

  @override
  String toString() {
    return 'ComicPicModel{url: $url, headers: $headers, w: $w, h: $h}';
  }

  Map<String, dynamic> toMap() =>
      {'url': url, 'headers': headers, 'w': w, 'h': h};
}

class HashMProxy {
  static int hashCodeGet(String key) {
    return HashM.hashCodeGet(key);
  }
}
