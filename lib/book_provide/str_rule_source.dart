import 'dart:convert';

import 'package:fast_gbk/fast_gbk.dart';
import 'package:qn_read_rule/book_provide/base.dart';
import 'package:qn_read_rule/book_provide/base_cache.dart';
import 'package:qn_read_rule/book_provide/base_model.dart';
import 'package:qn_read_rule/utils/b_utils.dart';
import 'package:qn_read_rule/utils/hash_m.dart';

class StringBaseProvide extends BookSourceProvide {
  final Map<String, dynamic> source;

  StringBaseProvide({required this.source}) {
    final gMap = source['g'];
    if (gMap is Map) {
      for (var k in gMap.keys) {
        if (k != 'debugSearchKey' &&
            k != 'headers' &&
            k != 'exploreGenerator') {
          _userDefineMap[k.toString()] = gMap[k.toString()] ?? '';
        }
      }
    }
  }

  final Map<String, String> _userDefineMap = {};

  Map<String, String> get userDefineMap => _userDefineMap;

  QNBaseModel? _info;
  QNExploreModel? _exploreModel;

  QNExploreModel _queryExploreModel() {
    info();
    bool support = _info?.supportExplore ?? false;
    if (!support) {
      return QNExploreModel.empty;
    }
    var exploreSource = source['exploreUrl'];
    if (exploreSource == null) {
      _exploreModel = QNExploreModel.empty;
    }
    if (exploreSource is List) {
      final List<QNExploreItem> boyExploreList = [];
      final List<QNExploreItem> girlExploreList = [];
      for (var m in exploreSource) {
        var tag = m['tag'].toString();
        var url = m['url'].toString();
        if (tag.contains('女')) {
          girlExploreList.add(QNExploreItem(tag: tag, url: url.trim()));
        } else {
          boyExploreList.add(QNExploreItem(tag: tag, url: url.trim()));
        }
      }
      return QNExploreModel.init(
          boyExploreList: boyExploreList, girlExploreList: girlExploreList);
    } else {
      final exploreUrl = exploreSource.toString().trim();

      var newExplore = exploreUrl.toString();
      if (newExplore.startsWith('@js:')) {
        newExplore = BNativeUtils.qn_querySingle(
            withRule: exploreUrl,
            node: '',
            model: QNBookModel.empty,
            addParams: _userDefineMap);
      }

      if (newExplore.isEmpty) {
        _exploreModel = QNExploreModel.empty;
      }

      if (newExplore.startsWith('[') || newExplore.startsWith('{')) {
        // json 格式
        var json = jsonDecode(newExplore);
        final List<dynamic> datas;
        if (json is List) {
          datas = json;
        } else {
          datas = [json];
        }
        final List<QNExploreItem> boyExploreList = [];
        final List<QNExploreItem> girlExploreList = [];
        for (var m in datas) {
          var tag = m['tag'].toString();
          var url = m['url'].toString();
          if (tag.contains('女')) {
            girlExploreList.add(QNExploreItem(tag: tag, url: url.trim()));
          } else {
            boyExploreList.add(QNExploreItem(tag: tag, url: url.trim()));
          }
        }
        return QNExploreModel.init(
            boyExploreList: boyExploreList, girlExploreList: girlExploreList);
      } else {
        final List<QNExploreItem> boyExploreList = [];
        final List<QNExploreItem> girlExploreList = [];
        var split = newExplore.split('\n');
        for (final s in split) {
          if (s.isEmpty) {
            continue;
          }
          var split2 = s.split('::');
          final tag = split2[0];
          final url = split2[1];
          if (tag.contains('女')) {
            girlExploreList.add(QNExploreItem(tag: tag, url: url.trim()));
          } else {
            boyExploreList.add(QNExploreItem(tag: tag, url: url.trim()));
          }
        }
        return QNExploreModel.init(
            boyExploreList: boyExploreList, girlExploreList: girlExploreList);
      }
    }
  }

  @override
  String siteId() {
    return source['siteId'] ?? '';
  }

  @override
  int currentSiteVersion() {
    // TODO: implement currentSiteVersion
    return int.parse((source['info']['versionNumb'] ?? 0).toString());
  }

  bool _transformBool(dynamic value) {
    if (value == true) {
      return true;
    } else if (value == false) {
      return false;
    } else if (value == '1') {
      return true;
    } else if (value == 1) {
      return true;
    } else {
      return false;
    }
  }

  bool _transformEnable(dynamic value) {
    if (value == true) {
      return true;
    } else if (value == false) {
      return false;
    } else if (value == '1') {
      return true;
    } else if (value == 1) {
      return true;
    } else if (value == null) {
      return true;
    } else if (value.toString().trim() == '') {
      return true;
    } else {
      return false;
    }
  }

  @override
  QNBaseModel info() {
    if (_info != null) {
      return _info!;
    }

    Map? infoMap = source['info'];
    if (infoMap == null) {
      throw Exception('不存在 info 节点');
    }
    bool supportSearch = _transformBool(infoMap['supportSearch']);
    bool supportExplore = _transformBool(infoMap['supportExplore']);
    bool isEncrypt = _transformBool(infoMap['isEncrypt']);
    bool isEnable = _transformEnable(infoMap['enable']);
    var updateTime;
    try {
      updateTime = int.parse((infoMap['updateTime'] ?? 0).toString());
    } catch (error) {
      // print('updateTime --> ${infoMap['updateTime']}');
      updateTime = 0;
    }

    _info = QNBaseModel.init(
        siteId: siteId(),
        type: (infoMap['type'] ?? 'book').toString().toLowerCase(),
        showName: infoMap['showName'],
        group: infoMap['group'],
        desc: infoMap['desc'] ?? '',
        comicShowType: '',
        enable: isEnable,
        updateTime: updateTime,
        versionNumb: currentSiteVersion(),
        supportExplore: supportExplore,
        vpnWebsite: infoMap['vpnWebsite'] ?? '',
        isEncrypt: isEncrypt,
        supportSearchBookName: supportSearch);

    return _info!;
  }

  String getSourceType(int? type) {
    switch (type) {
      case 0:
        return 'book';
      case 1:
        return 'listenbook';
      case 2:
        return 'comic';
      default:
        return 'book';
    }
  }

  @override
  QNExploreModel exploreModel() {
    if (_exploreModel != null) {
      return _exploreModel!;
    }
    _exploreModel = _queryExploreModel();
    return _exploreModel!;
  }

  @override
  int migrate(int oldVersion) {
    // TODO: implement migrate
    return 0;
  }

  @override
  Future<QNBookModel> queryBookDetail(QNBookModel model) async {
    var map = source['ruleDetail'];
    if (map == null) {
      return model;
    }
    var url = model.detailUrl;
    var data = await BNativeUtils.qn_str_Network(url, source['g']?['headers']);
    GlobalPrev.currentNetworkData = data;
    final cover = BNativeUtils.qn_querySingle(
        withRule: map['cover'],
        node: data,
        model: model,
        addParams: _userDefineMap);
    final author = BNativeUtils.qn_querySingle(
        withRule: map['author'],
        node: data,
        model: model,
        addParams: _userDefineMap);
    final desc = BNativeUtils.qn_querySingle(
        withRule: map['desc'],
        node: data,
        model: model,
        addParams: _userDefineMap);
    final tags = BNativeUtils.qn_querySingle(
            withRule: map['tags'],
            node: data,
            model: model,
            addParams: _userDefineMap)
        .split('\n');
    final lastChapter = BNativeUtils.qn_querySingle(
        withRule: map['newestChapter'],
        node: data,
        model: model,
        addParams: _userDefineMap);
    final bookName = BNativeUtils.qn_querySingle(
        withRule: map['bookName'],
        node: data,
        model: model,
        addParams: _userDefineMap);
    final wordCount = BNativeUtils.qn_querySingle(
        withRule: map['wordCount'],
        node: data,
        model: model,
        addParams: _userDefineMap);

    final star = BNativeUtils.qn_querySingle(
        withRule: map['star'],
        node: data,
        model: model,
        addParams: _userDefineMap);

    final isFinish = BNativeUtils.qn_querySingle(
        withRule: map['isFinish'],
        node: data,
        model: model,
        addParams: _userDefineMap);

    final tocUrl = BNativeUtils.qn_querySingle(
        withRule: map['tocUrl'],
        node: data,
        model: model,
        addParams: _userDefineMap);

    return QNBookModel.copyWith(model,
        newBookname: bookName,
        newSiteVersion: currentSiteVersion(),
        newTags: tags,
        newStar: double.tryParse(star) ?? 0,
        newWordcount: int.tryParse(wordCount) ?? 0,
        newDesc: desc,
        newAuthor: author,
        newCover: cover,
        newIsFinish: int.tryParse(isFinish) ?? -1,
        newNewestchapter: lastChapter,
        newTocurl: tocUrl);
  }

  @override
  Future<QNTocModel> queryBookToc(QNBookModel model) async {
    var map = source['ruleToc'];
    if (map == null) {
      return QNTocModel.empty;
    }
    var url = model.tocUrl;
    if (map['transform'] != null && map['transform'].toString().trim() != '') {
      url = BNativeUtils.qn_querySingle(
          withRule: map['transform'],
          node: {'tocUrl': url},
          model: model,
          join: '\n',
          chapterModel: null,
          addParams: _userDefineMap);
    }
    var data = await BNativeUtils.qn_str_Network(url, source['g']?['headers']);
    GlobalPrev.currentNetworkData = data;
    final listRule = map['tocList'] ?? '';
    var dataList =
        BNativeUtils.qn_queryList(withRule: listRule, node: data, model: model);

    if (dataList.isEmpty) {
      return QNTocModel.empty;
    }
    if (dataList[0] is List) {
      dataList = dataList[0];
    }
    final List<QNTocInnerModel> innerList = [];
    for (var d in dataList) {
      final title = BNativeUtils.qn_querySingle(
          withRule: map['title'],
          node: d,
          model: model,
          addParams: _userDefineMap);
      final url = BNativeUtils.qn_querySingle(
          withRule: map['url'],
          node: d,
          model: model,
          addParams: _userDefineMap);
      final needVip = BNativeUtils.qn_querySingle(
          withRule: map['needVip'],
          node: d,
          model: model,
          addParams: _userDefineMap);
      final wordCount = BNativeUtils.qn_querySingle(
          withRule: map['wordCount'],
          node: d,
          model: model,
          addParams: _userDefineMap);
      innerList.add(QNTocInnerModel.init(
          title: title,
          url: url,
          needVip: needVip == '1',
          wordCount: int.tryParse(wordCount) ?? 0));
    }

    var nextUrls = BNativeUtils.qn_querySingle(
            withRule: map['nextTocUrl'],
            node: data,
            model: model,
            join: '\n',
            addParams: _userDefineMap)
        .trim();

    if (nextUrls.isEmpty) {
      return QNTocModel.init(
          bookId: model.bookId,
          updateTime: DateTime.now().millisecondsSinceEpoch,
          dataList: innerList);
    }
    Set<String> urlCache = {};
    while (nextUrls.isNotEmpty) {
      var splitList = nextUrls.split('\n');
      int length = splitList.length;
      if (length == 1) {
        final s = splitList[0];
        if (urlCache.contains(s)) {
          break;
        }
        urlCache.add(s);
        var sData =
            await BNativeUtils.qn_str_Network(s, source['g']?['headers']);
        final newList = BNativeUtils.qn_queryList(
            withRule: listRule, node: sData, model: model);
        if (newList.isEmpty) {
          continue;
        }
        for (var d in newList) {
          final title = BNativeUtils.qn_querySingle(
              withRule: map['title'],
              node: d,
              model: model,
              addParams: _userDefineMap);
          final url = BNativeUtils.qn_querySingle(
              withRule: map['url'],
              node: d,
              model: model,
              addParams: _userDefineMap);
          final needVip = BNativeUtils.qn_querySingle(
              withRule: map['needVip'],
              node: d,
              model: model,
              addParams: _userDefineMap);
          final wordCount = BNativeUtils.qn_querySingle(
              withRule: map['wordCount'],
              node: d,
              model: model,
              addParams: _userDefineMap);
          innerList.add(QNTocInnerModel.init(
              title: title,
              url: url,
              needVip: needVip == '1',
              wordCount: int.tryParse(wordCount) ?? 0));
        }
        nextUrls = BNativeUtils.qn_querySingle(
                withRule: map['nextTocUrl'],
                node: sData,
                model: model,
                addParams: _userDefineMap,
                join: '\n')
            .trim();
      } else {
        for (var s in splitList) {
          var sData =
              await BNativeUtils.qn_str_Network(s, source['g']?['headers']);
          final newList = BNativeUtils.qn_queryList(
              withRule: listRule, node: sData, model: model);
          if (newList.isEmpty) {
            continue;
          }
          for (var d in newList) {
            final title = BNativeUtils.qn_querySingle(
                withRule: map['title'],
                node: d,
                model: model,
                addParams: _userDefineMap);
            final url = BNativeUtils.qn_querySingle(
                withRule: map['url'],
                node: d,
                model: model,
                addParams: _userDefineMap);
            final needVip = BNativeUtils.qn_querySingle(
                withRule: map['needVip'],
                node: d,
                model: model,
                addParams: _userDefineMap);
            final wordCount = BNativeUtils.qn_querySingle(
                withRule: map['wordCount'],
                node: d,
                model: model,
                addParams: _userDefineMap);
            innerList.add(QNTocInnerModel.init(
                title: title,
                url: url,
                needVip: needVip == '1',
                wordCount: int.tryParse(wordCount) ?? 0));
          }
        }
        nextUrls = '';
      }
    }
    return QNTocModel.init(
        bookId: model.bookId,
        updateTime: DateTime.now().millisecondsSinceEpoch,
        dataList: innerList);
  }

  @override
  Future<dynamic> debugConfig(Map<String, String> debugMap) async {
    final parseRule = debugMap['parseType'];
    switch (parseRule) {
      case 'ruleSearch':
        return _debugSearch(debugMap);
      case 'ruleExplore':
        return _debugExplore(debugMap);
      case 'ruleDetail':
        return _debugDetail(debugMap);
      case 'ruleToc':
        return _debugToc(debugMap);
      case 'ruleContent':
        return _debugContent(debugMap);
      default:
        // 正常执行 http请求。
        return _debugNormalHttp(debugMap);
    }
  }

  @override
  Future<List<QNBookModel>> queryExplore(String exploreUrl) async {
    var data =
        await BNativeUtils.qn_str_Network(exploreUrl, source['g']?['headers']);
    GlobalPrev.currentNetworkData = data;
    var map = source['ruleExplore'];
    if (map == null) {
      return [];
    }
    final listRule = map['bookList'] ?? '';
    var dataList = BNativeUtils.qn_queryList(
        withRule: listRule, node: data, model: QNBookModel.empty);
    if (dataList.isEmpty) {
      return [];
    }
    final site = siteId();
    final siteVersion = currentSiteVersion();
    final List<QNBookModel> resultList = [];
    for (var d in dataList) {
      final detailRule = map['detailUrl'].toString();
      final detailUrl = BNativeUtils.qn_querySingle(
          withRule: detailRule,
          node: d,
          model: QNBookModel.empty,
          addParams: _userDefineMap);
      final bookModelTemp = QNBookModel.init(
          siteId: site,
          channelId: '',
          bookName: '',
          siteVersion: siteVersion,
          encode: '',
          detailUrl: detailUrl);
      if (detailRule.contains('@put')) {
        BNativeUtils.qn_querySingle(
            withRule: detailRule,
            node: d,
            model: bookModelTemp,
            addParams: _userDefineMap);
      }
      final bookName = BNativeUtils.qn_querySingle(
          withRule: map['bookName'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);
      if (bookName.isEmpty) {
        continue;
      }
      final cover = BNativeUtils.qn_querySingle(
          withRule: map['cover'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);
      final channelId = BNativeUtils.qn_querySingle(
          withRule: map['channelId'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);
      final author = BNativeUtils.qn_querySingle(
          withRule: map['author'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);
      final desc = BNativeUtils.qn_querySingle(
          withRule: map['desc'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);
      final tags = BNativeUtils.qn_querySingle(
          withRule: map['tags'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);
      final lastChapter = BNativeUtils.qn_querySingle(
          withRule: map['newestChapter'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);

      final wordCount = BNativeUtils.qn_querySingle(
          withRule: map['wordCount'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);
      final star = BNativeUtils.qn_querySingle(
          withRule: map['star'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);

      final isFinish = BNativeUtils.qn_querySingle(
          withRule: map['isFinish'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);
      final tocUrl = BNativeUtils.qn_querySingle(
          withRule: map['tocUrl'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);
      resultList.add(QNBookModel.init(
          siteId: site,
          channelId: channelId,
          bookName: bookName,
          siteVersion: siteVersion,
          encode: '',
          detailUrl: detailUrl,
          tags: tags.split('\n'),
          star: double.tryParse(star) ?? 8.5,
          wordCount: int.tryParse(wordCount) ?? 0,
          newestChapter: lastChapter,
          cover: cover,
          desc: desc,
          isFinish: int.tryParse(isFinish) ?? 0,
          author: author,
          tocUrl: tocUrl));
    }
    return resultList;
  }

  @override
  Future<List<QNBookModel>> searchBookName(String key) async {
    var map = source['ruleSearch'];
    if (map == null) {
      return [];
    }
    final url = BNativeUtils.qn_querySingle(
        withRule: source['searchUrl'],
        node: {'key': key},
        model: QNBookModel.empty,
        addParams: _userDefineMap);
    var data = await BNativeUtils.qn_str_Network(url, source['g']?['headers']);
    GlobalPrev.currentNetworkData = data;
    var dataList = BNativeUtils.qn_queryList(
        withRule: map['bookList'], node: data, model: QNBookModel.empty);
    if (dataList.isEmpty) {
      return [];
    }
    final site = siteId();
    final siteVersion = currentSiteVersion();
    final List<QNBookModel> resultList = [];
    int index = 0;
    for (var d in dataList) {
      final detailRule = map['detailUrl'].toString();
      final detailUrl = BNativeUtils.qn_querySingle(
          withRule: detailRule,
          node: d,
          model: QNBookModel.empty,
          addParams: _userDefineMap);
      final bookModelTemp = QNBookModel.init(
          siteId: site,
          bookName: '',
          channelId: '',
          siteVersion: siteVersion,
          encode: '',
          detailUrl: detailUrl);
      if (detailRule.contains('@put')) {
        BNativeUtils.qn_querySingle(
            withRule: detailRule,
            node: d,
            model: bookModelTemp,
            addParams: _userDefineMap);
      }
      final bookName = BNativeUtils.qn_querySingle(
          withRule: map['bookName'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);
      if (bookName.trim().isEmpty) {
        continue;
      }
      final cover = BNativeUtils.qn_querySingle(
          withRule: map['cover'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);
      final channelId = BNativeUtils.qn_querySingle(
          withRule: map['channelId'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);
      final author = BNativeUtils.qn_querySingle(
          withRule: map['author'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);
      final desc = BNativeUtils.qn_querySingle(
          withRule: map['desc'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);
      final tags = BNativeUtils.qn_querySingle(
          withRule: map['tags'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);
      final lastChapter = BNativeUtils.qn_querySingle(
          withRule: map['newestChapter'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);

      final wordCount = BNativeUtils.qn_querySingle(
          withRule: map['wordCount'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);
      final star = BNativeUtils.qn_querySingle(
          withRule: map['star'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);

      final isFinish = BNativeUtils.qn_querySingle(
          withRule: map['isFinish'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);

      final tocUrl = BNativeUtils.qn_querySingle(
          withRule: map['tocUrl'],
          node: d,
          model: bookModelTemp,
          addParams: _userDefineMap);

      resultList.add(QNBookModel.init(
          siteId: site,
          channelId: channelId,
          bookName: bookName,
          siteVersion: siteVersion,
          encode: '',
          detailUrl: detailUrl,
          tags: tags.split('\n'),
          star: double.tryParse(star) ?? 8.5,
          wordCount: int.tryParse(wordCount) ?? 0,
          newestChapter: lastChapter,
          cover: cover,
          desc: desc,
          author: author,
          isFinish: int.tryParse(isFinish) ?? 0,
          tocUrl: tocUrl));
      index++;
    }
    return resultList;
  }

  @override
  Future<QNContentModel> queryBookContent(
      QNBookModel model, String contentKey) async {
    var map = source['ruleContent'];
    if (map == null) {
      return QNContentModel.empty;
    }
    var chapterModel = BookSourceCache.i.getInnerToc(contentKey);
    var url = contentKey;
    if (map['transform'] != null && map['transform'].toString().trim() != '') {
      url = BNativeUtils.qn_querySingle(
          withRule: map['transform'],
          node: {'contentUrl': url},
          model: model,
          join: '\n',
          chapterModel: chapterModel,
          addParams: _userDefineMap);
    }
    var data = await BNativeUtils.qn_str_Network(url, source['g']?['headers']);
    GlobalPrev.currentNetworkData = data;
    var contentV = BNativeUtils.qn_queryContent(
        withRule: map['content'],
        node: data,
        model: model,
        join: '\n',
        chapterModel: chapterModel,
        addParams: _userDefineMap);

    var nextUrls = BNativeUtils.qn_querySingle(
            withRule: map['nextUrls'],
            node: data,
            model: model,
            chapterModel: chapterModel,
            addParams: _userDefineMap,
            join: '\n')
        .trim();

    if (nextUrls.isNotEmpty) {
      Set<String> urlCache = {};
      final nextJoin = BNativeUtils.qn_querySingle(
              withRule: map['nextJoin'],
              node: data,
              model: model,
              chapterModel: chapterModel,
              addParams: _userDefineMap)
          .trim();
      while (nextUrls.isNotEmpty) {
        var splitList = nextUrls.split('\n');
        int length = splitList.length;
        if (length == 1) {
          final s = splitList[0];
          if (urlCache.contains(s)) {
            break;
          }
          urlCache.add(s);
          var sData =
              await BNativeUtils.qn_str_Network(s, source['g']?['headers']);
          final nextV = BNativeUtils.qn_queryContent(
              withRule: map['content'],
              node: sData,
              model: model,
              join: '\n',
              chapterModel: chapterModel,
              addParams: _userDefineMap);
          contentV = contentV + nextJoin + nextV;
          nextUrls = BNativeUtils.qn_querySingle(
                  withRule: map['nextUrls'],
                  node: sData,
                  model: model,
                  chapterModel: chapterModel,
                  addParams: _userDefineMap,
                  join: '\n')
              .trim();
        } else {
          for (var s in splitList) {
            var sData =
                await BNativeUtils.qn_str_Network(s, source['g']?['headers']);
            final nextV = BNativeUtils.qn_queryContent(
                withRule: map['content'],
                node: sData,
                model: model,
                join: '\n',
                chapterModel: chapterModel,
                addParams: _userDefineMap);
            contentV = contentV + nextJoin + nextV;
          }
          nextUrls = '';
        }
      }
    }
    switch (_info!.type) {
      case 'book':
        return QNContentModel.init(
            contentKey: contentKey, bookContent: contentV);
      case 'listenbook':
        var indexOf = contentV.indexOf(',{');
        if (indexOf == -1) {
          return QNContentModel.init(
              contentKey: contentKey,
              musicContent: MusicContentModel.init(url: contentV));
        } else {
          var jsonV = contentV.substring(indexOf + 1);
          final newUrl = contentV.substring(0, indexOf);
          Map? headers = json.decode(jsonV)['headers'];
          if (headers == null) {
            return QNContentModel.init(
                contentKey: contentKey,
                musicContent: MusicContentModel.init(url: newUrl));
          } else {
            return QNContentModel.init(
                contentKey: contentKey,
                musicContent: MusicContentModel.init(
                    url: newUrl,
                    headers: headers.map((key, value) =>
                        MapEntry(key.toString(), value.toString()))));
          }
        }
      case 'comic':
        var splitList = contentV.split('\n');
        final List<ComicPicModel> picList = [];
        for (var s in splitList) {
          var indexOf = s.indexOf(',{');
          if (indexOf == -1) {
            picList.add(ComicPicModel.init(url: s));
          } else {
            var jsonV = s.substring(indexOf + 1);
            final newUrl = s.substring(0, indexOf);
            Map? headers = json.decode(jsonV)['headers'];
            if (headers == null) {
              picList.add(ComicPicModel.init(url: newUrl));
            } else {
              picList.add(ComicPicModel.init(
                  url: newUrl,
                  headers: headers.map((key, value) =>
                      MapEntry(key.toString(), value.toString()))));
            }
          }
        }
        return QNContentModel.init(
            contentKey: contentKey, comicPicList: picList);
      default:
        throw Exception('不存在的类型 ->${_info!.type}');
    }
  }

  @override
  Map<int, String> siteUpdateInfo() {
    throw UnimplementedError();
  }

  @override
  Map<String, String> verifyBookConfig() {
    final verifyMap = source['verify'];
    final Map<String, String> result = {};
    for (var entry in verifyMap.entries) {
      result[entry.key.toString()] = entry.value.toString();
    }
    return result;
  }

  Future _debugSearch(Map<String, dynamic> debugMap) async {
    var map = source['ruleSearch'];
    if (map == null) {
      return {
        'xy': '',
        'jx': {'ok': 0, 'errorMsg': "ruleSearch规则未配置"},
        'response': null
      };
    }

    final _injectP = jsonDecode(debugMap['injectParams'] ?? '{}');
    final Map<String, String> injectParams = {};
    for (var m in _injectP.entries) {
      injectParams[m.key] = m.value.toString();
    }
    injectParams.addAll(_userDefineMap);
    final url = BNativeUtils.qn_querySingle(
        withRule: debugMap['requestUrl'],
        node: injectParams,
        addParams: injectParams,
        model: QNBookModel.empty);
    var responseData =
        await BNativeUtils.qn_response_Network(url, source['g']?['headers']);
    final charset = responseData.extra['qn_charset'];
    final data = (charset == 'gbk')
        ? gbk.decode(responseData.data ?? [], allowMalformed: true)
        : utf8.decode(responseData.data ?? [], allowMalformed: true);

    GlobalPrev.currentNetworkData = data;
    var dataList = BNativeUtils.qn_queryList(
      withRule: map['bookList'],
      node: data,
      model: QNBookModel.empty,
    );
    if (dataList.isEmpty) {
      return {
        'xy': data,
        'jx': {
          'ok': 0,
          'errorMsg': "获取列表失败，请检查ruleSearch.bookList规则",
          'data': []
        },
        'response': responseData
      };
    }
    final site = siteId();
    final siteVersion = currentSiteVersion();
    final List<Map<String, dynamic>> resultList = [];
    int index = 0;
    for (var d in dataList) {
      final detailRule = map['detailUrl'].toString();
      final detailUrl = BNativeUtils.qn_querySingle(
          withRule: detailRule,
          node: d,
          model: QNBookModel.empty,
          addParams: injectParams);
      final bookModelTemp = QNBookModel.init(
          siteId: site,
          bookName: '',
          channelId: '',
          siteVersion: siteVersion,
          encode: '',
          detailUrl: detailUrl);
      if (detailRule.contains('@put')) {
        BNativeUtils.qn_querySingle(
            withRule: detailRule,
            node: d,
            model: bookModelTemp,
            addParams: injectParams);
      }
      final bookName = BNativeUtils.qn_querySingle(
          withRule: map['bookName'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      if (bookName.trim().isEmpty) {
        continue;
      }
      final cover = BNativeUtils.qn_querySingle(
          withRule: map['cover'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      final channelId = BNativeUtils.qn_querySingle(
          withRule: map['channelId'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      final author = BNativeUtils.qn_querySingle(
          withRule: map['author'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      final desc = BNativeUtils.qn_querySingle(
          withRule: map['desc'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      final tags = BNativeUtils.qn_querySingle(
          withRule: map['tags'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      final lastChapter = BNativeUtils.qn_querySingle(
          withRule: map['newestChapter'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);

      final wordCount = BNativeUtils.qn_querySingle(
          withRule: map['wordCount'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      final star = BNativeUtils.qn_querySingle(
          withRule: map['star'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      final isFinish = BNativeUtils.qn_querySingle(
          withRule: map['isFinish'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      final tocUrl = BNativeUtils.qn_querySingle(
          withRule: map['tocUrl'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);

      resultList.add(QNBookModel.init(
              siteId: site,
              channelId: channelId,
              bookName: bookName,
              siteVersion: siteVersion,
              encode: '',
              detailUrl: detailUrl,
              tags: tags.split('\n'),
              star: double.tryParse(star) ?? 8.5,
              wordCount: int.tryParse(wordCount) ?? 0,
              newestChapter: lastChapter,
              cover: cover,
              desc: desc,
              author: author,
              isFinish: int.tryParse(isFinish) ?? 0,
              tocUrl: tocUrl)
          .toMap());
      index++;
    }
    return {
      'xy': data,
      'jx': {'ok': 1, 'data': resultList},
      'response': responseData
    };
  }

  Future _debugExplore(Map<String, dynamic> debugMap) async {
    var map = source['ruleExplore'];
    if (map == null) {
      return {
        'xy': '',
        'jx': {'ok': 0, 'errorMsg': "ruleExplore规则未配置"},
        'response': null
      };
    }
    final _injectP = jsonDecode(debugMap['injectParams'] ?? '{}');
    final Map<String, String> injectParams = {};
    for (var m in _injectP.entries) {
      injectParams[m.key] = m.value.toString();
    }
    injectParams.addAll(_userDefineMap);
    final url = BNativeUtils.qn_querySingle(
        withRule: debugMap['requestUrl'],
        node: injectParams,
        addParams: injectParams,
        model: QNBookModel.empty);
    var responseData =
        await BNativeUtils.qn_response_Network(url, source['g']?['headers']);
    final charset = responseData.extra['qn_charset'];
    final data = (charset == 'gbk')
        ? gbk.decode(responseData.data ?? [], allowMalformed: true)
        : utf8.decode(responseData.data ?? [], allowMalformed: true);

    final listRule = map['bookList'] ?? '';
    var dataList = BNativeUtils.qn_queryList(
        withRule: listRule, node: data, model: QNBookModel.empty);
    if (dataList.isEmpty) {
      return {
        'xy': data,
        'jx': {
          'ok': 0,
          'errorMsg': "获取列表失败，请检查ruleExplore.bookList规则",
          'data': []
        },
        'response': responseData
      };
    }
    final site = siteId();
    final siteVersion = currentSiteVersion();
    final List<Map<String, dynamic>> resultList = [];

    for (var d in dataList) {
      final detailRule = map['detailUrl'].toString();
      final detailUrl = BNativeUtils.qn_querySingle(
          withRule: detailRule,
          node: d,
          model: QNBookModel.empty,
          addParams: injectParams);
      final bookModelTemp = QNBookModel.init(
          siteId: site,
          channelId: '',
          bookName: '',
          siteVersion: siteVersion,
          encode: '',
          detailUrl: detailUrl);
      if (detailRule.contains('@put')) {
        BNativeUtils.qn_querySingle(
            withRule: detailRule,
            node: d,
            model: bookModelTemp,
            addParams: injectParams);
      }
      final bookName = BNativeUtils.qn_querySingle(
          withRule: map['bookName'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      if (bookName.isEmpty) {
        continue;
      }
      final cover = BNativeUtils.qn_querySingle(
          withRule: map['cover'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      final channelId = BNativeUtils.qn_querySingle(
          withRule: map['channelId'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      final author = BNativeUtils.qn_querySingle(
          withRule: map['author'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      final desc = BNativeUtils.qn_querySingle(
          withRule: map['desc'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      final tags = BNativeUtils.qn_querySingle(
          withRule: map['tags'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      final lastChapter = BNativeUtils.qn_querySingle(
          withRule: map['newestChapter'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);

      final wordCount = BNativeUtils.qn_querySingle(
          withRule: map['wordCount'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      final star = BNativeUtils.qn_querySingle(
          withRule: map['star'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      final isFinish = BNativeUtils.qn_querySingle(
          withRule: map['isFinish'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      final tocUrl = BNativeUtils.qn_querySingle(
          withRule: map['tocUrl'],
          node: d,
          model: bookModelTemp,
          addParams: injectParams);
      resultList.add(QNBookModel.init(
              siteId: site,
              channelId: channelId,
              bookName: bookName,
              siteVersion: siteVersion,
              encode: '',
              detailUrl: detailUrl,
              tags: tags.split('\n'),
              star: double.tryParse(star) ?? 8.5,
              wordCount: int.tryParse(wordCount) ?? 0,
              newestChapter: lastChapter,
              cover: cover,
              desc: desc,
              author: author,
              isFinish: int.tryParse(isFinish) ?? 0,
              tocUrl: tocUrl)
          .toMap());
    }
    return {
      'xy': data,
      'jx': {'ok': 1, 'data': resultList},
      'response': responseData
    };
  }

  Future _debugDetail(Map<String, dynamic> debugMap) async {
    final _injectP = jsonDecode(debugMap['injectParams'] ?? '{}');
    final Map<String, String> injectParams = {};
    for (var m in _injectP.entries) {
      injectParams[m.key] = m.value.toString();
    }
    injectParams.addAll(_userDefineMap);
    final url = BNativeUtils.qn_querySingle(
        withRule: debugMap['requestUrl'],
        node: injectParams,
        addParams: injectParams,
        model: QNBookModel.empty);

    final model = BUtils.qn_QueryBookBy(bookId: HashM.hashCodeGet(url)) ??
        QNBookModel.init(
            siteId: siteId(),
            bookName: '',
            siteVersion: currentSiteVersion(),
            encode: '',
            detailUrl: url,
            channelId: '');

    var map = source['ruleDetail'];
    if (map == null) {
      return {
        'xy': '',
        'jx': {'ok': 1, 'data': model.toMap()},
        'response': null
      };
    }

    var responseData =
        await BNativeUtils.qn_response_Network(url, source['g']?['headers']);
    final charset = responseData.extra['qn_charset'];
    final data = (charset == 'gbk')
        ? gbk.decode(responseData.data ?? [], allowMalformed: true)
        : utf8.decode(responseData.data ?? [], allowMalformed: true);
    GlobalPrev.currentNetworkData = data;
    final cover = BNativeUtils.qn_querySingle(
        withRule: map['cover'],
        node: data,
        model: model,
        addParams: injectParams);
    final author = BNativeUtils.qn_querySingle(
        withRule: map['author'],
        node: data,
        model: model,
        addParams: injectParams);
    final desc = BNativeUtils.qn_querySingle(
        withRule: map['desc'],
        node: data,
        model: model,
        addParams: injectParams);
    final tags = BNativeUtils.qn_querySingle(
            withRule: map['tags'],
            node: data,
            model: model,
            addParams: injectParams)
        .split('\n');
    final lastChapter = BNativeUtils.qn_querySingle(
        withRule: map['newestChapter'],
        node: data,
        model: model,
        addParams: injectParams);
    final bookName = BNativeUtils.qn_querySingle(
        withRule: map['bookName'],
        node: data,
        model: model,
        addParams: injectParams);
    final wordCount = BNativeUtils.qn_querySingle(
        withRule: map['wordCount'],
        node: data,
        model: model,
        addParams: injectParams);

    final star = BNativeUtils.qn_querySingle(
        withRule: map['star'],
        node: data,
        model: model,
        addParams: injectParams);

    final isFinish = BNativeUtils.qn_querySingle(
        withRule: map['isFinish'],
        node: data,
        model: model,
        addParams: injectParams);

    final tocUrl = BNativeUtils.qn_querySingle(
        withRule: map['tocUrl'],
        node: data,
        model: model,
        addParams: injectParams);
    return {
      'xy': data,
      'jx': {
        'ok': 1,
        'data': QNBookModel.copyWith(model,
                newBookname: bookName,
                newSiteVersion: currentSiteVersion(),
                newTags: tags,
                newStar: double.tryParse(star) ?? 0,
                newWordcount: int.tryParse(wordCount) ?? 0,
                newDesc: desc,
                newAuthor: author,
                newCover: cover,
                newIsFinish: int.tryParse(isFinish) ?? -1,
                newNewestchapter: lastChapter,
                newTocurl: tocUrl)
            .toMap()
      },
      'response': responseData
    };
  }

  Future _debugToc(Map<String, dynamic> debugMap) async {
    var map = source['ruleToc'];
    if (map == null) {
      return {
        'xy': '',
        'jx': {'ok': 0, 'errorMsg': "ruleToc规则未配置"},
        'response': null
      };
    }

    final _injectP = jsonDecode(debugMap['injectParams'] ?? '{}');
    final Map<String, String> injectParams = {};
    for (var m in _injectP.entries) {
      injectParams[m.key] = m.value.toString();
    }
    injectParams.addAll(_userDefineMap);
    final detailUrl = debugMap['detailUrl'] ?? '';
    final model = BookSourceCache.i.get(HashM.hashCodeGet(detailUrl)) ??
        QNBookModel.empty;
    var url = BNativeUtils.qn_querySingle(
        withRule: debugMap['requestUrl'],
        node: injectParams,
        addParams: injectParams,
        model: model);
    if (map['transform'] != null && map['transform'].toString().trim() != '') {
      url = BNativeUtils.qn_querySingle(
          withRule: map['transform'],
          node: {'tocUrl': url},
          model: model,
          join: '\n',
          addParams: injectParams,
          chapterModel: null);
    }
    var responseData =
        await BNativeUtils.qn_response_Network(url, source['g']?['headers']);
    final charset = responseData.extra['qn_charset'];
    var data = (charset == 'gbk')
        ? gbk.decode(responseData.data ?? [], allowMalformed: true)
        : utf8.decode(responseData.data ?? [], allowMalformed: true);
    GlobalPrev.currentNetworkData = data;

    final listRule = map['tocList'] ?? '';
    var dataList =
        BNativeUtils.qn_queryList(withRule: listRule, node: data, model: model);
    if (dataList.isEmpty) {
      return {
        'xy': data,
        'jx': {
          'ok': 0,
          'errorMsg': "获取列表失败，请检查ruleToc.transform或者ruleToc.tocList规则",
          'data': []
        },
        'response': responseData
      };
    }
    if (dataList[0] is List) {
      dataList = dataList[0];
    }
    final List<Map<String, dynamic>> innerList = [];
    for (var d in dataList) {
      final title = BNativeUtils.qn_querySingle(
          withRule: map['title'],
          node: d,
          model: model,
          addParams: injectParams);
      final url = BNativeUtils.qn_querySingle(
          withRule: map['url'], node: d, model: model, addParams: injectParams);
      final needVip = BNativeUtils.qn_querySingle(
          withRule: map['needVip'],
          node: d,
          model: model,
          addParams: injectParams);
      final wordCount = BNativeUtils.qn_querySingle(
          withRule: map['wordCount'],
          node: d,
          model: model,
          addParams: injectParams);
      innerList.add(QNTocInnerModel.init(
              title: title,
              url: url,
              needVip: needVip == '1',
              wordCount: int.tryParse(wordCount) ?? 0)
          .toMap());
    }

    var nextUrls = BNativeUtils.qn_querySingle(
            withRule: map['nextTocUrl'],
            node: data,
            model: model,
            join: '\n',
            addParams: injectParams)
        .trim();

    if (nextUrls.isEmpty) {
      return {
        'xy': data,
        'jx': {'ok': 1, 'tocCount': innerList.length, 'data': innerList},
        'response': responseData
      };
    }
    Set<String> urlCache = {};
    while (nextUrls.isNotEmpty) {
      var splitList = nextUrls.split('\n');
      int length = splitList.length;
      if (length == 1) {
        final s = splitList[0];
        if (urlCache.contains(s)) {
          break;
        }
        urlCache.add(s);
        var sData =
            await BNativeUtils.qn_str_Network(s, source['g']?['headers']);
        final newList = BNativeUtils.qn_queryList(
            withRule: listRule, node: sData, model: model);
        if (newList.isEmpty) {
          continue;
        }
        for (var d in newList) {
          final title = BNativeUtils.qn_querySingle(
              withRule: map['title'],
              node: d,
              model: model,
              addParams: injectParams);
          final url = BNativeUtils.qn_querySingle(
              withRule: map['url'],
              node: d,
              model: model,
              addParams: injectParams);
          final needVip = BNativeUtils.qn_querySingle(
              withRule: map['needVip'],
              node: d,
              model: model,
              addParams: injectParams);
          final wordCount = BNativeUtils.qn_querySingle(
              withRule: map['wordCount'],
              node: d,
              model: model,
              addParams: injectParams);
          innerList.add(QNTocInnerModel.init(
                  title: title,
                  url: url,
                  needVip: needVip == '1',
                  wordCount: int.tryParse(wordCount) ?? 0)
              .toMap());
        }
        nextUrls = BNativeUtils.qn_querySingle(
                withRule: map['nextTocUrl'],
                node: sData,
                model: model,
                addParams: injectParams,
                join: '\n')
            .trim();
      } else {
        for (var s in splitList) {
          var sData =
              await BNativeUtils.qn_str_Network(s, source['g']?['headers']);
          final newList = BNativeUtils.qn_queryList(
              withRule: listRule, node: sData, model: model);
          if (newList.isEmpty) {
            continue;
          }
          for (var d in newList) {
            final title = BNativeUtils.qn_querySingle(
                withRule: map['title'],
                node: d,
                model: model,
                addParams: injectParams);
            final url = BNativeUtils.qn_querySingle(
                withRule: map['url'],
                node: d,
                model: model,
                addParams: injectParams);
            final needVip = BNativeUtils.qn_querySingle(
                withRule: map['needVip'],
                node: d,
                model: model,
                addParams: injectParams);
            final wordCount = BNativeUtils.qn_querySingle(
                withRule: map['wordCount'],
                node: d,
                model: model,
                addParams: injectParams);
            innerList.add(QNTocInnerModel.init(
                    title: title,
                    url: url,
                    needVip: needVip == '1',
                    wordCount: int.tryParse(wordCount) ?? 0)
                .toMap());
          }
        }
        nextUrls = '';
      }
    }

    return {
      'xy': data,
      'jx': {'ok': 1, 'tocCount': innerList.length, 'data': innerList},
      'response': responseData
    };
  }

  Future _debugContent(Map<String, dynamic> debugMap) async {
    var map = source['ruleContent'];

    if (map == null) {
      return {
        'xy': '',
        'jx': {'ok': 0, 'errorMsg': "ruleContent规则未配置"},
        'response': null
      };
    }
    final _injectP = jsonDecode(debugMap['injectParams'] ?? '{}');
    final Map<String, String> injectParams = {};
    for (var m in _injectP.entries) {
      injectParams[m.key] = m.value.toString();
    }
    injectParams.addAll(_userDefineMap);
    final detailUrl = debugMap['detailUrl'] ?? '';
    final model = BookSourceCache.i.get(HashM.hashCodeGet(detailUrl)) ??
        QNBookModel.empty;
    final contentKey = BNativeUtils.qn_querySingle(
        withRule: debugMap['requestUrl'],
        node: injectParams,
        addParams: injectParams,
        model: model);
    var chapterModel = BookSourceCache.i.getInnerToc(contentKey);
    var url = contentKey;
    if (map['transform'] != null && map['transform'].toString().trim() != '') {
      url = BNativeUtils.qn_querySingle(
          withRule: map['transform'],
          node: {'contentUrl': url},
          model: model,
          join: '\n',
          addParams: injectParams,
          chapterModel: null);
    }
    var responseData =
        await BNativeUtils.qn_response_Network(url, source['g']?['headers']);
    final charset = responseData.extra['qn_charset'];
    var data = (charset == 'gbk')
        ? gbk.decode(responseData.data ?? [], allowMalformed: true)
        : utf8.decode(responseData.data ?? [], allowMalformed: true);
    GlobalPrev.currentNetworkData = data;
    var contentV = BNativeUtils.qn_queryContent(
        withRule: map['content'],
        node: data,
        model: model,
        join: '\n',
        chapterModel: chapterModel,
        addParams: _userDefineMap);

    var nextUrls = BNativeUtils.qn_querySingle(
            withRule: map['nextUrls'],
            node: data,
            model: model,
            chapterModel: chapterModel,
            addParams: injectParams,
            join: '\n')
        .trim();

    if (nextUrls.isNotEmpty) {
      Set<String> urlCache = {};
      final nextJoin = BNativeUtils.qn_querySingle(
              withRule: map['nextJoin'],
              node: data,
              model: model,
              chapterModel: chapterModel,
              addParams: injectParams)
          .trim();
      while (nextUrls.isNotEmpty) {
        var splitList = nextUrls.split('\n');
        int length = splitList.length;
        if (length == 1) {
          final s = splitList[0];
          if (urlCache.contains(s)) {
            break;
          }
          urlCache.add(s);
          var sData =
              await BNativeUtils.qn_str_Network(s, source['g']?['headers']);
          final nextV = BNativeUtils.qn_queryContent(
              withRule: map['content'],
              node: sData,
              model: model,
              join: '\n',
              chapterModel: chapterModel,
              addParams: injectParams);
          contentV = contentV + nextJoin + nextV;
          nextUrls = BNativeUtils.qn_querySingle(
                  withRule: map['nextUrls'],
                  node: sData,
                  model: model,
                  chapterModel: chapterModel,
                  join: '\n',
                  addParams: injectParams)
              .trim();
        } else {
          for (var s in splitList) {
            var sData =
                await BNativeUtils.qn_str_Network(s, source['g']?['headers']);
            final nextV = BNativeUtils.qn_queryContent(
                withRule: map['content'],
                node: sData,
                model: model,
                join: '\n',
                chapterModel: chapterModel,
                addParams: injectParams);
            contentV = contentV + nextJoin + nextV;
          }
          nextUrls = '';
        }
      }
    }
    switch (_info!.type) {
      case 'book':
        return {
          'xy': data,
          'jx': {'ok': 1, 'type': 'book', 'bookContent': contentV},
          'response': responseData
        };
      // return QNContentModel.init(
      //     contentKey: contentKey, bookContent: contentV);
      case 'listenbook':
        var indexOf = contentV.indexOf(',{');
        if (indexOf == -1) {
          return {
            'xy': data,
            'jx': {
              'ok': 1,
              'type': 'listenbook',
              'musicContent': MusicContentModel.init(url: contentV).toMap()
            },
            'response': responseData
          };
          // return QNContentModel.init(
          //     contentKey: contentKey,
          //    musicContent: MusicContentModel.init(url: contentV));
        } else {
          var jsonV = contentV.substring(indexOf + 1);
          final newUrl = contentV.substring(0, indexOf);
          Map? headers = json.decode(jsonV)['headers'];
          if (headers == null) {
            return {
              'xy': data,
              'jx': {
                'ok': 1,
                'type': 'listenbook',
                'musicContent': MusicContentModel.init(url: newUrl).toMap()
              },
              'response': responseData
            };
            // return QNContentModel.init(
            //     contentKey: contentKey,
            //     musicContent: MusicContentModel.init(url: newUrl));
          } else {
            return {
              'xy': data,
              'jx': {
                'ok': 1,
                'type': 'listenbook',
                'musicContent': MusicContentModel.init(
                    url: newUrl,
                    headers: headers.map((key, value) =>
                        MapEntry(key.toString(), value.toString()))).toMap()
              },
              'response': responseData
            };
            // return QNContentModel.init(
            //     contentKey: contentKey,
            //     musicContent: MusicContentModel.init(
            //         url: newUrl,
            //         headers: headers.map((key, value) =>
            //             MapEntry(key.toString(), value.toString()))));
          }
        }
      case 'comic':
        var splitList = contentV.split('\n');
        final List<ComicPicModel> picList = [];
        for (var s in splitList) {
          var indexOf = s.indexOf(',{');
          if (indexOf == -1) {
            picList.add(ComicPicModel.init(url: s));
          } else {
            var jsonV = s.substring(indexOf + 1);
            final newUrl = s.substring(0, indexOf);
            Map? headers = json.decode(jsonV)['headers'];
            if (headers == null) {
              picList.add(ComicPicModel.init(url: newUrl));
            } else {
              picList.add(ComicPicModel.init(
                  url: newUrl,
                  headers: headers.map((key, value) =>
                      MapEntry(key.toString(), value.toString()))));
            }
          }
        }
        return {
          'xy': data,
          'jx': {
            'ok': 1,
            'type': 'comic',
            'comicPicList': picList.map((e) => e.toMap()).toList()
          },
          'response': responseData
        };
      default:
        throw Exception('不存在的类型 ->${_info!.type}');
    }
  }

  Future _debugNormalHttp(Map<String, dynamic> debugMap) async {}
}
