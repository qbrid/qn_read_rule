import 'package:qn_read_rule/book_provide/base_model.dart';

class BookSourceCache {
  static final BookSourceCache _instance = BookSourceCache._internal();

  factory BookSourceCache() => _instance;

  static BookSourceCache get i => _instance;

  BookSourceCache._internal();

  final Map<int, QNBookModel> _cache = {};

  final Map<int, QNTocModel> _tocCache = {};

  final Map<int, QNTocInnerModel> _innerTocCache = {};

  final Map<int, QNContentModel> _contentCache = {};
  final Map<int, Map<String, String>> _kvMap = {};

  void add(QNBookModel source) {
    _cache[source.bookId] = source;
  }

  void addToc(QNTocModel source) {
    _tocCache[source.bookId] = source;
    for (var element in source.dataList) {
      _innerTocCache[element.url.hashCode] = element;
    }
  }

  void addInnerToc(QNTocInnerModel source) {
    _innerTocCache[source.url.hashCode] = source;
  }

  void addContent(QNContentModel source) {
    _contentCache[source.id] = source;
  }

  void addAll(List<QNBookModel> modelList) {
    for (var element in modelList) {
      _cache[element.bookId] = element;
    }
  }

  void addAllContent(List<QNContentModel> modelList) {
    for (var element in modelList) {
      _contentCache[element.id] = element;
    }
  }

  void remove(int id) {
    _cache.remove(id);
  }

  void removeToc(int id) {
    _tocCache.remove(id);
  }

  void removeContent(int id) {
    _contentCache.remove(id);
  }

  QNBookModel? get(int id) {
    return _cache[id];
  }

  bool containBookId(int id) {
    return _cache.containsKey(id);
  }

  QNTocInnerModel getInnerToc(String contentKey) {
    return _innerTocCache[contentKey.hashCode] ??
        QNTocInnerModel.init(title: 'unknown', url: 'unknown', needVip: false);
  }

  QNTocModel? getToc(int id) {
    return _tocCache[id];
  }

  void clear() {
    _cache.clear();
  }

  void qnPut(int bookId, String key, String value) {
    var map = _kvMap.putIfAbsent(bookId, () => {});
    map[key] = value;
  }

  String? qnGet(int bookId, String key) {
    var kvMap = _kvMap[bookId];

    return kvMap == null ? null : kvMap[key];
  }

  Map<String, String>? qnGetAllKV(int bookId) {
    return _kvMap[bookId];
  }
}
