import 'package:qn_read_rule/book_provide/base.dart';
import 'package:qn_read_rule/book_provide/base_model.dart';

class ErrorProvide extends BookSourceProvide {
  final String site;

  ErrorProvide(this.site) : super();

  @override
  int currentSiteVersion() {
    // TODO: implement currentSiteVersion
    return 0;
  }

  @override
  QNExploreModel exploreModel() {
    // TODO: implement exploreModel
    return QNExploreModel.empty;
  }

  @override
  QNBaseModel info() {
    // TODO: implement info
    return QNBaseModel.init(
        siteId: site,
        type: 'unknown',
        showName: '未知',
        group: '',
        desc: '',
        comicShowType: '',
        enable: false,
        updateTime: 0,
        versionNumb: 0,
        supportExplore: false,
        vpnWebsite: '',
        supportSearchBookName: false,
        isEncrypt: false);
  }

  @override
  int migrate(int oldVersion) {
    // TODO: implement migrate
    return 0;
  }

  @override
  Future<QNContentModel> queryBookContent(
      QNBookModel model, String contentKey) {
    // TODO: implement queryBookContent
    throw UnimplementedError();
  }

  @override
  Future<QNBookModel> queryBookDetail(QNBookModel model) {
    // TODO: implement queryBookDetail
    throw UnimplementedError();
  }

  @override
  Future<QNTocModel> queryBookToc(QNBookModel model) {
    // TODO: implement queryBookToc
    throw UnimplementedError();
  }

  @override
  Future<List<QNBookModel>> queryExplore(String exploreUrl) {
    // TODO: implement queryExplore
    throw UnimplementedError();
  }

  @override
  Future<List<QNBookModel>> searchBookName(String key) {
    // TODO: implement searchBookName
    throw UnimplementedError();
  }

  @override
  String siteId() {
    // TODO: implement siteId
    return site;
  }

  @override
  Map<int, String> siteUpdateInfo() {
    // TODO: implement siteUpdateInfo
    throw UnimplementedError();
  }

  @override
  Map<String, String> verifyBookConfig() {
    // TODO: implement verifyBookConfig
    throw UnimplementedError();
  }
}
