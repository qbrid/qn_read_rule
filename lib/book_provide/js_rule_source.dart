import 'dart:convert';

import 'package:qn_read_rule/book_provide/base.dart';
import 'package:qn_read_rule/book_provide/base_model.dart';
import 'package:qn_read_rule/utils/js_utils.dart';
import 'package:qn_read_rule/utils/logger.dart';

class JSRuleSource extends BookSourceProvide {
  String js;

  late String className;

  late String jsPrefix;

  String? siteIdCache;

  QNBaseModel? infoCache;
  QNExploreModel? exploreCache;

  int? currentVersionCache;

  JSRuleSource.initWith(this.js) {
    RegExp regExp = RegExp(r'class (?<className>.*) {');
    var firstMatch = regExp.firstMatch(js);
    var className = 'default_class';
    if (firstMatch != null) {
      className = firstMatch.namedGroup('className') ?? '';
    }

    int currentTime = DateTime.now().millisecondsSinceEpoch;
    final newClassName = '${className}_$currentTime';
    js = js.replaceFirst(className, newClassName);
    jsPrefix = "GLOBAL_RULE_MAP['$newClassName']";
    JSUtils.evaluateSync(
        "$js\nGLOBAL_RULE_MAP['$newClassName'] = new $newClassName();");
  }

  @override
  int currentSiteVersion() {
    if (currentVersionCache != null) {
      return currentVersionCache!;
    }
    final result = JSUtils.evaluateSync(" $jsPrefix.currentSiteVersion;");
    if (result is double) {
      currentVersionCache = result.toInt();
    } else {
      currentVersionCache = int.tryParse(result.toString()) ?? -1;
    }
    return currentVersionCache!;
  }

  @override
  QNExploreModel exploreModel() {
    if (exploreCache != null) {
      return exploreCache!;
    }
    final map = JSUtils.evaluateSync(" $jsPrefix.exploreModel();");
    if (map == null) {
      return const QNExploreModel.init(boyExploreList: [], girlExploreList: []);
    }
    final boyList = map['boy'];
    List<QNExploreItem> boys = [];
    if (boyList != null) {
      for (Map m in boyList) {
        boys.add(QNExploreItem(tag: m['tag'], url: m['url']));
      }
    }
    final girlList = map['girl'];
    List<QNExploreItem> girls = [];
    if (girlList != null) {
      for (Map m in girlList) {
        girls.add(QNExploreItem(tag: m['tag'], url: m['url']));
      }
    }
    exploreCache =
        QNExploreModel.init(boyExploreList: boys, girlExploreList: girls);
    return exploreCache!;
  }

  @override
  QNBaseModel info() {
    if (infoCache != null) {
      return infoCache!;
    }
    var m = JSUtils.evaluateSync(" $jsPrefix.info;");
    if (m is String) {
      m = jsonDecode(m);
    }
    infoCache = QNBaseModel.init(
        siteId: m['siteId'],
        type: m['type'],
        showName: m['showName'],
        group: m['group'] ?? '',
        desc: m['desc'] ?? '',
        comicShowType: m['comicShowType'] ?? '',
        enable: m['enable'] ?? true,
        updateTime: _transformInt(m['updateTime'], 0),
        versionNumb: m['versionNumb'] ?? 0,
        supportExplore: m['supportExplore'] ?? false,
        vpnWebsite: m['vpnWebsite'] ?? '',
        supportSearchBookName: (m['supportSearchBookName'] ?? false) ||
            (m['supportSearch'] ?? false),
        isEncrypt: m['isEncrypt'] ?? false);
    return infoCache!;
  }

  @override
  int migrate(int oldVersion) {
    return JSUtils.evaluateSync(" $jsPrefix.migrate('$oldVersion');");
  }

  @override
  Future<QNContentModel> queryBookContent(
      QNBookModel model, String contentKey) async {
    final modelJson = jsonEncode(model.toMap());
    final contentJS = "$jsPrefix.queryBookContent($modelJson,'$contentKey')";
    Logger.debug(() => "contentJS -> $contentJS");
    final map = await JSUtils.evaluateAsync(contentJS);
    final bookContent = map['bookContent'] ?? '';
    final List<ComicPicModel> picModels = [];
    if (map['comicPicList'] != null) {
      for (Map m in map['comicPicList']) {
        final Map<String, String> headers = {};
        if (m['headers'] != null && m['headers'] is Map) {
          for (MapEntry entry in m['headers'].entries) {
            headers[entry.key.toString()] = entry.value.toString();
          }
        }
        final model = ComicPicModel.init(
            url: (m['url'] ?? '').toString(),
            headers: headers,
            w: _transformInt(m['w'], 0),
            h: _transformInt(m['h'], 0));
        picModels.add(model);
      }
    }
    MusicContentModel musicContentModel = const MusicContentModel.init(url: '');
    if (map['musicContent'] != null) {
      Map m = map['musicContent'];
      musicContentModel = MusicContentModel.init(
          url: m['url'] ?? '',
          headers: m['headers'] ?? {},
          duration: _transformInt(m['duration'], 0));
    }

    return QNContentModel.init(
        contentKey: contentKey,
        bookContent: bookContent,
        comicPicList: picModels,
        musicContent: musicContentModel);
  }

  @override
  Future<QNBookModel> queryBookDetail(QNBookModel model) async {
    final modelJson = jsonEncode(model.toMap());
    final map =
        await JSUtils.evaluateAsync(" $jsPrefix.queryBookDetail($modelJson);");
    return QNBookModel.copyWith(model,
        newWordcount: _transformInt(map['wordCount'], 0),
        newTags: _transformTags(map['tags']),
        newTocurl: map['tocUrl'] ?? '',
        newAuthor: map['author'] ?? '',
        newBookname: map['bookName'] ?? '',
        newCover: map['cover'] ?? '',
        newDesc: map['desc'] ?? '',
        newNewestchapter: map['newestChapter'] ?? '',
        newSiteVersion: currentVersionCache ?? currentSiteVersion(),
        newStar: _transformDouble(map['star'], 8.5));
  }

  @override
  Future<QNTocModel> queryBookToc(QNBookModel model) async {
    final modelJson = jsonEncode(model.toMap());
    final map =
        await JSUtils.evaluateAsync(" $jsPrefix.queryBookToc($modelJson);");
    final List<QNTocInnerModel> tocList = [];
    for (var data in map) {
      tocList.add(QNTocInnerModel.init(
          title: data['title'],
          url: data['url'],
          needVip: data['needVip'] ?? false));
    }
    return QNTocModel.init(
        bookId: model.bookId,
        updateTime: DateTime.now().millisecondsSinceEpoch,
        dataList: tocList);
  }

  @override
  Future<List<QNBookModel>> queryExplore(String exploreUrl) async {
    var dataList =
        await JSUtils.evaluateAsync(" $jsPrefix.queryExplore('$exploreUrl');");
    if (dataList == null) {
      return [];
    }
    List<QNBookModel> resultList = [];
    for (Map m in dataList) {
      final model = QNBookModel.init(
        siteId: siteIdCache ?? siteId(),
        bookName: m['bookName'] ?? '',
        channelId: m['channelId'] ?? '',
        siteVersion: currentVersionCache ?? currentSiteVersion(),
        encode: '',
        detailUrl: m['detailUrl'] ?? '',
        tags: _transformTags(m['tags']),
        star: _transformDouble(m['star'], 8.5),
        wordCount: _transformInt(m['wordCount'], 0),
        newestChapter: m['newestChapter'] ?? '',
        tocUrl: m['tocUrl'] ?? '',
        cover: m['cover'] ?? '',
        desc: m['desc'] ?? '',
        author: m['author'] ?? '',
      );
      resultList.add(model);
    }
    return resultList;
  }

  @override
  Future<List<QNBookModel>> searchBookName(String key) async {
    final searchJS = " $jsPrefix.searchBookName('$key');";
    var dataList = await JSUtils.evaluateAsync(searchJS);
    if (dataList == null) {
      return [];
    }
    List<QNBookModel> resultList = [];
    for (Map m in dataList) {
      final model = QNBookModel.init(
        siteId: siteIdCache ?? siteId(),
        bookName: m['bookName'] ?? '',
        channelId: m['channelId'] ?? '',
        siteVersion: currentVersionCache ?? currentSiteVersion(),
        encode: '',
        detailUrl: m['detailUrl'] ?? '',
        tags: _transformTags(m['tags']),
        star: _transformDouble(m['star'], 8.5),
        wordCount: _transformInt(m['wordCount'], 0),
        newestChapter: m['newestChapter'] ?? '',
        tocUrl: m['tocUrl'] ?? '',
        cover: m['cover'] ?? '',
        desc: m['desc'] ?? '',
        author: m['author'] ?? '',
      );
      resultList.add(model);
    }
    return resultList;
  }

  @override
  String siteId() {
    if (siteIdCache != null) {
      return siteIdCache!;
    }
    siteIdCache = JSUtils.evaluateSync("$jsPrefix.siteId");
    return siteIdCache!;
  }

  @override
  Map<int, String> siteUpdateInfo() {
    var m = JSUtils.evaluateSync("$jsPrefix.siteUpdateInfo");
    if (m == null) {
      return {};
    }
    return m.map((key, value) => MapEntry(key as int, value.toString()));
  }

  @override
  Map<String, String> verifyBookConfig() {
    var m = JSUtils.evaluateSync("$jsPrefix.verifyBookConfig");
    if (m == null) {
      return {};
    }
    Map<String, String> result = {};
    if (m is String) {
      var jsonMap = jsonDecode(m);
      for (var entry in jsonMap.entries) {
        result[entry.key.toString()] = entry.value.toString();
      }
    } else {
      for (var entry in m.entries) {
        result[entry.key.toString()] = entry.value.toString();
      }
    }

    return result;
  }

  double _transformDouble(dynamic m, double defaultV) {
    if (m == null || m == '') {
      return defaultV;
    }
    return double.tryParse(m.toString()) ?? defaultV;
  }

  int _transformInt(dynamic m, int defaultI) {
    if (m == null || m == '') {
      return defaultI;
    }
    return int.tryParse(m.toString()) ?? defaultI;
  }

  List<String> _transformTags(dynamic m) {
    if (m == null) {
      return [];
    }
    if (m is List) {
      return m
          .map((e) => e.toString())
          .map((e) => e.trim())
          .where((element) => element.isNotEmpty)
          .toList();
    } else {
      var string = m.toString();
      return string
          .split('\n')
          .map((e) => e.trim())
          .where((element) => element.isNotEmpty)
          .toList();
    }
  }
}
