import 'dart:convert';

import 'package:qn_read_rule/book_provide/base_cache.dart';
import 'package:qn_read_rule/book_provide/base_model.dart';
import 'package:qn_read_rule/book_provide/error_source.dart';
import 'package:qn_read_rule/book_provide/js_rule_source.dart';
import 'package:qn_read_rule/book_provide/str_rule_source.dart';
import 'package:qn_read_rule/utils/b_utils.dart';
import 'package:qn_read_rule/utils/logger.dart';

import 'base.dart';

class BookSourceManager {
  final Map<String, BSourceWrapper> _bookProvides;

  // 工厂模式6
  factory BookSourceManager() => _getInstance();

  static BookSourceManager get i => _getInstance();
  static BookSourceManager? _instance;

  BookSourceManager._internal() : _bookProvides = {};

  static BookSourceManager _getInstance() {
    _instance ??= BookSourceManager._internal();
    return _instance!;
  }

  void addBookSource(BSourceWrapper bookProvide) {
    _bookProvides[bookProvide.siteId] = bookProvide;
  }

  void removeBookSource(BookSourceProvide bookProvide) {
    _bookProvides.remove(bookProvide.siteId());
  }

  BSourceWrapper? queryBy({required String site}) {
    return _bookProvides[site];
  }

  Future<bool> verifyBookSource(String site) async {
    Logger.debug(() => '开始进行书源验证 -> $site');
    var provide = queryBy(site: site);
    if (provide == null) {
      return false;
    }
    var flag = await provide.verifyBookSource();
    return flag.success;
  }

  void clear() {
    _bookProvides.clear();
  }
}

class BSourceWrapper {
  late BookSourceProvide provide;

  late String sourceRule;

  BSourceWrapper.json({required String source}) {
    provide = StringBaseProvide(source: jsonDecode(source));
    sourceRule = source;
  }

  // BSourceWrapper.dart({required ByteData bytecode,required int siteId,required String className}){
  //   final runtime = Runtime(bytecode);
  //   runtime.addPlugin(QNEvalPlugin());
  //   runtime.setup();
  //   provide = runtime.executeLib(
  //       'package:dart_site_$siteId/${className.toLowerCase()}.dart', '$className.');
  //   sourceRule = '暂不支持Dart字节码的调试';
  // }

  BSourceWrapper.source({required String source}) {
    if (source.startsWith('class')) {
      provide = JSRuleSource.initWith(source);
    } else {
      provide = StringBaseProvide(source: jsonDecode(source));
    }
    sourceRule = source;
  }

  BSourceWrapper.error({required String source, required String site}) {
    provide = ErrorProvide(site);
    sourceRule = source;
  }

  BSourceWrapper(this.provide) : sourceRule = '暂不支持此类调试';

  String get sourceType {
    if (provide is StringBaseProvide) {
      return 'json';
    } else if (provide is JSRuleSource) {
      return 'js';
    } else {
      return 'dart';
    }
  }

  QNBaseModel get info => provide.info();

  int get version => provide.currentSiteVersion();

  bool get isEnable => provide.info().enable;

  bool get isEncrypt => provide.info().isEncrypt;

  bool get isBookType => type == 2;

  bool get isListenBookType => type == 3;

  bool get isComicType => type == 4;

  String get showName => provide.info().showName;

  String get comicShowStyle => provide.info().comicShowType;

  String get desc => provide.info().desc;

  bool get supportExplore => provide.info().supportExplore;

  bool get supportSearch => provide.info().supportSearchBookName;

  bool get needVpn => provide.info().vpnWebsite.trim().isNotEmpty;

  String get groupName => provide.info().group;

  int get updateTime => provide.info().updateTime;

  String get typeDesc {
    final sType = type;
    if (sType == 2) {
      return '书籍';
    } else if (sType == 3) {
      return '听书';
    } else if (sType == 4) {
      return '漫画';
    } else {
      return '未知';
    }
  }

  int? _type;

  int get type {
    if (_type != null) {
      return _type!;
    }

    var typeStr = provide.info().type.toLowerCase();
    switch (typeStr) {
      case 'book':
        _type = 2;
        return 2;
      case 'listenbook':
        _type = 3;
        return 3;
      case 'comic':
        _type = 4;
        return 4;
      default:
        _type = 2;
        return 2;
    }
  }

  Future<VerifyResult> verifyBookSource({bool verifyDisable = false}) async {
    if (provide.siteId() == 'unknown') {
      return const VerifyResult(
          success: true, type: '未知站点', errorMsg: '不存在此站点');
    }
    var verifyConf = provide.verifyBookConfig();
    var result = await verifyBookSourceWrapper(
        searchKey: verifyConf['searchKey'] ?? '',
        bookDetailPrefix: verifyConf['bookDetailPrefix'] ?? '',
        verifyDisable: verifyDisable);
    return result;
  }

  // page从1开始
  String _handlerExploreUrl(String url, int page) {
    GlobalPrev.currentPage = page;
    if (provide is StringBaseProvide) {
      return BNativeUtils.qn_querySingle(
          withRule: url,
          node: {'page': page.toString()},
          model: QNBookModel.empty,
          addParams: (provide as StringBaseProvide).userDefineMap);
    } else {
      return BNativeUtils.qn_querySingle(
          withRule: url,
          node: {'page': page.toString()},
          addParams: {},
          model: QNBookModel.empty);
    }
  }

  Future<VerifyResult> verifyBookSourceWrapper(
      {required String searchKey,
      required String bookDetailPrefix,
      bool verifyDisable = false}) async {
    if (bookDetailPrefix == '') {
      return const VerifyResult(
          success: false,
          type: 'verify_book',
          errorMsg: 'bookDetailPrefix必须有值');
    }
    if (!verifyDisable && provide.info().enable == false) {
      return const VerifyResult(
          success: true, type: 'verify_all', errorMsg: '验证成功');
    }

    QNBookModel model = QNBookModel.empty;
    if (provide.info().supportExplore) {
      var exploreModel = provide.exploreModel();
      final exploreUrl = exploreModel.boyExploreList.isEmpty
          ? ''
          : exploreModel.boyExploreList[0].url;
      var list = await queryExploreWrapper(exploreUrl, 0);
      if (list.isEmpty) {
        return VerifyResult(
            success: false,
            type: 'verify_explore',
            errorMsg:
                '验证发现失败\n发现页列表获取书书籍为空\n当前使用Tag->${exploreModel.boyExploreList.isEmpty ? '' : exploreModel.boyExploreList[0].tag}::$exploreUrl');
      }
      model = list[0];
      if (!model.detailUrl.startsWith(bookDetailPrefix)) {
        return VerifyResult(
            success: false,
            type: 'verify_explore',
            errorMsg:
                '验证发现失败\n发现获取的书籍详情页地址前缀不匹配\n书籍地址->${model.detailUrl}\n前缀->$bookDetailPrefix');
      }

      model = await queryBookDetailWrapper(model.bookId);

      if (!model.detailUrl.startsWith(bookDetailPrefix)) {
        return VerifyResult(
            success: false,
            type: 'verify_detail',
            errorMsg:
                '验证详情页失败\n发现获取的书籍详情页地址前缀不匹配\n书籍地址->${model.detailUrl}\n前缀->$bookDetailPrefix');
      }

      if ((!model.detailUrl.startsWith(bookDetailPrefix)) ||
          model.tocUrl.isEmpty) {
        return const VerifyResult(
            success: false,
            type: 'verify_detail',
            errorMsg: '验证详情页失败\n通过发现获取的详情页中的tocUrl为空');
      }

      var tocModel = await queryBookTocWrapper(model.bookId);
      if (tocModel.dataList.isEmpty) {
        return const VerifyResult(
            success: false,
            type: 'verify_toc',
            errorMsg: '验证目录页失败\n通过发现获取的目录页章节列表为空');
      }

      var tocInnerModel = tocModel.dataList[0];

      var contentModel =
          await queryBookContentWrapper(model.bookId, tocInnerModel);

      if (contentModel.bookContent.isNotEmpty ||
          contentModel.musicContent.url.isNotEmpty ||
          contentModel.comicPicList.isNotEmpty) {
      } else {
        return const VerifyResult(
            success: false,
            type: 'verify_content',
            errorMsg: '验证内容页失败\n通过发现获取的内容页数据为空');
      }
    }

    if (provide.info().supportSearchBookName) {
      var list = await searchBookNameWrapper(searchKey);
      if (list.isEmpty) {
        return VerifyResult(
            success: false,
            type: 'verify_search',
            errorMsg: '验证搜索失败\n通过搜索关键字[$searchKey]获取的列表数据为空');
      }

      bool hasMatchSearchKey = false;
      for (var m in list) {
        if (m.bookName == searchKey) {
          model = m;
          hasMatchSearchKey = true;
          break;
        }
      }

      if (!hasMatchSearchKey) {
        return VerifyResult(
            success: false,
            type: 'verify_search',
            errorMsg:
                '验证搜索失败\n通过搜索关键字[$searchKey]获取的书籍名称不匹配\n搜索词->${searchKey}');
      }

      if ((!model.detailUrl.startsWith(bookDetailPrefix))) {
        return VerifyResult(
            success: false,
            type: 'verify_search',
            errorMsg:
                '验证搜索失败\n通过搜索关键字[$searchKey]获取的书籍详情页地址前缀不匹配\n书籍地址->${model.detailUrl}\n前缀->$bookDetailPrefix');
      }

      model = await queryBookDetailWrapper(model.bookId);

      if (!model.detailUrl.startsWith(bookDetailPrefix)) {
        return VerifyResult(
            success: false,
            type: 'verify_detail',
            errorMsg:
                '验证详情页失败\n通过搜索关键字[$searchKey]获取的书籍详情页地址前缀不匹配\n书籍地址->${model.detailUrl}\n前缀->$bookDetailPrefix');
      }

      if (model.tocUrl.isEmpty) {
        return VerifyResult(
            success: false,
            type: 'verify_detail',
            errorMsg: '验证详情页失败\n通过搜索关键字[$searchKey]获取的书籍详情页的tocUrl为空');
      }

      var tocModel = await queryBookTocWrapper(model.bookId);
      if (tocModel.dataList.isEmpty) {
        return VerifyResult(
            success: false,
            type: 'verify_toc',
            errorMsg: '验证目录页失败\n通过搜索关键字[$searchKey]获取的目录章节列表为空');
      }

      var tocInnerModel = tocModel.dataList[0];

      var contentModel =
          await queryBookContentWrapper(model.bookId, tocInnerModel);

      if (contentModel.bookContent.isNotEmpty ||
          contentModel.musicContent.url.isNotEmpty ||
          contentModel.comicPicList.isNotEmpty) {
      } else {
        return VerifyResult(
            success: false,
            type: 'verify_content',
            errorMsg: '验证内容页失败\n通过搜索关键字[$searchKey]获取的内容页数据为空');
      }
    }
    return VerifyResult(
        success: true, type: 'verify_all', errorMsg: '$siteId  验证成功');
  }

  // -------------------------  分割线 以下为公用方法区  ---------------------

  Future<List<QNBookModel>> queryExploreWrapper(
      String exploreUrl, int page) async {
    try {
      final modelList =
          await provide.queryExplore(_handlerExploreUrl(exploreUrl, page));
      if (modelList.isEmpty) {
        return <QNBookModel>[];
      }
      BookSourceCache.i.addAll(modelList);
      return modelList;
    } catch (error) {
      Logger.error(error);
      return <QNBookModel>[];
    }
  }

  /// 验证 搜索书籍
  Future<List<QNBookModel>> searchBookNameWrapper(String key) async {
    try {
      var modelList = await provide.searchBookName(key);
      if (modelList.isEmpty) {
        return <QNBookModel>[];
      }
      BookSourceCache.i.addAll(modelList);
      return modelList;
    } catch (error, stack) {
      Logger.errorStack(error, stack);
      return <QNBookModel>[];
    }
  }

  /// 获取书籍详情
  /// 入参为 bookId 及 detailUrl
  Future<QNBookModel> queryBookDetailWrapper(int bookId) async {
    final model = BUtils.qn_QueryBookBy(bookId: bookId);
    if (model == null) {
      return QNBookModel.empty;
    }
    try {
      var qnBookModel = await provide.queryBookDetail(model);
      BookSourceCache.i.add(qnBookModel);
      return qnBookModel;
    } catch (error) {
      Logger.error(error);
      return QNBookModel.empty;
    }
  }

  /// 获取书籍目录
  /// 入参为 bookId 及 tocUrl
  Future<QNTocModel> queryBookTocWrapper(int bookId) async {
    final model = BUtils.qn_QueryBookBy(bookId: bookId);
    if (model == null) {
      return QNTocModel.empty;
    }
    try {
      var qnTocModel = await provide.queryBookToc(model);
      BookSourceCache.i.addToc(qnTocModel);
      return qnTocModel;
    } catch (error, stack) {
      Logger.errorStack(error, stack);
      return QNTocModel.empty;
    }
  }

  /// 获取书籍内容
  /// /// 入参为 bookId 及 contentUrl
  Future<QNContentModel> queryBookContentWrapper(
      int bookId, QNTocInnerModel innerModel) async {
    final model = BUtils.qn_QueryBookBy(bookId: bookId);
    if (model == null) {
      return QNContentModel.empty;
    }
    BookSourceCache.i.addInnerToc(innerModel);
    try {
      var qnContentModel =
          await provide.queryBookContent(model, innerModel.url);
      BookSourceCache.i.addContent(qnContentModel);
      return qnContentModel;
    } catch (error, stack) {
      Logger.errorStack(error, stack);
      return QNContentModel.empty;
    }
  }

  String get siteId => provide.siteId();

  QNExploreModel get exploreTags => provide.exploreModel();
}

class VerifyResult<T> {
  final bool success;
  final String type;
  final String? errorMsg;
  final int? costTime;
  final T? data;

  const VerifyResult(
      {required this.success,
      required this.type,
      required this.errorMsg,
      this.data,
      this.costTime});

  const VerifyResult.error(
      {required this.errorMsg, this.type = 'default', this.data, this.costTime})
      : success = false;

  const VerifyResult.ok(this.data,
      {this.type = 'default', this.errorMsg, this.costTime})
      : success = true;

  bool get isSuccess => success;

  @override
  String toString() {
    return 'VerifyResult{success: $success, type: $type, msg: $errorMsg}';
  }
}
