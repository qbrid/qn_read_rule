import 'dart:async';
import 'dart:convert';

import 'package:fast_gbk/fast_gbk.dart';
import 'package:html/dom.dart';
import 'package:html/parser.dart' as html_parse;
import 'package:json_path/json_path.dart';
import 'package:qn_read_rule/book_provide/base_cache.dart';
import 'package:qn_read_rule/book_provide/base_model.dart';
import 'package:qn_read_rule/utils/NeworkHelper.dart';
import 'package:qn_read_rule/utils/logger.dart';
import 'package:qn_read_rule/utils/qn_utils.dart';
import 'package:xpath_selector_html_parser/xpath_selector_html_parser.dart';

// part 'base.g.dart';

abstract class BookSourceProvide {
  BookSourceProvide();

  /// 站点ID,必须，唯一
  String siteId();

  /// 当前站点版本
  int currentSiteVersion();

  /// 站点更新信息。
  /// 格式为 版本号：更新信息
  Map<int, String> siteUpdateInfo();

  /// 站点ID,必须，基础信息
  QNBaseModel info();

  /// 探索模型
  /// 建议区分 男频 和 女频
  QNExploreModel exploreModel();

  /// 从 110版本，进行了网站地址变更，低于此版本的书籍数据均不可用，需要进行数据迁移
  ///
  /// 数据迁移由 App进行处理，站点规则 需确认是否进行旧数据处理及迁移。
  ///
  /// 如返回为 true，当用户 在书架打开书籍时，会 将旧书籍删除，使用新的规则 进行 搜索 -> 获取详情 -> 获取目录，重新生成数据数据。
  ///
  /// 返回值为  0：不需要进行数据迁移，适用于 逻辑优化、去广告等
  /// 返回值为  1：代表 书籍详情页地址发生变更，当用户 在书架打开书籍时，会 将旧书籍删除，使用新的规则 进行 搜索 -> 获取详情 -> 获取目录 -> 获取章节列表，重新生成完整的书籍数据。
  /// 返回值为  2：代表 书籍的目录集合页地址发生变更，当用户 在书架打开书籍时，会通过 新的规则 ->获取目录 -> 获取章节列表，重新生成书籍数据。
  /// 返回值为  3：代表 书籍的目录章节地址（正文地址）发生变更，当用户打开书籍时，会通过 新的规则 -> 获取章节列表，重新生成书籍数据。
  int migrate(int oldVersion);

  /// 根据书名搜索 书籍
  /// 入参为 key
  Future<List<QNBookModel>> queryExplore(String exploreUrl);

  /// 根据书名搜索 书籍
  /// 入参为 key
  Future<dynamic> debugConfig(Map<String, String> map) {
    return Future(() => null);
  }

  /// 根据书名搜索 书籍
  /// 入参为 key
  Future<List<QNBookModel>> searchBookName(String key);

  /// 获取书籍详情
  /// 入参为 bookId 及 detailUrl
  Future<QNBookModel> queryBookDetail(QNBookModel model);

  /// 获取书籍目录
  /// 入参为 bookId 及 tocUrl
  Future<QNTocModel> queryBookToc(QNBookModel model);

  /// 获取书籍内容
  /// /// 入参为 bookId 及 contentKey
  Future<QNContentModel> queryBookContent(QNBookModel model, String contentKey);

  Map<String, String> verifyBookConfig();
}

class BUtils {
  static QNBookModel? qn_QueryBookBy({required int bookId}) {
    return BookSourceCache.i.get(bookId);
  }

  static QNTocInnerModel qn_QueryInnerTocBy({required String contentKey}) {
    return BookSourceCache.i.getInnerToc(contentKey);
  }

  static Future<String> qn_Network(
    String url, {
    String method = 'get',
    Map<String, String> headers = const {},
    String requestData = '',
    bool isWebViewRequest = false,
    String responseCharset = '',
    int waitTime = 500,
    bool needCookies = false,
    int cacheTime = 0,
  }) async {
    var response = await NetworkHelper.excutorRequestWithResponse(url,
        method: method,
        headers: headers,
        requestData: requestData,
        responseCharset: responseCharset,
        isWebView: isWebViewRequest,
        waitTime: waitTime,
        needCookies: needCookies,
        cacheTime: cacheTime);
    // final Map<String, dynamic> resultData;
    final dynamic data;
    if (responseCharset == '' ||
        responseCharset == 'utf8' ||
        responseCharset == 'utf-8') {
      data = utf8.decode(response.data ?? [], allowMalformed: true);
    } else if (responseCharset == 'gbk') {
      data = gbk.decode(response.data ?? [], allowMalformed: true);
    } else {
      data = response.data;
    }
    return data;
  }

  /// 插入数据
  static void qn_Put(int bookId, String key, String value) {
    BookSourceCache.i.qnPut(bookId, key, value);
  }

  /// 获取数据
  static String qn_Get(int bookId, String key) {
    return BookSourceCache.i.qnGet(bookId, key) ?? '';
  }

  static List<String> qn_queryList(
      {required String withRule, required String node}) {
    if (withRule.startsWith('\$')) {
      return _qn_JsonPathList(withRule, node);
    } else if (withRule.startsWith('/')) {
      return _qn_XpathList(withRule, node);
    } else {
      throw UnimplementedError('未实现的规则');
    }
  }

  static List<String> _qn_XpathList(String rule, String node) {
    final HtmlXPath htmlXPath = HtmlXPath.html(node);
    var queryXPath = htmlXPath.queryXPath(rule);
    return queryXPath.nodes.map((e) {
      var n = e.node;
      if (n is Element) {
        return n.outerHtml;
      } else {
        return '';
      }
    }).toList(growable: false);
  }

  static String _qn_XpathSingle(String rule, String node, {String join = ''}) {
    if (node.startsWith("<tr")) {
      node = '<table>$node</table>';
    }
    final HtmlXPath htmlXPath = HtmlXPath.node(html_parse.parseFragment(node));
    // parse(node);
    var queryXPath = htmlXPath.queryXPath(rule);
    if (queryXPath.nodes.length > 1) {
      return queryXPath.attrs.join(join);
    } else {
      return queryXPath.attr ?? '';
    }
  }

  static List<String> _qn_JsonPathList(String rule, String jsonMap) {
    if (!jsonMap.startsWith('{')) {
      return <String>[];
    }
    var decodeMap = json.decode(jsonMap);
    var values = JsonPath(rule).readValues(decodeMap);
    return values.map((e) {
      if (e == null) {
        return "{}";
      } else {
        return json.encode(e);
      }
    }).toList(growable: false);
  }

  static String _qn_JsonPathSingle(String rule, String jsonMap,
      {String join = ""}) {
    return JsonPath(rule).readValues(jsonDecode(jsonMap)).join(join);
  }

  /// 常用工具类
  ///
  /// 提供 md5 等等。
  static dynamic qn_UtilsSync(String method, List<dynamic> args) {
    return QNUtilsProxy.handlerSync(method, args);
  }

  static Future<dynamic> qn_UtilsAsync(String method, List<dynamic> args) {
    return QNUtilsProxy.handlerASync(method, args);
  }

  static void qn_LogDebug(String input, {String tag = ''}) {
    Logger.debug(() => input, tag: tag);
  }

  static int roundToInt(double d) {
    return d.round();
  }
}
