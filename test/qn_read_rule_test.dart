
import 'package:flutter_test/flutter_test.dart';
import 'package:qn_read_rule/book_provide/base.dart';
import 'package:qn_read_rule/book_provide/base_model.dart';
import 'package:qn_read_rule/book_provide/book_source_manager.dart';
import 'package:qn_read_rule/utils/NeworkHelper.dart';
import 'package:qn_read_rule/utils/b_utils.dart';
import 'package:qn_read_rule/utils/js_utils.dart';
import 'package:qn_read_rule/utils/logger.dart';

const htmlStr2 = """
<!doctype html>
<html>
<head>
<title>万古神帝无弹窗_万古神帝最新章节列表_万古神帝5200_笔趣阁</title>
</head>
<body>
<div id="wrapper">
<div class="box_con">
		<div style="width:910px;margin:0px auto;" id="cstad"><table id="cstad"><tr>
			<td id="adt1"><script type="text/javascript">cad1();</script></td>
			<td id="adt2"><script type="text/javascript">cad2();</script></td>
			<td id="adt3"><script type="text/javascript">cad3();</script></td>
			</tr></table>
		</div>
        <script>dingbu();</script>    
		<div class="box_con">
			<div id="list">
				<dl>
                        <dd kk='1' ><a href="/8_8187/3899817.html">1.第1章 八百年后</a></dd>
                        <dd kk='2'><a href="/8_8187/3899818.html">2.第2章 开启神武印记</a></dd>
                        <dd kk='3'><a href="/8_8187/3899819.html">3.第3章 黄极境</a></dd>
                        <dd kk='4'><a href="/8_8187/3899820.html">4.第4章 时空秘典</a></dd>
                        <dd kk='5'><a href="/8_8187/3899821.html">5.第5章 龙象般若</a></dd>
                        <dd kk='6'><a href="/8_8187/3899822.html">6.第6章 林泞姗</a></dd>
                     
				</dl>
			</div>
		</div>
	</div>
</body></html>
""";


void main() {


  test('xpath', () {

    // const htmel = """
    // <tr xx='1'>
    //   <td><a href="/book/220/" title="坤宁（宁安如梦原著小说）" target="_blank" class="orange">《坤宁（宁安如梦原著小说）》</a></td>
    //   <td>时镜</td>
    //   <td><a href="/txt/220.html" target="_blank">TXT下载</a></td>
    // </tr>
    // """;

    // {
    //   var list = BNativeUtils.qn_queryList(withRule: '//div[@id="list"]//dd', node: htmlStr2, model: QNBookModel.empty);
    //
    //   for(var item in list){
    //     final name = BNativeUtils.qn_querySingle(withRule: '/a/text()', node: item, model: QNBookModel.empty);
    //     final href = BNativeUtils.qn_querySingle(withRule: 'css: a@href', node: item, model: QNBookModel.empty);
    //     final kk = BNativeUtils.qn_querySingle(withRule: '/@kk', node: item, model: QNBookModel.empty);
    //     print("kk ->$kk   name -> $name  href --> $href \n");
    //   }
    // }
    //
    // print('----------------');

    JSUtils.init(needLog: true);
    {
      var list = BNativeUtils.qn_queryList(withRule: 'css: #list > dl > dd ', node: htmlStr2, model: QNBookModel.empty);

      for(var item in list){
        // final name = BNativeUtils.qn_querySingle(withRule: "css: a@href,,,@js:return result + '124';,,,{{preV}}{{/a/text()}}", node: item, model: QNBookModel.empty);
        // final href = BNativeUtils.qn_querySingle(withRule: 'css: a@href', node: item, model: QNBookModel.empty);
        // final kk = BNativeUtils.qn_querySingle(withRule: '/@kk', node: item, model: QNBookModel.empty);
        // print("kk ->$kk   name -> $name  href --> $href \n");
      }
    }


    // final doc2 = html_parser.parseFragment(htmel,container: 'tr');
    // print(doc2.outerHtml);
    //
    // final htmlXPath = HtmlXPath.node(doc2);
    // htmlXPath.root.node.attributes;
    // final text = htmlXPath.query("//tr/td[1]/a/text()");
    // print('text --> ${text.attr}');
    // final doc = html_parser.parse(htmlStr2);
    // final htmlXPath = HtmlXPath.node(doc);
    // const rule = "//div[@id='list']//dd[position() < last() -2]";
    //
    //
    // final list = htmlXPath.query(rule);
    // final nodeList = list.nodes;
    // print('length -> ${nodeList.length} -> ${(nodeList[0].node as Element).outerHtml}');
    // int a = 1;
  });


  test('css', () {

    // final doc = html_parser.parse(htmlStr);
    // var querySelector = doc.querySelectorAll('#list > dl > dd');
    // _qn_CssSingle()
    // var list = BNativeUtils.qn_CSSList('css: #list > dl > dd',htmlStr);
    // for(var s in list){
    //   var href = BNativeUtils.qn_CssSingle('css: a@text', s);
    //   print('href --> $href');
    //   // Logger.debug(() => h)
    // }
    // querySelector[0].q

    int a =1;
  });


}



Future<void> _testStrNative(String strRule) async {
  Logger.addLog(CLog());
  NetworkHelper.init(true,true);
  final sourceWrapper = BSourceWrapper.source(source: strRule.trim(),);
  await sourceWrapper.verifyBookSource();
}

Future<void> _testNative(BookSourceProvide bookSourceProvide) async {
  var sourceWrapper = BSourceWrapper(bookSourceProvide);
  await sourceWrapper.verifyBookSource();
}

Future<void> _testEval(String bookSourceCode) async {
  Logger.addLog(CLog());
  NetworkHelper.init(true,true);
  final sourceWrapper = BSourceWrapper.source(source: bookSourceCode.trim(),);
  var startTime = DateTime.now().millisecondsSinceEpoch;
  var verifyResult = await sourceWrapper.verifyBookSource();
  var endTime = DateTime.now().millisecondsSinceEpoch;
  Logger.debug(() => '${endTime - startTime}ms');
  int a = 1;
}

