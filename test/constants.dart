


const String example_1 = r"""
import 'dart:async';
import 'dart:convert';

import 'package:qn_read_rule/book_provide/base.dart';
import 'package:qn_read_rule/book_provide/base_model.dart';

class BookSource_MaoEr extends BookSourceProvide {
  @override
  String siteId() => "https://www.missevan.com#乃星";

  @override
  int currentSiteVersion() => 120;

  @override
  Map<int, String> siteUpdateInfo() => {
        80: '初始化版本',
        100: '修改正文正则',
        110: '网站变更，旧数据无法兼容，需进行迁移',
        currentSiteVersion(): '站点信息无变更，去除正文内容的广告'
      };

  @override
  int migrate(int oldVersion) {
    /// 从 110版本，进行了网站地址变更，低于此版本的书籍数据均不可用，需要进行数据迁移
    ///
    /// 数据迁移由 App进行处理，站点规则 需确认是否进行旧数据处理及迁移。
    ///
    ///
    /// 返回值为  0：不需要进行数据迁移，适用于 逻辑优化、去广告等
    /// 返回值为  1：代表 书籍详情页地址发生变更，当用户 在书架打开书籍时，会 将旧书籍删除，使用新的规则 进行 搜索 -> 获取详情 -> 获取目录 -> 获取章节列表，重新生成完整的书籍数据。
    /// 返回值为  2：代表 书籍的目录集合页地址发生变更，当用户 在书架打开书籍时，会通过 新的规则 ->获取目录 -> 获取章节列表，重新生成书籍数据。
    /// 返回值为  3：代表 书籍的目录章节地址（正文地址）发生变更，当用户打开书籍时，会通过 新的规则 -> 获取章节列表，重新生成书籍数据。

    return 0;
  }

  @override
  QNBaseModel info() => QNBaseModel.init(

      /// 站点ID ，一般为站点网站
      siteId: siteId(),

      /// 类型： book 书籍  listenbook听书  comic漫画
      type: 'listenbook',

      /// 站点显示名称
      showName: '猫耳',

      /// 站点分类
      group: '搜狗',

      /// 站点描述
      desc: '猫耳API接口',

      /// 漫画详情章节展示，仅漫画有效，分为 small 小，middle 中等  big大
      comicShowType: 'small',

      enable: true,

      /// 站点更新时间戳
      updateTime: DateTime.now().millisecondsSinceEpoch,

      /// 站点版本
      versionNumb: currentSiteVersion(),

      /// 是否支持 探索
      supportExplore: false,
      vpnWebsite: '',

      /// 是否支持 书名搜索
      supportSearchBookName: true,
      isEncrypt: false,
  );

  @override

  /// 探索 模型 `
  QNExploreModel exploreModel() {
    return QNExploreModel.empty;
  }

  @override
  Future<List<QNBookModel>> queryExplore(String exploreUrl) async {
    return <QNBookModel>[];
  }

  @override
  Future<List<QNBookModel>> searchBookName(String key) async {
    if (key.length < 2) {
      /// 此站点不支持 关键词长度小于3的 书名搜索
      return [];
    }
    var requestPath = 'https://www.missevan.com/dramaapi/search?s=$key&page=1';
    // 获取网络数据
    var result = await BUtils.qn_Network(requestPath, method: "GET", headers: {
      'User-Agent':
          'Mozilla/5.0 (Linux; Android 10; MI 8 Build/QKQ1.190828.002; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/83.0.4103.101 Mobile Safari/537.36'
    });
    // json字符串转换为 Map对象
    final dataList =
        BUtils.qn_queryList(withRule: r'$..Datas[*]', node: result);
    final List<QNBookModel> searchList = [];
    // 便利数组
    for (var d in dataList) {
      var data = json.decode(d.toString());
      final id = data['id'];
      final String detailUrl =
          'https://www.missevan.com/dramaapi/getdrama?drama_id=$id';
      String author = data['author'] == null ? '' : data['author'];
      var bookName = '${data['name']}';
      {
        var indexOf = bookName.indexOf("丨");
        if (indexOf > 0) {
          bookName = bookName.substring(0, indexOf);
        }
      }
      {
        var indexOf = bookName.indexOf("|");
        if (indexOf > 0) {
          bookName = bookName.substring(0, indexOf);
        }
      }
      final _model = QNBookModel.init(
          siteId: siteId(),
          siteVersion: currentSiteVersion(),
          bookName: bookName,
          encode: '',
          detailUrl: detailUrl,
          author: author,
          cover: data['cover'],
          newestChapter: data['newest']);
      searchList.add(_model);
    }
    return searchList;
  }

  @override
  Future<QNBookModel> queryBookDetail(QNBookModel model) async {
    // 根据 bookID 获取
    var detailUrl = model.detailUrl;
    var networkData = await BUtils.qn_Network(detailUrl);
    var kind =
        BUtils.qn_querySingle(withRule: r'$..catalog_name', node: networkData);
    var desc =
        BUtils.qn_querySingle(withRule: r'$..abstract', node: networkData);
    return QNBookModel.copyWith(
      model,
      newTags: [kind],
      newDesc: desc,
      newTocurl: detailUrl,
    );
  }

  @override
  Future<QNTocModel> queryBookToc(QNBookModel model) async {
    final tocUrl = model.tocUrl;
    if (tocUrl.isEmpty) {
      return QNTocModel.empty;
    }
    var networkData = await BUtils.qn_Network(tocUrl, method: 'GET');
    List<String> dataList =
        BUtils.qn_queryList(withRule: r'$..episode[*]', node: networkData);
    final List<QNTocInnerModel> innerList = [];
    for (String d in dataList) {
      var data = json.decode(d);
      final url =
          'https://www.missevan.com/sound/getsound?soundid=${data['sound_id']}';
      innerList.add(
          QNTocInnerModel.init(title: data['name'], url: url, needVip: false));
    }
    return QNTocModel.init(
        bookId: model.bookId,
        updateTime: DateTime.now().millisecondsSinceEpoch,
        dataList: innerList);
  }

  @override
  Future<QNContentModel> queryBookContent(
      QNBookModel model, String contentKey) async {
    var networkData = await BUtils.qn_Network(contentKey, headers: {
      'User-Agent':
          'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
      'Host': 'www.missevan.com'
    });
    var url =
        BUtils.qn_querySingle(withRule: r'$..soundurl', node: networkData);
    return QNContentModel.init(
        contentKey: contentKey, musicContent: MusicContentModel.init(url: url));
  }

  /// --------------------  以下为 站点校验  -------------------------
  ///
  ///
  @override
  Map<String, String> verifyBookConfig() => {
        "searchKey": "犯罪现场",
        "bookDetailPrefix": 'https://www.missevan.com',
      };
}

""";

const String example_2 = r"""
import 'dart:async';
import 'dart:convert';

import 'package:qn_read_rule/book_provide/base.dart';
import 'package:qn_read_rule/book_provide/base_model.dart';

class BookSource_9x extends BookSourceProvide {
  @override
  String siteId() => "https://novel-api.elklk.cn";

  @override
  int currentSiteVersion() => 120;

  @override
  Map<int, String> siteUpdateInfo() => {
        80: '初始化版本',
        100: '修改正文正则',
        110: '网站变更，旧数据无法兼容，需进行迁移',
        currentSiteVersion(): '站点信息无变更，去除正文内容的广告'
      };

  @override
  int migrate(int oldVersion) {
    /// 从 110版本，进行了网站地址变更，低于此版本的书籍数据均不可用，需要进行数据迁移
    ///
    /// 数据迁移由 App进行处理，站点规则 需确认是否进行旧数据处理及迁移。
    ///
    ///
    /// 返回值为  0：不需要进行数据迁移，适用于 逻辑优化、去广告等
    /// 返回值为  1：代表 书籍详情页地址发生变更，当用户 在书架打开书籍时，会 将旧书籍删除，使用新的规则 进行 搜索 -> 获取详情 -> 获取目录 -> 获取章节列表，重新生成完整的书籍数据。
    /// 返回值为  2：代表 书籍的目录集合页地址发生变更，当用户 在书架打开书籍时，会通过 新的规则 ->获取目录 -> 获取章节列表，重新生成书籍数据。
    /// 返回值为  3：代表 书籍的目录章节地址（正文地址）发生变更，当用户打开书籍时，会通过 新的规则 -> 获取章节列表，重新生成书籍数据。

    return 0;
  }

  @override
  QNBaseModel info() => QNBaseModel.init(

      /// 站点ID ，一般为站点网站
      siteId: siteId(),

      /// 类型： book 书籍  listenbook听书  comic漫画
      type: 'book',

      /// 站点显示名称
      showName: '⚡ 新9x小说',

      /// 站点分类
      group: '搜狗',

      /// 站点描述
      desc: '新9x小说',

      /// 漫画详情章节展示，仅漫画有效，分为 small 小，middle 中等  big大
      comicShowType: 'small',

      /// 站点作者
      enable: true,
      vpnWebsite: '',

      /// 站点更新时间戳
      updateTime: DateTime.now().millisecondsSinceEpoch,

      /// 站点版本
      versionNumb: currentSiteVersion(),

      /// 是否支持 探索
      supportExplore: true,

      /// 是否支持 书名搜索
      supportSearchBookName: true,
      isEncrypt: false,
  );

  @override

  /// 探索 模型 `
  QNExploreModel exploreModel() {
    const tag =
        "玄幻-最热::https://novel-api.elklk.cn/cdn/category/rankList/1/1/auire/all/{{add(page,1)}}.html\n玄幻-最新::https://novel-api.elklk.cn/cdn/category/rankList/1/1/xinshu/all/{{add(page,1)}}.html\n玄幻-评分::https://novel-api.elklk.cn/cdn/category/rankList/1/1/pingfen/all/{{add(page,1)}}.html\n玄幻-完结::https://novel-api.elklk.cn/cdn/category/rankList/1/1/wanjie/all/{{add(page,1)}}.html\n武侠-最热::https://novel-api.elklk.cn/cdn/category/rankList/1/2/auire/all/{{add(page,1)}}.html\n武侠-最新::https://novel-api.elklk.cn/cdn/category/rankList/1/2/xinshu/all/{{add(page,1)}}.html\n武侠-评分::https://novel-api.elklk.cn/cdn/category/rankList/1/2/pingfen/all/{{add(page,1)}}.html\n武侠-完结::https://novel-api.elklk.cn/cdn/category/rankList/1/2/wanjie/all/{{add(page,1)}}.html\n都市-最热::https://novel-api.elklk.cn/cdn/category/rankList/1/3/auire/all/{{add(page,1)}}.html\n都市-最新::https://novel-api.elklk.cn/cdn/category/rankList/1/3/xinshu/all/{{add(page,1)}}.html\n都市-评分::https://novel-api.elklk.cn/cdn/category/rankList/1/3/pingfen/all/{{add(page,1)}}.html\n都市-完结::https://novel-api.elklk.cn/cdn/category/rankList/1/3/wanjie/all/{{add(page,1)}}.html\n历史-最热::https://novel-api.elklk.cn/cdn/category/rankList/1/4/auire/all/{{add(page,1)}}.html\n历史-最新::https://novel-api.elklk.cn/cdn/category/rankList/1/4/xinshu/all/{{add(page,1)}}.html\n历史-评分::https://novel-api.elklk.cn/cdn/category/rankList/1/4/pingfen/all/{{add(page,1)}}.html\n历史-完结::https://novel-api.elklk.cn/cdn/category/rankList/1/4/wanjie/all/{{add(page,1)}}.html\n科幻-最热::https://novel-api.elklk.cn/cdn/category/rankList/1/5/auire/all/{{add(page,1)}}.html\n科幻-最新::https://novel-api.elklk.cn/cdn/category/rankList/1/5/xinshu/all/{{add(page,1)}}.html\n科幻-评分::https://novel-api.elklk.cn/cdn/category/rankList/1/5/pingfen/all/{{add(page,1)}}.html\n科幻-完结::https://novel-api.elklk.cn/cdn/category/rankList/1/5/wanjie/all/{{add(page,1)}}.html\n网游-最热::https://novel-api.elklk.cn/cdn/category/rankList/1/6/auire/all/{{add(page,1)}}.html\n网游-最新::https://novel-api.elklk.cn/cdn/category/rankList/1/6/xinshu/all/{{add(page,1)}}.html\n网游-评分::https://novel-api.elklk.cn/cdn/category/rankList/1/6/pingfen/all/{{add(page,1)}}.html\n网游-完结::https://novel-api.elklk.cn/cdn/category/rankList/1/6/wanjie/all/{{add(page,1)}}.html\n女生-最热::https://novel-api.elklk.cn/cdn/category/rankList/1/7/auire/all/{{add(page,1)}}.html\n女生-最新::https://novel-api.elklk.cn/cdn/category/rankList/1/7/xinshu/all/{{add(page,1)}}.html\n女生-评分::https://novel-api.elklk.cn/cdn/category/rankList/1/7/pingfen/all/{{add(page,1)}}.html\n女生-完结::https://novel-api.elklk.cn/cdn/category/rankList/1/7/wanjie/all/{{add(page,1)}}.html\n同人-最热::https://novel-api.elklk.cn/cdn/category/rankList/1/66/auire/all/{{add(page,1)}}.html\n同人-最新::https://novel-api.elklk.cn/cdn/category/rankList/1/66/xinshu/all/{{add(page,1)}}.html\n同人-评分::https://novel-api.elklk.cn/cdn/category/rankList/1/66/pingfen/all/{{add(page,1)}}.html\n同人-完结::https://novel-api.elklk.cn/cdn/category/rankList/1/66/wanjie/all/{{add(page,1)}}.html\n玄幻-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/100/zuire/{{add(page,1)}}.html\n玄幻-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/100/xinshu/{{add(page,1)}}.html\n玄幻-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/100/tuijian/{{add(page,1)}}.html\n玄幻-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/100/wanjie/{{add(page,1)}}.html\n武侠-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/101/zuire/{{add(page,1)}}.html\n武侠-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/101/xinshu/{{add(page,1)}}.html\n武侠-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/101/tuijian/{{add(page,1)}}.html\n武侠-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/101/wanjie/{{add(page,1)}}.html\n游戏动漫-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/102/zuire/{{add(page,1)}}.html\n游戏动漫-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/102/xinshu/{{add(page,1)}}.html\n游戏动漫-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/102/tuijian/{{add(page,1)}}.html\n游戏动漫-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/102/wanjie/{{add(page,1)}}.html\n悬疑-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/104/zuire/{{add(page,1)}}.html\n悬疑-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/104/xinshu/{{add(page,1)}}.html\n悬疑-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/104/tuijian/{{add(page,1)}}.html\n悬疑-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/104/wanjie/{{add(page,1)}}.html\n影视小说-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/105/zuire/{{add(page,1)}}.html\n影视小说-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/105/xinshu/{{add(page,1)}}.html\n影视小说-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/105/tuijian/{{add(page,1)}}.html\n影视小说-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/105/wanjie/{{add(page,1)}}.html\n兵王-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/106/zuire/{{add(page,1)}}.html\n兵王-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/106/xinshu/{{add(page,1)}}.html\n兵王-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/106/tuijian/{{add(page,1)}}.html\n兵王-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/106/wanjie/{{add(page,1)}}.html\n腹黑-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/107/zuire/{{add(page,1)}}.html\n腹黑-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/107/xinshu/{{add(page,1)}}.html\n腹黑-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/107/tuijian/{{add(page,1)}}.html\n腹黑-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/107/wanjie/{{add(page,1)}}.html\n系统-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/110/zuire/{{add(page,1)}}.html\n系统-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/110/xinshu/{{add(page,1)}}.html\n系统-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/110/tuijian/{{add(page,1)}}.html\n系统-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/110/wanjie/{{add(page,1)}}.html\n穿越-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/111/zuire/{{add(page,1)}}.html\n穿越-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/111/xinshu/{{add(page,1)}}.html\n穿越-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/111/tuijian/{{add(page,1)}}.html\n穿越-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/111/wanjie/{{add(page,1)}}.html\n神豪-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/112/zuire/{{add(page,1)}}.html\n神豪-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/112/xinshu/{{add(page,1)}}.html\n神豪-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/112/tuijian/{{add(page,1)}}.html\n神豪-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/112/wanjie/{{add(page,1)}}.html\n推理-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/113/zuire/{{add(page,1)}}.html\n推理-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/113/xinshu/{{add(page,1)}}.html\n推理-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/113/tuijian/{{add(page,1)}}.html\n推理-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/113/wanjie/{{add(page,1)}}.html\n星际-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/114/zuire/{{add(page,1)}}.html\n星际-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/114/xinshu/{{add(page,1)}}.html\n星际-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/114/tuijian/{{add(page,1)}}.html\n星际-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/114/wanjie/{{add(page,1)}}.html\n灵异-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/115/zuire/{{add(page,1)}}.html\n灵异-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/115/xinshu/{{add(page,1)}}.html\n灵异-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/115/tuijian/{{add(page,1)}}.html\n灵异-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/115/wanjie/{{add(page,1)}}.html\n灵异-❀男生榜单❀::https://novel-api.elklk.cn/cdn/library/channelRankList/3/hot/{{add(page,1)}}.html\n灵异-最热总榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/zuire/all/{{add(page,1)}}.html\n灵异-最热月榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/zuire/month/{{add(page,1)}}.html\n灵异-最热周榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/zuire/week/{{add(page,1)}}.html\n灵异-完结总榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/wanjie/all/{{add(page,1)}}.html\n灵异-完结月榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/wanjie/month/{{add(page,1)}}.html\n灵异-完结周榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/wanjie/week/{{add(page,1)}}.html\n灵异-推荐总榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/tuijian/all/{{add(page,1)}}.html\n灵异-推荐月榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/tuijian/month/{{add(page,1)}}.html\n灵异-推荐周榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/tuijian/week/{{add(page,1)}}.html\n灵异-新书总榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/xinshu/all/{{add(page,1)}}.html\n灵异-新书月榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/xinshu/month/{{add(page,1)}}.html\n灵异-新书周榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/xinshu/week/{{add(page,1)}}.html\n灵异-评分总榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/pingfen/all/{{add(page,1)}}.html\n灵异-评分月榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/pingfen/month/{{add(page,1)}}.html\n灵异-评分周榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/pingfen/week/{{add(page,1)}}.html\n灵异-收藏总榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/shoucang/all/{{add(page,1)}}.html\n灵异-收藏月榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/shoucang/month/{{add(page,1)}}.html\n灵异-收藏周榜::https://novel-api.elklk.cn/cdn/category/rankList/1/0/shoucang/week/{{add(page,1)}}.html\n现代言情-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/124/zuire/{{add(page,1)}}.html\n现代言情-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/124/xinshu/{{add(page,1)}}.html\n现代言情-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/124/tuijian/{{add(page,1)}}.html\n现代言情-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/124/wanjie/{{add(page,1)}}.html\n古代言情-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/125/zuire/{{add(page,1)}}.html\n古代言情-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/125/xinshu/{{add(page,1)}}.html\n古代言情-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/125/tuijian/{{add(page,1)}}.html\n古代言情-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/125/wanjie/{{add(page,1)}}.html\n幻想言情-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/127/zuire/{{add(page,1)}}.html\n幻想言情-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/127/xinshu/{{add(page,1)}}.html\n幻想言情-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/127/tuijian/{{add(page,1)}}.html\n幻想言情-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/127/wanjie/{{add(page,1)}}.html\n科幻-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/128/zuire/{{add(page,1)}}.html\n科幻-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/128/xinshu/{{add(page,1)}}.html\n科幻-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/128/tuijian/{{add(page,1)}}.html\n科幻-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/128/wanjie/{{add(page,1)}}.html\n王妃-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/130/zuire/{{add(page,1)}}.html\n王妃-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/130/xinshu/{{add(page,1)}}.html\n王妃-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/130/tuijian/{{add(page,1)}}.html\n王妃-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/130/wanjie/{{add(page,1)}}.html\n豪门总裁-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/131/zuire/{{add(page,1)}}.html\n豪门总裁-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/131/xinshu/{{add(page,1)}}.html\n豪门总裁-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/131/tuijian/{{add(page,1)}}.html\n豪门总裁-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/131/wanjie/{{add(page,1)}}.html\n皇后-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/132/zuire/{{add(page,1)}}.html\n皇后-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/132/xinshu/{{add(page,1)}}.html\n皇后-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/132/tuijian/{{add(page,1)}}.html\n皇后-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/132/wanjie/{{add(page,1)}}.html\n萌宝-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/133/zuire/{{add(page,1)}}.html\n萌宝-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/133/xinshu/{{add(page,1)}}.html\n萌宝-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/133/tuijian/{{add(page,1)}}.html\n萌宝-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/133/wanjie/{{add(page,1)}}.html\n公主-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/134/zuire/{{add(page,1)}}.html\n公主-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/134/xinshu/{{add(page,1)}}.html\n公主-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/134/tuijian/{{add(page,1)}}.html\n公主-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/134/wanjie/{{add(page,1)}}.html\n腹黑-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/135/zuire/{{add(page,1)}}.html\n腹黑-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/135/xinshu/{{add(page,1)}}.html\n腹黑-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/135/tuijian/{{add(page,1)}}.html\n腹黑-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/135/wanjie/{{add(page,1)}}.html\n宠妻-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/136/zuire/{{add(page,1)}}.html\n宠妻-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/136/xinshu/{{add(page,1)}}.html\n宠妻-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/136/tuijian/{{add(page,1)}}.html\n宠妻-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/136/wanjie/{{add(page,1)}}.html\n嫡女-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/137/zuire/{{add(page,1)}}.html\n嫡女-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/137/xinshu/{{add(page,1)}}.html\n嫡女-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/137/tuijian/{{add(page,1)}}.html\n嫡女-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/137/wanjie/{{add(page,1)}}.html\n种田-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/138/zuire/{{add(page,1)}}.html\n种田-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/138/xinshu/{{add(page,1)}}.html\n种田-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/138/tuijian/{{add(page,1)}}.html\n种田-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/138/wanjie/{{add(page,1)}}.html\n甜宠-最热::https://novel-api.elklk.cn/cdn/category/tagsRankList/140/zuire/{{add(page,1)}}.html\n甜宠-最新::https://novel-api.elklk.cn/cdn/category/tagsRankList/140/xinshu/{{add(page,1)}}.html\n甜宠-评分::https://novel-api.elklk.cn/cdn/category/tagsRankList/140/tuijian/{{add(page,1)}}.html\n甜宠-完结::https://novel-api.elklk.cn/cdn/category/tagsRankList/140/wanjie/{{add(page,1)}}.html\n❀女生榜单❀-最热总榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/zuire/all/{{add(page,1)}}.html\n❀女生榜单❀-最热月榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/zuire/month/{{add(page,1)}}.html\n❀女生榜单❀-最热周榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/zuire/week/{{add(page,1)}}.html\n❀女生榜单❀-完结总榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/wanjie/all/{{add(page,1)}}.html\n❀女生榜单❀-完结月榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/wanjie/month/{{add(page,1)}}.html\n❀女生榜单❀-完结周榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/wanjie/week/{{add(page,1)}}.html\n❀女生榜单❀-推荐总榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/tuijian/all/{{add(page,1)}}.html\n❀女生榜单❀-推荐月榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/tuijian/month/{{add(page,1)}}.html\n❀女生榜单❀-推荐周榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/tuijian/week/{{add(page,1)}}.html\n❀女生榜单❀-新书总榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/xinshu/all/{{add(page,1)}}.html\n❀女生榜单❀-新书月榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/xinshu/month/{{add(page,1)}}.html\n❀女生榜单❀-新书周榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/xinshu/week/{{add(page,1)}}.html\n❀女生榜单❀-评分总榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/pingfen/all/{{add(page,1)}}.html\n❀女生榜单❀-评分月榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/pingfen/month/{{add(page,1)}}.html\n❀女生榜单❀-评分周榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/pingfen/week/{{add(page,1)}}.html\n❀女生榜单❀-收藏总榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/shoucang/all/{{add(page,1)}}.html\n❀女生榜单❀-收藏月榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/shoucang/month/{{add(page,1)}}.html\n❀女生榜单❀-收藏周榜::https://novel-api.elklk.cn/cdn/category/rankList/2/0/shoucang/week/{{add(page,1)}}.html";

    final List<QNExploreItem> boyExploreList = [];
    final List<QNExploreItem> girlExploreList = [];
    var split = tag.split('\n');
    int length = split.length;
    for (int i = 0; i < length; i++) {
      String s = split[i];
      var _array = s.split("::");
      final String tag = _array[0];
      final String url = _array[1];
      if (tag.startsWith('女')) {
        girlExploreList.add(QNExploreItem(tag: tag, url: url));
      } else {
        boyExploreList.add(QNExploreItem(tag: tag, url: url));
      }
    }
    return QNExploreModel.init(
        boyExploreList: boyExploreList, girlExploreList: girlExploreList);
  }

  @override
  Future<List<QNBookModel>> queryExplore(String exploreUrl) async {
    var networkData = await BUtils.qn_Network(exploreUrl);
    final list =
        BUtils.qn_queryList(withRule: r'$..list[*]', node: networkData);
    final List<QNBookModel> bookList = [];
    for (var s in list) {
      var map = json.decode(s);
      QNBookModel bookModel = QNBookModel.init(
          siteId: siteId(),
          bookName: map['name'],
          siteVersion: currentSiteVersion(),
          encode: '',
          author: map['author'],
          tags: [
            "${map['category']}",
          ],
          desc: map['description'],
          cover: map['icon'],
          detailUrl:
              'https://novel-api.elklk.cn/api/book-info?id=${map['id']}&source_id=1');
      bookList.add(bookModel);
    }
    return bookList;
  }

  @override
  Future<List<QNBookModel>> searchBookName(String key) async {
    var requestPath =
        'https://novel-api.elklk.cn/api/category-search?name=$key';
    // 获取网络数据
    var result = await BUtils.qn_Network(
      requestPath,
    );
    // json字符串转换为 Map对象
    final list = BUtils.qn_queryList(withRule: r'$..list[*]', node: result);
    final List<QNBookModel> bookList = [];
    for (var s in list) {
      var map = json.decode(s);
      var bId = '${map['id']}';
      QNBookModel bookModel = QNBookModel.init(
          siteId: siteId(),
          bookName: map['name'],
          siteVersion: currentSiteVersion(),
          encode: '',
          author: map['author'],
          tags: [
            "${map['category']}",
          ],
          desc: map['description'],
          cover: map['icon'],
          detailUrl:
              'https://novel-api.elklk.cn/api/book-info?id=$bId&source_id=1');
      bookList.add(bookModel);
      BUtils.qn_Put(bookModel.bookId, 'bId', bId);
    }
    return bookList;
  }

  @override
  Future<QNBookModel> queryBookDetail(QNBookModel model) async {
    // 根据 bookID 获取
    var bId = BUtils.qn_Get(model.bookId, 'bId');
    return QNBookModel.copyWith(model,
        newTocurl: 'https://novel-api.elklk.cn/cdn/book/chapterList/$bId.html');
  }

  @override
  Future<QNTocModel> queryBookToc(QNBookModel model) async {
    final tocUrl = model.tocUrl;
    var networkData = await BUtils.qn_Network(tocUrl);
    var dataList = BUtils.qn_queryList(
        withRule: r'$.result.list..list[*]', node: networkData);
    final List<QNTocInnerModel> innerList = [];
    var bId = BUtils.qn_Get(model.bookId, 'bId');
    for (var s in dataList) {
      var map = json.decode(s);
      var id = "${map['id']}";
      final url = 'https://novel-api.elklk.cn/cdn/book/content/$bId/$id.html';
      var name = "${map['name']}";
      if (name.startsWith('{{{}}}')) {
        var secret = name.replaceFirst('{{{}}}', '');
        name = BUtils.qn_UtilsSync(
            'DESede', [secret, 'OW84U8Eerdb99rtsTXWSILDO', 'SK8bncVu']);
      }
      innerList
          .add(QNTocInnerModel.init(title: name, url: url, needVip: false));
    }
    return QNTocModel.init(
        bookId: model.bookId,
        updateTime: DateTime.now().millisecondsSinceEpoch,
        dataList: innerList);
  }

  @override
  Future<QNContentModel> queryBookContent(
      QNBookModel model, String contentKey) async {
    var networkData = await BUtils.qn_Network(contentKey);
    var content =
        BUtils.qn_querySingle(withRule: '\$..content', node: networkData);
    final indexOf = content.indexOf('{{{}}}');
    if (indexOf > 0) {
      content = content.substring(indexOf + 6);
      content = BUtils.qn_UtilsSync(
          'DESede', [content, 'OW84U8Eerdb99rtsTXWSILDO', 'SK8bncVu']);
    }
    {
      // 去广告
      content = content.replaceAll('看小说就用9x.com', '');
      content = content.replaceAll('天.*籁小说', '');
      content = content.replaceAll('上一章', '');
      content = content.replaceAll('下一章', '');
      content = content.replaceAll('.*更。新快.*', '');
    }
    return QNContentModel.init(contentKey: contentKey, bookContent: content);
  }

  @override
  Map<String, String> verifyBookConfig() => {
        "searchKey": '圣墟',
        "bookDetailPrefix": 'https://novel-api.elklk.cn/api/book-info',
      };
}
""";


const String example_3 = r"""
import 'dart:async';

import 'package:qn_read_rule/book_provide/base.dart';
import 'package:qn_read_rule/book_provide/base_model.dart';

class BookSource_69 extends BookSourceProvide {
  @override
  String siteId() => "https://www.69shu.com";

  @override
  int currentSiteVersion() => 120;

  @override
  Map<int, String> siteUpdateInfo() => {
        80: '初始化版本',
        100: '修改正文正则',
        110: '网站变更，旧数据无法兼容，需进行迁移',
        currentSiteVersion(): '站点信息无变更，去除正文内容的广告'
      };

  @override
  QNBaseModel info() => QNBaseModel.init(

      /// 站点ID ，一般为站点网站
      siteId: siteId(),

      /// 类型： book 书籍  listenbook听书  comic漫画
      type: 'book',

      /// 站点显示名称
      showName: '⚡ 69书吧',

      /// 站点分类
      group: '搜狗',

      /// 站点描述
      desc: '69书吧',

      /// 漫画详情章节展示，仅漫画有效，分为 small 小，middle 中等  big大
      comicShowType: 'small',

      enable: true,

      /// 站点更新时间戳
      updateTime: DateTime.now().millisecondsSinceEpoch,

      /// 站点版本
      versionNumb: currentSiteVersion(),
      vpnWebsite: 'https://www.69shu.com',

      /// 是否支持 探索
      supportExplore: false,

      /// 是否支持 书名搜索
      supportSearchBookName: true,
    isEncrypt: false,);

  @override
  int migrate(int oldVersion) {
    /// 从 110版本，进行了网站地址变更，低于此版本的书籍数据均不可用，需要进行数据迁移
    ///
    /// 数据迁移由 App进行处理，站点规则 需确认是否进行旧数据处理及迁移。
    ///
    ///
    /// 返回值为  0：不需要进行数据迁移，适用于 逻辑优化、去广告等
    /// 返回值为  1：代表 书籍详情页地址发生变更，当用户 在书架打开书籍时，会 将旧书籍删除，使用新的规则 进行 搜索 -> 获取详情 -> 获取目录 -> 获取章节列表，重新生成完整的书籍数据。
    /// 返回值为  2：代表 书籍的目录集合页地址发生变更，当用户 在书架打开书籍时，会通过 新的规则 ->获取目录 -> 获取章节列表，重新生成书籍数据。
    /// 返回值为  3：代表 书籍的目录章节地址（正文地址）发生变更，当用户打开书籍时，会通过 新的规则 -> 获取章节列表，重新生成书籍数据。

    return 0;
  }

  /// 探索 模型 `

  @override
  QNExploreModel exploreModel() {
    return QNExploreModel.empty;
  }

  @override
  Future<List<QNBookModel>> queryExplore(String exploreUrl) async {
    return [];
  }

  @override
  Future<List<QNBookModel>> searchBookName(String key) async {
    var requestPath = 'https://www.69shu.com/modules/article/search.php';
    // 获取网络数据
    var gbkKey = BUtils.qn_UtilsSync('gbk', [key]);
    var formRequestData = 'searchkey=$gbkKey&searchtype=all';
    var result = await BUtils.qn_Network(requestPath,
        method: 'post',
        requestData: formRequestData,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        isWebViewRequest: false,
        responseCharset: 'gbk');
    // xPath解析
    final list = BUtils.qn_queryList(
        withRule: "//div[@class='newbox']/ul/li", node: result);
    final List<QNBookModel> bookList = [];
    for (var node in list) {
      var href = BUtils.qn_querySingle(withRule: "//h3/a[1]/@href", node: node);
      final detailUrl =
          href.replaceFirst('/txt/', '/').replaceFirst('.htm', '/');
      QNBookModel bookModel = QNBookModel.init(
          siteId: siteId(),
          bookName: BUtils.qn_querySingle(
              withRule: "//div[@class='newnav']/h3/text()", node: node),
          siteVersion: currentSiteVersion(),
          encode: '',
          author: BUtils.qn_querySingle(
              withRule: "//div[@class='labelbox']/label[1]/text()", node: node),
          tags: [
            BUtils.qn_querySingle(
                withRule: "//div[@class='labelbox']/label[2]/text()",
                node: node)
          ],
          desc: BUtils.qn_querySingle(
              withRule: "//ol[@class='ellipsis_2']/text()", node: node),
          cover: BUtils.qn_querySingle(withRule: "//img/@src", node: node),
          detailUrl: detailUrl,
          tocUrl: detailUrl);
      bookList.add(bookModel);
    }
    return bookList;
  }

  @override
  Future<QNBookModel> queryBookDetail(QNBookModel model) async {
    return model;
  }

  @override
  Future<QNTocModel> queryBookToc(QNBookModel model) async {
    final tocUrl = model.tocUrl;
    if (tocUrl.trimLeft().trimRight().isEmpty) {
      return QNTocModel.empty;
    }
    var networkData = await BUtils.qn_Network(tocUrl,
        isWebViewRequest: true, responseCharset: 'gbk');
    final dataList = BUtils.qn_queryList(
        withRule: "//div[@id='catalog']/ul/li", node: networkData);
    final List<QNTocInnerModel> innerList = [];
    for (var data in dataList) {
      innerList.add(QNTocInnerModel.init(
          title: BUtils.qn_querySingle(withRule: "/a/text()", node: data),
          url: BUtils.qn_querySingle(withRule: "/a/@href", node: data),
          needVip: false));
    }
    return QNTocModel.init(
        bookId: model.bookId,
        updateTime: DateTime.now().millisecondsSinceEpoch,
        dataList: innerList);
  }

  @override
  Future<QNContentModel> queryBookContent(
      QNBookModel model, String contentKey) async {
    var networkData =
        await BUtils.qn_Network(contentKey, responseCharset: 'gbk');
    var content = BUtils.qn_querySingle(
        withRule: "//div[@class='txtnav']/text()", node: networkData);
    {
      // 去广告
      content = content.replaceAll('loadAdv.*', '\n');
      content = content.replaceAll('第[0-9]+章.*', '\n');
      content = content.replaceAll('[0-9]{4}-[0-9]{2}-[0-9]{2} 作者：.*', '\n');
    }
    return QNContentModel.init(contentKey: contentKey, bookContent: content);
  }

  @override
  Map<String, String> verifyBookConfig() => {
        "searchKey": '元尊',
        "bookDetailPrefix": 'https://www.69shu.com',
      };
}
""";

const String example_4 = r"""

import 'dart:async';
import 'dart:convert';

import 'package:qn_read_rule/book_provide/base.dart';
import 'package:qn_read_rule/book_provide/base_model.dart';


///  书源 白鹤
class BookSource_Baihe extends BookSourceProvide {

  /// 站点信息
  @override
  String siteId() => "https://apk-lb-play.fodexin.com";

  /// 当前书源版本
  @override
  int currentSiteVersion() => 120;

  @override
  Map<int, String> siteUpdateInfo() => {
    80: '初始化版本',
    100: '修改正文正则',
    110: '网站变更，旧数据无法兼容，需进行迁移',
    currentSiteVersion(): '站点信息无变更，去除正文内容的广告'
  };


  /// 配置信息
  @override
  QNBaseModel info() => QNBaseModel.init(

    /// 站点ID ，一般为站点网站
      siteId: siteId(),

      /// 类型： book 书籍  listenbook听书  comic漫画
      type: 'listenbook',

      /// 站点显示名称
      showName: '🍉白鹤故事(推荐)',

      /// 站点分类
      group: '搜狗',

      /// 站点描述
      desc: '🍉白鹤故事',

      /// 漫画详情章节展示，仅漫画有效，分为 small 小，middle 中等  big大
      comicShowType: '',

      /// 站点作者
      maker: 'laobai',

      /// 站点更新时间戳
      updateTime: DateTime.now().millisecondsSinceEpoch,

      /// 站点版本
      versionNumb: currentSiteVersion(),

      vpnWebsite: '',

      /// 是否支持 探索
      supportExplore: true,

      /// 是否支持 书名搜索
      supportSearchBookName: true);


  /// 数据迁移模式
  @override
  int migrate(int oldVersion) {
    /// 从 110版本，进行了网站地址变更，低于此版本的书籍数据均不可用，需要进行数据迁移
    ///
    /// 数据迁移由 App进行处理，站点规则 需确认是否进行旧数据处理及迁移。
    ///
    ///
    /// 返回值为  0：不需要进行数据迁移，适用于 逻辑优化、去广告等
    /// 返回值为  1：代表 书籍详情页地址发生变更，当用户 在书架打开书籍时，会 将旧书籍删除，使用新的规则 进行 搜索 -> 获取详情 -> 获取目录 -> 获取章节列表，重新生成完整的书籍数据。
    /// 返回值为  2：代表 书籍的目录集合页地址发生变更，当用户 在书架打开书籍时，会通过 新的规则 ->获取目录 -> 获取章节列表，重新生成书籍数据。
    /// 返回值为  3：代表 书籍的目录章节地址（正文地址）发生变更，当用户打开书籍时，会通过 新的规则 -> 获取章节列表，重新生成书籍数据。

    return 0;
  }

  /// 探索 模型 ，区分 男女生
  @override
  QNExploreModel exploreModel() {
    const tag =
        "玄幻奇幻::https://apk-lb-json.fodexin.com/json/v1/cat_list/46/index/{{add(page,1)}}.json\n武侠小说::https://apk-lb-json.fodexin.com/json/v1/cat_list/11/index/{{add(page,1)}}.json\n言情通俗::https://apk-lb-json.fodexin.com/json/v1/cat_list/19/index/{{add(page,1)}}.json\n恐怖惊悚::https://apk-lb-json.fodexin.com/json/v1/cat_list/14/index/{{add(page,1)}}.json\n历史军事::https://apk-lb-json.fodexin.com/json/v1/cat_list/15/index/{{add(page,1)}}.json\n官场商战::https://apk-lb-json.fodexin.com/json/v1/cat_list/17/index/{{add(page,1)}}.json\n有声文学::https://apk-lb-json.fodexin.com/json/v1/cat_list/10/index/{{add(page,1)}}.json\n人物纪实::https://apk-lb-json.fodexin.com/json/v1/cat_list/18/index/{{add(page,1)}}.json\n刑侦反腐::https://apk-lb-json.fodexin.com/json/v1/cat_list/16/index/{{add(page,1)}}.json\n百家讲坛::https://apk-lb-json.fodexin.com/json/v1/cat_list/9/index/{{add(page,1)}}.json\n单田芳::https://apk-lb-json.fodexin.com/json/v1/cat_list/1/index/{{add(page,1)}}.json\n刘兰芳::https://apk-lb-json.fodexin.com/json/v1/cat_list/2/index/{{add(page,1)}}.json\n田连元::https://apk-lb-json.fodexin.com/json/v1/cat_list/3/index/{{add(page,1)}}.json\n袁阔成::https://apk-lb-json.fodexin.com/json/v1/cat_list/4/index/{{add(page,1)}}.json\n连丽如::https://apk-lb-json.fodexin.com/json/v1/cat_list/5/index/{{add(page,1)}}.json\n孙一::https://apk-lb-json.fodexin.com/json/v1/cat_list/8/index/{{add(page,1)}}.json\n王子封臣::https://apk-lb-json.fodexin.com/json/v1/cat_list/30/index/{{add(page,1)}}.json\n马长辉::https://apk-lb-json.fodexin.com/json/v1/cat_list/25/index/{{add(page,1)}}.json\n昊儒书场::https://apk-lb-json.fodexin.com/json/v1/cat_list/26/index/{{add(page,1)}}.json\n王军::https://apk-lb-json.fodexin.com/json/v1/cat_list/27/index/{{add(page,1)}}.json\n王玥波::https://apk-lb-json.fodexin.com/json/v1/cat_list/28/index/{{add(page,1)}}.json\n石连军::https://apk-lb-json.fodexin.com/json/v1/cat_list/29/index/{{add(page,1)}}.json\n粤语评书::https://apk-lb-json.fodexin.com/json/v1/cat_list/12/index/{{add(page,1)}}.json\n关永超::https://apk-lb-json.fodexin.com/json/v1/cat_list/35/index/{{add(page,1)}}.json\n张少佐::https://apk-lb-json.fodexin.com/json/v1/cat_list/6/index/{{add(page,1)}}.json\n田战义::https://apk-lb-json.fodexin.com/json/v1/cat_list/7/index/{{add(page,1)}}.json\n其他评书::https://apk-lb-json.fodexin.com/json/v1/cat_list/13/index/{{add(page,1)}}.json\n童话寓言::https://apk-lb-json.fodexin.com/json/v1/cat_list/20/index/{{add(page,1)}}.json\n教育培训::https://apk-lb-json.fodexin.com/json/v1/cat_list/44/index/{{add(page,1)}}.json\n亲子教育::https://apk-lb-json.fodexin.com/json/v1/cat_list/43/index/{{add(page,1)}}.json\n商业财经::https://apk-lb-json.fodexin.com/json/v1/cat_list/42/index/{{add(page,1)}}.json\n脱口秀::https://apk-lb-json.fodexin.com/json/v1/cat_list/41/index/{{add(page,1)}}.json\n戏曲::https://apk-lb-json.fodexin.com/json/v1/cat_list/38/index/{{add(page,1)}}.json\n头条::https://apk-lb-json.fodexin.com/json/v1/cat_list/40/index/{{add(page,1)}}.json\n综艺娱乐::https://apk-lb-json.fodexin.com/json/v1/cat_list/34/index/{{add(page,1)}}.json\n健康养生::https://apk-lb-json.fodexin.com/json/v1/cat_list/33/index/{{add(page,1)}}.json\n二人转::https://apk-lb-json.fodexin.com/json/v1/cat_list/31/index/{{add(page,1)}}.json\n广播剧::https://apk-lb-json.fodexin.com/json/v1/cat_list/36/index/{{add(page,1)}}.json\n轻音清心::https://apk-lb-json.fodexin.com/json/v1/cat_list/23/index/{{add(page,1)}}.json\n英文读物::https://apk-lb-json.fodexin.com/json/v1/cat_list/22/index/{{add(page,1)}}.json\n相声小品::https://apk-lb-json.fodexin.com/json/v1/cat_list/21/index/{{add(page,1)}}.json\n时尚生活::https://apk-lb-json.fodexin.com/json/v1/cat_list/45/index/{{add(page,1)}}.json";
    final List<QNExploreItem> boyExploreList = [];
    final List<QNExploreItem> girlExploreList = [];
    var split = tag.split('\n');
    int length = split.length;
    for (var i = 0; i < length; i++) {
      String s = split[i];
      var _array = s.split("::");
      final String tag = _array[0];
      final String url = _array[1];
      if (s.isNotEmpty) {
        if (tag.startsWith('女')) {
          girlExploreList.add(QNExploreItem(tag: tag, url: url));
        } else {
          boyExploreList.add(QNExploreItem(tag: tag, url: url));
        }
      }
    }
    return QNExploreModel.init(
        boyExploreList: boyExploreList, girlExploreList: girlExploreList);
  }

  /// 探索 获取详情
  @override
  Future<List<QNBookModel>> queryExplore(String exploreUrl) async {
    var networkData = await BUtils.qn_Network(exploreUrl, headers: {
      'User-Agent':
      'laobai_tv/1.1.3(Mozilla/5.0 (Linux; Android 10; Redmi Note 8 Build/PKQ1.190616.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.186 Mobile Safari/537.36)',
      'Referer': "https://apk.jqkan.com/",
    });
    var dataList =
    BUtils.qn_queryList(withRule: r'$.data.books[*]', node: networkData);
    List<QNBookModel> bookList = [];
    for (var s in dataList) {
      final data = json.decode(s);
      final bId = "${data['book_id']}";
      final detailUrl =
          "https://apk-lb-json.fodexin.com/json/v1/cont/$bId.json";
      var bookModel = QNBookModel.init(
          siteId: siteId(),
          bookName: data['name'],
          siteVersion: currentSiteVersion(),
          encode: '',
          author: data['teller'],
          tags: ["${data['type']}"],
          desc: data['synopsis'],
          cover: 'https://pic.iiszg.com/${data['pic']}',
          detailUrl: detailUrl,
          tocUrl: detailUrl);
      BUtils.qn_Put(bookModel.bookId, 'bId', bId);
      bookList.add(bookModel);
    }
    return bookList;
  }


  /// 通过书名搜索书籍
  @override
  Future<List<QNBookModel>> searchBookName(String key) async {
    var requestPath =
        'https://apk-lb-play.fodexin.com/api2/web/index.php/?r=api';
    // 获取网络数据
    var encodeSearchKey = '';
    {
      var ti = DateTime.now().millisecondsSinceEpoch * 1.0 / 1000;
      var me = ti % 60;
      final _5 = BUtils.roundToInt(ti - me);
      var md_5 = '7d0526fa291e8baa4173afcf8e08acea1449682949$_5';
      final t = BUtils.qn_UtilsSync('md5', [md_5]);
      final str = '{"m":"search","t":"$t","aid":"0","pid":"0","key":"$key"}';
      List<int> encryptData =
      BUtils.qn_UtilsSync('rsaEncrypt', [str, jmkey, 'PKCS1']);
      final String encodeStr = laobaiEncode(encryptData);
      encodeSearchKey = BUtils.qn_UtilsSync('urlEncode', [encodeStr]);
    }
    var formRequestData = 'params=$encodeSearchKey&version=1.1.3';
    var result = await BUtils.qn_Network(
      requestPath,
      method: 'post',
      requestData: formRequestData,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'User-Agent':
        'laobai_tv/1.1.3(Mozilla/5.0 (Linux; Android 10; Redmi Note 8 Build/PKQ1.190616.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.186 Mobile Safari/537.36)',
        'Referer': "https://apk.jqkan.com/",
      },
    );
    // xPath解析
    final list = BUtils.qn_queryList(withRule: r"$..books[*]", node: result);
    final List<QNBookModel> bookList = [];
    for (var s in list) {
      final data = json.decode(s);
      final bId = "${data['book_id']}";
      final detailUrl =
          "https://apk-lb-json.fodexin.com/json/v1/cont/$bId.json";
      var bookModel = QNBookModel.init(
          siteId: siteId(),
          bookName: data['name'],
          siteVersion: currentSiteVersion(),
          encode: '',
          author: data['teller'],
          tags: ["${data['type']}"],
          desc: data['synopsis'],
          cover: 'https://pic.iiszg.com/${data['pic']}',
          detailUrl: detailUrl,
          tocUrl: detailUrl);
      BUtils.qn_Put(bookModel.bookId, 'bId', bId);
      bookList.add(bookModel);
    }
    return bookList;
  }

  /// 获取详情页
  @override
  Future<QNBookModel> queryBookDetail(QNBookModel model) async {
    return model;
  }

  /// 获取章节目录
  @override
  Future<QNTocModel> queryBookToc(QNBookModel model) async {
    final tocUrl = model.tocUrl;
    if (tocUrl.trimLeft().trimRight().isEmpty) {
      return QNTocModel.empty;
    }
    var networkData = await BUtils.qn_Network(tocUrl, headers: {
      'User-Agent':
      'laobai_tv/1.1.3(Mozilla/5.0 (Linux; Android 10; Redmi Note 8 Build/PKQ1.190616.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.186 Mobile Safari/537.36)',
      'Referer': "https://apk.jqkan.com/",
    });
    final dataList = BUtils.qn_queryList(
        withRule: "\$.data.play_data[*]", node: networkData);
    final List<QNTocInnerModel> innerList = [];
    // var bId = qnGet(model.bookId, 'bId');
    for (var s in dataList) {
      final data = json.decode(s);
      var playId = data['play_id'];
      final url =
          'https://apk-lb-play.fodexin.com/api2/web/index.php/?r=api,,,$playId';
      innerList.add(
          QNTocInnerModel.init(title: data['name'], url: url, needVip: false));
    }
    return QNTocModel.init(
        bookId: model.bookId,
        updateTime: DateTime.now().millisecondsSinceEpoch,
        dataList: innerList);
  }


  /// 获取正文内容（音乐、漫画）
  @override
  Future<QNContentModel> queryBookContent(
      QNBookModel model, String contentKey) async {
    String bId = BUtils.qn_Get(model.bookId, 'bId');
    var split = contentKey.split(',,,');
    String requestData = _laobai(split[1], bId );
    var networkData = await BUtils.qn_Network(
      split[0],
      method: 'POST',
      requestData: requestData,
      headers: {
        "Referer": 'https://apk-lb.iiszg.com/',
        'User-Agent':
        'laobai_tv/1.1.3(Mozilla/5.0 (Linux; Android 10; Redmi Note 8 Build/PKQ1.190616.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.186 Mobile Safari/537.36)',
        'content-type': 'application/x-www-form-urlencoded'
      },
    );
    var content =
    BUtils.qn_querySingle(withRule: "\$.data.url", node: networkData);
    return QNContentModel.init(
        contentKey: contentKey,
        musicContent: MusicContentModel.init(url: content, headers: {
          'Referer': 'https://apk-lb.iiszg.com/',
          'User-Agent':
          'laobai_tv/1.1.3(Mozilla/5.0 (Linux; Android 10; Redmi Note 8 Build/PKQ1.190616.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.186 Mobile Safari/537.36)'
        }));
  }

  /// --------------------  以下为 站点校验  -------------------------
  ///
  ///

  /// 站点校验配置
  @override
  Map<String, String> verifyBookConfig() => {
    "exploreUrl":
    'https://apk-lb-json.fodexin.com/json/v1/cat_list/46/index/1.json',
    "searchKey": '元尊',
    "bookDetailPrefix":
    'https://apk-lb-json.fodexin.com/json/v1/cont',
    "detailUrl": 'https://apk-lb-json.fodexin.com/json/v1/cont/39546.json',
    "tocUrl": 'https://apk-lb-json.fodexin.com/json/v1/cont/39546.json',
    "contentKey":
    'https://apk-lb-play.fodexin.com/api2/web/index.php/?r=api,,,1',
    "injectArgs": 'bId=39546'
  };


  // -------------
  static String laobaiEncode(List<int> bArr) {
    var bArr2 = BUtils.qn_UtilsSync('utf8_bytes',
        ["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"]);
    final length = bArr.length - (bArr.length % 3);
    List<int> bArr3 = [];
    for(int i = 0 ;i < length * 3; i++ ){
      bArr3.add(-1234);
    }
    var i = 0;
    var i2 = 0;
    while (i < length) {
      var i3 = i + 1;
      var b2 = bArr[i];
      var i4 = i3 + 1;
      var b3 = bArr[i3];
      i = i4 + 1;
      var b4 = bArr[i4];
      var i5 = i2 + 1;
      bArr3[i2] = bArr2[(b2 & 255) >> 2];
      var i6 = i5 + 1;
      bArr3[i5] = bArr2[((b2 & 3) << 4) | ((b3 & 255) >> 4)];
      var i7 = i6 + 1;
      // LogUtil.debug(() => 'i6 ->$i6  i ->$i');
      bArr3[i6] = bArr2[((b3 & 15) << 2) | ((b4 & 255) >> 6)];
      i2 = i7 + 1;
      bArr3[i7] = bArr2[b4 & 63];
      // LogUtil.debug(() => 'i7 ->$i7  i ->$i');
    }
    var length2 = bArr.length - length;
    if (length2 == 1) {
      var b5 = bArr[i];
      var i8 = i2 + 1;
      bArr3[i2] = bArr2[(b5 & 255) >> 2];
      var i9 = i8 + 1;
      bArr3[i8] = bArr2[(b5 & 3) << 4];
      var b6 = 61;
      bArr3[i9] = b6;
      bArr3[i9 + 1] = b6;
    } else if (length2 == 2) {
      var i10 = i + 1;
      var b7 = bArr[i];
      var b8 = bArr[i10];
      var i11 = i2 + 1;
      bArr3[i2] = bArr2[(b7 & 255) >> 2];
      var i12 = i11 + 1;
      bArr3[i11] = bArr2[((b7 & 3) << 4) | ((b8 & 255) >> 4)];
      bArr3[i12] = bArr2[(b8 & 15) << 2];
      bArr3[i12 + 1] = 61;
    }
    final List<int> newList = [];
    for (int x in bArr3) {
      if (x == -1234) {
      }else {
        newList.add(x);
      }
    }
    var decodeStr = BUtils.qn_UtilsSync("utf8_decode",[newList]);
    return decodeStr;
  }

  static final jmkey = [
    48,
    -126,
    2,
    34,
    48,
    13,
    6,
    9,
    42,
    -122,
    72,
    -122,
    -9,
    13,
    1,
    1,
    1,
    5,
    0,
    3,
    -126,
    2,
    15,
    0,
    48,
    -126,
    2,
    10,
    2,
    -126,
    2,
    1,
    0,
    -40,
    104,
    121,
    -84,
    -28,
    -99,
    -13,
    -100,
    21,
    12,
    95,
    14,
    1,
    110,
    44,
    31,
    75,
    -116,
    107,
    42,
    -39,
    52,
    59,
    -91,
    -80,
    -15,
    28,
    102,
    -4,
    -4,
    -91,
    -98,
    -92,
    -80,
    -37,
    -63,
    68,
    35,
    45,
    -42,
    75,
    52,
    -54,
    61,
    40,
    -67,
    -36,
    79,
    -56,
    107,
    2,
    34,
    -20,
    67,
    123,
    -126,
    -107,
    37,
    46,
    -3,
    51,
    64,
    -17,
    64,
    -126,
    -98,
    -45,
    -51,
    28,
    -12,
    78,
    -87,
    38,
    -62,
    87,
    -55,
    -102,
    27,
    -27,
    -111,
    1,
    75,
    86,
    30,
    -20,
    -5,
    -44,
    -17,
    -87,
    -32,
    -107,
    42,
    77,
    116,
    -71,
    -124,
    -109,
    -117,
    -28,
    -56,
    -107,
    59,
    -106,
    -56,
    118,
    82,
    -76,
    -128,
    56,
    75,
    15,
    15,
    -9,
    -15,
    -101,
    53,
    -47,
    -117,
    -71,
    -112,
    -43,
    -100,
    61,
    67,
    -113,
    49,
    -82,
    87,
    104,
    -34,
    -74,
    15,
    -24,
    -113,
    37,
    109,
    -90,
    -115,
    52,
    6,
    -95,
    23,
    111,
    125,
    -106,
    42,
    -122,
    74,
    -15,
    71,
    -23,
    -116,
    32,
    25,
    83,
    -19,
    8,
    72,
    99,
    -54,
    47,
    -112,
    22,
    -71,
    76,
    94,
    122,
    -77,
    99,
    46,
    -74,
    -118,
    -100,
    106,
    58,
    -63,
    -106,
    30,
    23,
    -84,
    90,
    23,
    -45,
    -85,
    124,
    -56,
    8,
    -101,
    42,
    -33,
    90,
    -32,
    29,
    -70,
    -103,
    93,
    28,
    1,
    -45,
    -81,
    -98,
    68,
    69,
    -107,
    -75,
    92,
    -95,
    -123,
    11,
    29,
    76,
    -103,
    -15,
    -23,
    -4,
    -16,
    107,
    122,
    52,
    6,
    50,
    -29,
    52,
    -25,
    -123,
    -33,
    59,
    -78,
    94,
    49,
    -2,
    70,
    18,
    85,
    37,
    99,
    21,
    -103,
    -12,
    97,
    -11,
    61,
    -28,
    37,
    100,
    33,
    119,
    51,
    94,
    -113,
    127,
    5,
    -49,
    -47,
    -60,
    17,
    122,
    -23,
    117,
    17,
    -100,
    -72,
    54,
    -37,
    16,
    -116,
    -99,
    35,
    -122,
    -86,
    85,
    87,
    -58,
    107,
    -13,
    -61,
    -93,
    -78,
    -88,
    113,
    14,
    -2,
    -66,
    52,
    105,
    -44,
    24,
    43,
    79,
    -114,
    -19,
    73,
    -97,
    -10,
    -20,
    -48,
    90,
    18,
    -59,
    -2,
    -20,
    89,
    2,
    115,
    45,
    -11,
    -47,
    -13,
    69,
    -122,
    -2,
    -7,
    -30,
    -91,
    14,
    -38,
    -29,
    62,
    41,
    -103,
    107,
    127,
    -77,
    -59,
    -83,
    20,
    118,
    -17,
    123,
    -10,
    -119,
    123,
    32,
    79,
    104,
    -75,
    -112,
    -11,
    -88,
    92,
    0,
    -57,
    116,
    -95,
    -26,
    96,
    115,
    84,
    43,
    -90,
    -84,
    124,
    -104,
    121,
    -116,
    -122,
    106,
    24,
    112,
    -19,
    120,
    49,
    -26,
    -11,
    66,
    17,
    60,
    -24,
    69,
    -122,
    -113,
    -45,
    9,
    58,
    -83,
    -14,
    57,
    72,
    -67,
    115,
    -34,
    -48,
    32,
    -34,
    53,
    31,
    -59,
    -102,
    -102,
    -49,
    100,
    9,
    18,
    -61,
    -35,
    69,
    18,
    -71,
    -97,
    -22,
    77,
    -109,
    22,
    -9,
    -75,
    -73,
    104,
    -55,
    77,
    37,
    16,
    63,
    -101,
    83,
    -59,
    46,
    93,
    121,
    -124,
    59,
    120,
    34,
    -10,
    45,
    -92,
    24,
    -78,
    98,
    -3,
    93,
    -82,
    15,
    -5,
    53,
    -17,
    -17,
    127,
    89,
    -76,
    -19,
    53,
    62,
    112,
    95,
    102,
    114,
    -82,
    37,
    -68,
    -56,
    -40,
    -25,
    -22,
    -108,
    41,
    101,
    -94,
    67,
    110,
    10,
    -119,
    115,
    -85,
    -36,
    -103,
    0,
    14,
    105,
    -60,
    37,
    -127,
    -107,
    -2,
    125,
    -3,
    125,
    -113,
    -61,
    -25,
    -43,
    -8,
    79,
    13,
    101,
    -103,
    32,
    -54,
    -68,
    121,
    16,
    -79,
    113,
    -118,
    -100,
    39,
    -3,
    -90,
    -24,
    -51,
    -108,
    -78,
    120,
    -109,
    -86,
    -116,
    99,
    0,
    69,
    -72,
    97,
    -65,
    -15,
    2,
    3,
    1,
    0,
    1
  ];



  String _laobai(String pid, String bid) {
    double ti = DateTime.now().millisecondsSinceEpoch * 1.0 / 1000;
    double me = ti % 60;
    final int _5 = BUtils.roundToInt(ti - me);
    var md = BUtils.qn_UtilsSync('md5', ['play$bid$pid']);
    var md_5 = '${md}1449682949$_5';
    final String t = BUtils.qn_UtilsSync('md5', [md_5]);
    final str = '{"m":"play","t":"$t","aid":"$bid","pid":"$pid"}';
    final rsaEncrpyt = BUtils.qn_UtilsSync('rsaEncrypt', [str, jmkey, 'PKCS1']);
    var encodeStrr = laobaiEncode(rsaEncrpyt);
    final bm = BUtils.qn_UtilsSync('urlEncode', [encodeStrr]);
    return 'params=$bm&version=1.1.3';
  }
}


""";


const example_pototo = r"""
import 'dart:async';
import 'dart:convert';

import 'package:qn_read_rule/book_provide/base.dart';
import 'package:qn_read_rule/book_provide/base_model.dart';

class BookSource_Potato extends BookSourceProvide {
  /// 站点信息
  @override
  String siteId() => "https://novel.snssdk.com";

  /// 当前书源版本
  @override
  int currentSiteVersion() => 120;

  @override
  Map<int, String> siteUpdateInfo() => {
        80: '初始化版本',
        100: '修改正文正则',
        110: '网站变更，旧数据无法兼容，需进行迁移',
        currentSiteVersion(): '站点信息无变更，去除正文内容的广告'
      };

  /// 配置信息
  @override
  QNBaseModel info() => QNBaseModel.init(
        siteId: siteId(),
        type: 'listenbook',
        showName: '⚡ 西红柿-男频',
        group: '搜狗',
        desc: '⚡ 西红柿-男频',
        comicShowType: '',
        enable: true,
        updateTime: DateTime.now().millisecondsSinceEpoch,
        versionNumb: currentSiteVersion(),
        vpnWebsite: '',
        supportExplore: true,
        supportSearchBookName: true,
        isEncrypt: true,
      );

  /// 数据迁移模式
  @override
  int migrate(int oldVersion) {
    return 0;
  }

 @override
  QNExploreModel exploreModel() {
    final List<QNExploreItem> boyExploreList = [];
    final List<QNExploreItem> girlExploreList = [];

    final List<List> girlTags = [
      ["无敌", 384],
      ["种田", 23],
      ["萌宝", 28],
      ["美食", 78],
      ["游戏动漫", 57],
      ["娱乐圈", 43],
      ["直播", 69],
      ["女扮男装", 388],
      ["二次元", 39],
      ["团宠", 94],
      ["无cp", 392],
      ["青梅竹马", 387],
      ["病娇", 380],
      ["科幻", 8],
      ["武侠", 16],
      ["年代", 79],
      ["重生", 36],
      ["灵异", 100],
      ["公主", 83],
      ["甜宠", 96],
      ["盗墓", 81],
      ["系统", 19],
      ["女强", 86],
      ["皇后", 84],
      ["推理", 61],
      ["文化历史", 62],
      ["生活", 48],
      ["反派", 369],
      ["末世", 68],
      ["悬疑", 10],
      ["穿越", 37],
      ["学霸", 82],
      ["扮猪吃虎", 93],
      ["清穿", 76],
      ["穿书", 382],
      ["快穿", 24],
      ["皇叔", 87],
      ["空间", 44],
      ["宠妻", 30],
      ["豪门总裁", 29],
      ["影视小说", 45],
      ["成功励志", 56],
      ["职场", 127],
      ["民国", 390],
      ["都市生活", 2],
      ["腹黑", 92],
      ["星际", 77],
      ["天才", 90],
      ["古代言情", 5],
      ["家庭", 125],
      ["兽世", 72],
      ["校园", 4],
      ["诗歌散文", 46],
      ["虐文", 95],
      ["嫡女", 88],
      ["王妃", 85],
      ["精灵", 89],
      ["幻想言情", 32],
      ["现代言情", 3],
      ["现言甜宠", 395],
      ["现言脑洞", 267],
      ["先婚后爱", 265],
      ["都市日常", 261],
      ["古言脑洞", 253],
      ["古言萌宝", 249],
      ["古言甜宠", 394],
      ["现言日常", 269],
      ["现言复仇", 268],
      ["玄幻言情", 248],
      ["医术", 247],
      ["马甲", 266]
    ];
    const String girlBasePrefix =
        'https://tsearch.toutiaoapi.com/2/wap/search/extra/novel_operator?tab_name=%25E5%2585%25A8%25E9%2583%25A8%25E5%2588%2586%25E7%25B1%25BB&ala_src=novel_tag&is_finish=0&iid=2008145514494631&aid=13&app_name=news_article&version_code=692&version_name=6.9.2&abflag=3&partner=novel_tag&limit=10&word_num=0&gender=0';
    for (List tagList in girlTags) {
      final tag = tagList[0];
      for (int i = 0; i < 3; i++) {
        var sort = '0';
        var sortTitle = '推荐';
        if (i == 1) {
          sort = '1';
          sortTitle = '评分';
        } else if (i == 2) {
          sort = '2';
          sortTitle = '热门';
        }

        for (int j = 0; j < 3; j++) {
          var status = 'ALL';
          var statusTitle = '全';
          if (j == 1) {
            sort = 'Finished';
            statusTitle = '完结';
          } else if (j == 2) {
            sort = 'Loading';
            statusTitle = '连载';
          }

          final String title = '༺$tag༻$sortTitle($statusTitle)';
          final String url =
              '$girlBasePrefix&tags=%5B%22${tagList[1]}%22%5D&creation_status=$status&sort=$sort';
          girlExploreList.add(QNExploreItem(tag: title, url: url));
        }
      }
    }

    const String boyBasePrefix =
        'https://tsearch.toutiaoapi.com/2/wap/search/extra/novel_operator?tab_name=%25E5%2585%25A8%25E9%2583%25A8%25E5%2588%2586%25E7%25B1%25BB&ala_src=novel_tag&is_finish=0&iid=2008145514494631&aid=13&app_name=news_article&version_code=692&version_name=6.9.2&abflag=3&partner=novel_tag&limit=10&word_num=0&gender=1';
    final List<List> boyTags = [
      ["玄幻", 7],
      ["神豪", 20],
      ["鉴宝", 17],
      ["三国", 67],
      ["二次元", 39],
      ["历史", 12],
      ["美食", 78],
      ["奶爸", 42],
      ["娱乐圈", 43],
      ["洪荒", 66],
      ["大唐", 73],
      ["外卖", 75],
      ["末世", 68],
      ["都市", 1],
      ["宠物", 74],
      ["学霸", 82],
      ["游戏动漫", 57],
      ["科幻", 8],
      ["体育", 15],
      ["直播", 69],
      ["年代", 79],
      ["文化历史", 62],
      ["诸天万界", 71],
      ["海岛", 40],
      ["神医", 26],
      ["明朝", 126],
      ["武侠", 16],
      ["灵异", 100],
      ["星际", 77],
      ["穿越", 37],
      ["剑道", 80],
      ["都市修真", 124],
      ["赘婿", 25],
      ["盗墓", 81],
      ["推理", 61],
      ["无限流", 70],
      ["种田", 23],
      ["战争", 97],
      ["天才", 90],
      ["职场", 127],
      ["悬疑", 10],
      ["成功励志", 56],
      ["重生", 36],
      ["系统", 19],
      ["空间", 44],
      ["腹黑", 92],
      ["诗歌散文", 46],
      ["家庭", 125],
      ["影视小说", 45],
      ["生活", 48],
      ["都市生活", 2],
      ["扮猪吃虎", 93],
      ["大秦", 377],
      ["无敌", 384],
      ["漫威", 374],
      ["火影", 368],
      ["西游", 373],
      ["龙珠", 376],
      ["聊天群", 381],
      ["海贼", 370],
      ["奥特同人", 367],
      ["特种兵", 375],
      ["反派", 369],
      ["校花", 385],
      ["女帝", 378],
      ["单女主", 389],
      ["神奇宝贝", 371],
      ["九叔", 383],
      ["求生", 379],
      ["无女主", 391],
      ["武魂", 386],
      ["网游", 372],
      ["战神", 27],
      ["都市脑洞", 262],
      ["都市种田", 263],
      ["都市日常", 261],
      ["历史脑洞", 272],
      ["玄幻脑洞", 257],
      ["奇幻仙侠", 259],
      ["都市青春", 396],
      ["传统玄幻", 258],
      ["历史古代", 273]
    ];

    for (List tagList in boyTags) {
      final tag = tagList[0];
      for (int i = 0; i < 3; i++) {
        var sort = '0';
        var sortTitle = '推荐';
        if (i == 1) {
          sort = '1';
          sortTitle = '评分';
        } else if (i == 2) {
          sort = '2';
          sortTitle = '热门';
        }

        for (int j = 0; j < 3; j++) {
          var status = 'ALL';
          var statusTitle = '全';
          if (j == 1) {
            sort = 'Finished';
            statusTitle = '完结';
          } else if (j == 2) {
            sort = 'Loading';
            statusTitle = '连载';
          }

          final String title = '༺$tag༻$sortTitle($statusTitle)';
          final String url =
              '$boyBasePrefix&tags=%5B%22${tagList[1]}%22%5D&creation_status=$status&sort=$sort';
          boyExploreList.add(QNExploreItem(tag: title, url: url));
        }
      }
    }

    return QNExploreModel.init(
        boyExploreList: boyExploreList, girlExploreList: girlExploreList);
  }

   @override
  Future<List<QNBookModel>> queryExplore(String exploreUrl) async {
    var networkData = await BUtils.qn_Network(exploreUrl, headers: {
      'User-Agent':
          'laobai_tv/1.1.3(Mozilla/5.0 (Linux; Android 10; Redmi Note 8 Build/PKQ1.190616.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.186 Mobile Safari/537.36)',
      'Referer': "https://apk.jqkan.com/",
    });
    var dataList =
        BUtils.qn_queryList(withRule: r'$..content[*]', node: networkData);
    List<QNBookModel> bookList = [];
    for (var s in dataList) {
      final data = json.decode(s);
      final bId = "${data['book_id']}";
      final detailUrl =
          "https://novel.snssdk.com/api/novel/book/directory/list/v1?book_id=$bId";
      final tocUrl = 'https://fanqienovel.com/api/reader/directory/detail?bookId=$bId';
      var bookModel = QNBookModel.init(
          siteId: siteId(),
          bookName: data['book_name'],
          siteVersion: currentSiteVersion(),
          encode: '',
          author: data['author'],
          tags: ["${data['category']}"],
          desc: data['abstract'],
          cover: data['thumb_url'],
          // star: double.parse("${data['score']}"),
          detailUrl: detailUrl,
          tocUrl: tocUrl);
      BUtils.qn_Put(bookModel.bookId, 'bId', bId);
      bookList.add(bookModel);
    }
    return bookList;
  }

  /// 通过书名搜索书籍
  @override
  Future<List<QNBookModel>> searchBookName(String key) async {
    var requestPath =
        'https://novel.snssdk.com/api/novel/channel/homepage/search/search/v1/?aid=13&q=$key';
    // 获取网络数据
    var result = await BUtils.qn_Network(
      requestPath,
      method: 'get',
    );
    // xPath解析
    final list = BUtils.qn_queryList(withRule: r"$..ret_data[*]", node: result);
    final List<QNBookModel> bookList = [];
    for (var s in list) {
      final data = json.decode(s);
      final bId = "${data['book_id']}";
      final detailUrl =
          "https://novel.snssdk.com/api/novel/book/directory/list/v1?book_id=$bId";
      final tocUrl = 'https://fanqienovel.com/api/reader/directory/detail?bookId=$bId';
      var bookModel = QNBookModel.init(
          siteId: siteId(),
          bookName: "${data['title']}".replaceAll(r"<em>", '').replaceAll(r'</em>', ''),
          siteVersion: currentSiteVersion(),
          encode: '',
          author: data['author'],
          tags: ["${data['category']}"],
          desc: data['abstract'],
          cover: data['thumb_url'],
          detailUrl: detailUrl,
          tocUrl: tocUrl);
      BUtils.qn_Put(bookModel.bookId, 'bId', bId);
      bookList.add(bookModel);
    }
    return bookList;
  }

  /// 获取详情页
  @override
  Future<QNBookModel> queryBookDetail(QNBookModel model) async {
    return model;
  }

  /// 获取章节目录
  @override
  Future<QNTocModel> queryBookToc(QNBookModel model) async {
    final tocUrl = model.tocUrl;
    if (tocUrl.trimLeft().trimRight().isEmpty) {
      return QNTocModel.empty;
    }
    var networkData = await BUtils.qn_Network(tocUrl,);
    final data1 = json.decode(networkData);
    List<dynamic> _idsList = data1['data']['allItemIds'];
    final List<String> ids = [];
    for(var _id in _idsList){
      ids.add('$_id');
    }
    const int requestSize = 100;

    final List<QNTocInnerModel> innerList = [];
    int count = ids.length;
    int requestCount = 0;
    while(requestCount < count){
      int length = ids.length;
      int _size = 0;
      if(requestCount + requestSize < length){
        _size = requestSize;
      }else {
        _size = length - requestCount;
      }
      int end = requestCount + _size;
      String idJoin = '';
      for(int i = requestCount; i < end; i ++){
        if(i == end ){
         idJoin = '$idJoin${ids[i]}';
        }else {
         idJoin = '$idJoin${ids[i]},';
        }
      }
      var networkData = await BUtils.qn_Network("https://novel.snssdk.com/api/novel/book/directory/detail/v1/?item_ids=$idJoin");
      final data = json.decode(networkData);
      final dataList = data['data'];
      for(var d in dataList){
        final _time = DateTime.now().millisecondsSinceEpoch;
        var itemId = d['item_id'];
        final key = '13$itemId${_time}8bb34026-ee3b-11ed-90d2-acde48001122';
        var md5 = BUtils.qn_UtilsSync('md5', [key]);
        var title = d['title'];
        innerList.add(QNTocInnerModel.init(title: title, url: 'https://novel.snssdk.com/api/novel/book/reader/full/v1/?x-ts=$_time&item_id=$itemId&aid=13&x-s=$md5', needVip: false));
      }
      requestCount = requestCount + _size;
    }
    return QNTocModel.init(
        bookId: model.bookId,
        updateTime: DateTime.now().millisecondsSinceEpoch,
        dataList: innerList);
  }

  /// 获取正文内容（音乐、漫画）
  @override
  Future<QNContentModel> queryBookContent(
      QNBookModel model, String contentKey) async {
    final data = await BUtils.qn_Network(contentKey);
    var content = BUtils.qn_querySingle(withRule: r"$..content", node: data);
    if(content.startsWith("<header>")){
      content =  BUtils.qn_querySingle(withRule: r'//article/p/text()', node: content,join: '\n');
      return  QNContentModel.init(
          contentKey: contentKey,
          bookContent:content);
    }else {
      return QNContentModel.init(
          contentKey: contentKey,
          bookContent: content
      );
    }

  }

  /// --------------------  以下为 站点校验  -------------------------
  ///
  ///

  /// 站点校验配置
  @override
  Map<String, String> verifyBookConfig() => {
        "searchKey": '元尊',
        "bookDetailPrefix": 'https://novel.snssdk.com/api/novel/book/directory/list/v1?book_id',
      };

}
""";