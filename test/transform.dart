import 'dart:collection';
import 'dart:convert';

class TransformUtils {


  static const String ttsJson = r"""
  [
  {
    "id": 10001,
    "name": "百度评书(在线)",
    "queryRule": "{\"queryPath\":\"http://tsn.baidu.com/text2audio\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"_res_tag_=audio&aue=6&cod=2&ctp=1&cuid=baidu_speech_demo&idx=1&lan=zh&pdt=220&per=4114&pit=5&tex={{urlEncode(speakText)}}&vol=10\"},\"request_headers\":{\"content-type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"0\" }"
  },
  {
    "id": 10003,
    "name": "度丫丫(在线)",
    "queryRule": "{\"queryPath\":\"http://tsn.baidu.com/text2audio\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"_res_tag_=audio&aue=6&cod=2&ctp=1&cuid=baidu_speech_demo&idx=1&lan=zh&pdt=220&per=4&pit=5&tex={{urlEncode(speakText)}}&vol=10\"},\"request_headers\":{\"content-type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"0\" }"
  },
  {
    "id": 10004,
    "name": "度小娇(在线)",
    "queryRule": "{\"queryPath\":\"http://tsn.baidu.com/text2audio\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"_res_tag_=audio&aue=6&cod=2&ctp=1&cuid=baidu_speech_demo&idx=1&lan=zh&pdt=220&per=5&pit=5&tex={{urlEncode(speakText)}}&vol=10\"},\"request_headers\":{\"content-type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"0\" }"
  },
  {
    "id": 10006,
    "name": "度逍遥(在线)",
    "queryRule": "{\"queryPath\":\"http://tsn.baidu.com/text2audio\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"_res_tag_=audio&aue=6&cod=2&ctp=1&cuid=baidu_speech_demo&idx=1&lan=zh&pdt=220&per=5003&pit=5&tex={{urlEncode(speakText)}}&vol=10\"},\"request_headers\":{\"content-type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"0\" }"
  },
  {
    "id": 10007,
    "name": "度小朵(在线)",
    "queryRule": "{\"queryPath\":\"http://tsn.baidu.com/text2audio\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"_res_tag_=audio&aue=6&cod=2&ctp=1&cuid=baidu_speech_demo&idx=1&lan=zh&pdt=220&per=4103&pit=5&tex={{urlEncode(speakText)}}&vol=10\"},\"request_headers\":{\"content-type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"0\" }"
  },
  {
    "id": 10008,
    "name": "度博文(在线)",
    "queryRule": "{\"queryPath\":\"http://tsn.baidu.com/text2audio\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"_res_tag_=audio&aue=6&cod=2&ctp=1&cuid=baidu_speech_demo&idx=1&lan=zh&pdt=220&per=106&pit=5&tex={{urlEncode(speakText)}}&vol=10\"},\"request_headers\":{\"content-type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"0\" }"
  },
  {
    "id": 10009,
    "name": "情感女生(在线)",
    "queryRule": "{\"queryPath\":\"http://tsn.baidu.com/text2audio\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"_res_tag_=audio&aue=6&cod=2&ctp=1&cuid=baidu_speech_demo&idx=1&lan=zh&pdt=220&per=4105&pit=5&tex={{urlEncode(speakText)}}&vol=10\"},\"request_headers\":{\"content-type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"0\" }"
  },
  {
    "id": 10010,
    "name": "情感男生(在线)",
    "queryRule": "{\"queryPath\":\"http://tsn.baidu.com/text2audio\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"_res_tag_=audio&aue=6&cod=2&ctp=1&cuid=baidu_speech_demo&idx=1&lan=zh&pdt=220&per=4115&pit=5&tex={{urlEncode(speakText)}}&vol=10\"},\"request_headers\":{\"content-type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"0\" }"
  },
  {
    "id": 20001,
    "name": "微软晓晓(在线)",
    "voiceKey": "zh-CN-XiaoxiaoNeural",
    "queryRule": "{\"queryPath\":\"http://152.136.205.53:3000/api/ra\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"name=zh-CN-XiaoxiaoNeural&text={{speakText}}\"},\"request_headers\":{\"Content-Type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"1\" }"
  },
  {
    "id": 20002,
    "name": "微软蔓蔓-粤语(在线)",
    "voiceKey": "zh-HK-HiuMaanNeural",
    "queryRule": "{\"queryPath\":\"http://152.136.205.53:3000/api/ra\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"name=zh-HK-HiuMaanNeural&text={{speakText}}\"},\"request_headers\":{\"Content-Type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"1\" }"
  },
  {
    "id": 20003,
    "name": "微软万隆-粤语(在线)",
    "voiceKey": "zh-HK-WanLungNeural",
    "queryRule": "{\"queryPath\":\"http://152.136.205.53:3000/api/ra\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"name=zh-HK-WanLungNeural&text={{speakText}}\"},\"request_headers\":{\"Content-Type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"1\" }"
  },
  {
    "id": 20004,
    "name": "微软小依(在线)",
    "voiceKey": "zh-CN-XiaoyiNeural",
    "queryRule": "{\"queryPath\":\"http://152.136.205.53:3000/api/ra\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"name=zh-CN-XiaoyiNeural&text={{speakText}}\"},\"request_headers\":{\"Content-Type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"1\" }"
  },
  {
    "id": 20005,
    "name": "微软云间(在线)",
    "voiceKey": "zh-CN-YunjianNeural",
    "queryRule": "{\"queryPath\":\"http://152.136.205.53:3000/api/ra\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"name=zh-CN-YunjianNeural&text={{speakText}}\"},\"request_headers\":{\"Content-Type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"1\"}"
  },
  {
    "id": 20006,
    "name": "微软云溪(在线)",
    "voiceKey": "zh-CN-YunxiNeural",
    "queryRule": "{\"queryPath\":\"http://152.136.205.53:3000/api/ra\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"name=zh-CN-YunxiNeural&text={{speakText}}\"},\"request_headers\":{\"Content-Type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"1\" }"
  },
  {
    "id": 20007,
    "name": "微软云霞(在线)",
    "voiceKey": "zh-CN-YunxiaNeural",
    "queryRule": "{\"queryPath\":\"http://152.136.205.53:3000/api/ra\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"name=zh-CN-YunxiaNeural&text={{speakText}}\"},\"request_headers\":{\"Content-Type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"1\" }"
  },
  {
    "id": 20009,
    "name": "微软云杨(在线)",
    "voiceKey": "zh-CN-YunyangNeural",
    "queryRule": "{\"queryPath\":\"http://152.136.205.53:3000/api/ra\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"name=zh-CN-YunyangNeural&text={{speakText}}\"},\"request_headers\":{\"Content-Type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"1\" }"
  },
  {
    "id": 20010,
    "name": "辽宁-小北(在线)",
    "voiceKey": "zh-CN-liaoning-XiaobeiNeural",
    "queryRule": "{\"queryPath\":\"http://152.136.205.53:3000/api/ra\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"name=zh-CN-liaoning-XiaobeiNeural&text={{speakText}}\"},\"request_headers\":{\"Content-Type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"1\" }"
  },
  {
    "id": 20011,
    "name": "台湾-黄晓贞(在线)",
    "voiceKey": "zh-TW-HsiaoChenNeural",
    "queryRule": "{\"queryPath\":\"http://152.136.205.53:3000/api/ra\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"name=zh-TW-HsiaoChenNeural&text={{speakText}}\"},\"request_headers\":{\"Content-Type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"1\" }"
  },
  {
    "id": 20012,
    "name": "台湾-云哲(在线)",
    "voiceKey": "zh-TW-YunJheNeural",
    "queryRule": "{\"queryPath\":\"http://152.136.205.53:3000/api/ra\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"name=zh-TW-YunJheNeural&text={{speakText}}\"},\"request_headers\":{\"Content-Type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"1\" }"
  },
  {
    "id": 20013,
    "name": "台湾-黄晓渝(在线)",
    "voiceKey": "zh-TW-HsiaoYuNeural",
    "queryRule": "{\"queryPath\":\"http://152.136.205.53:3000/api/ra\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"name=zh-TW-HsiaoYuNeural&text={{speakText}}\"},\"request_headers\":{\"Content-Type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"1\" }"
  },
  {
    "id": 20014,
    "name": "陕西-小宁(在线)",
    "voiceKey": "zh-CN-shaanxi-XiaoniNeural",
    "queryRule": "{\"queryPath\":\"http://152.136.205.53:3000/api/ra\",\"method\":\"post\",\"request_body\":{\"raw-data\":\"name=zh-CN-shaanxi-XiaoniNeural&text={{speakText}}\"},\"request_headers\":{\"Content-Type\":\"application/x-www-form-urlencoded\"},\"response_charset\":\"tts-bytes\" ,\"oneday_tts\":\"1\" }"
  }
]
  """;

  static const String oldJson = r"""
  {
    "site": "https://mjjxs.net",
    "type": "0",
    "showName": "\uD83C\uDF89 聚合书库(精选)",
    "group": "青鸟阅读",
    "searchGroup": "1",
    "version": "1.0.0",
    "desc": "\uD83C\uDF89 聚合书库(精选)",
    "bookType": "2",
    "requestHeaders": "{}",
    "queryType": "json",
    "disable": "0",
    "verifyContent": "{\"exploreUrl\":\"http://www.22ff.vip/xuanhuan/2/\"}",
    "exploreTagRule": {
    },
    "exploreRule": {
    },
    "searchRule": {
      "verify": "{\"key\":\"东方怪物校园\",\"bookName\":\"东方怪物校园\",\"detailUrlPrefix\":\"mjjxs.net\"}",
      "authorSearch": "",
      "nameSearch": "{\"queryPath\":\"https://mjjxs.net/search/s312t20230309?keyword={{key}}\",\"method\":\"get\",\"request_body\":{},\"request_headers\":{\"User-Agent\": \"Mozilla/5.0 (Linux; Android 13; M2012K11AC Build/TKQ1.220829.002; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/104.0.5112.97 Mobile Safari/537.36\",\"Referer\":\"https://mjjxs.net/chapter/9139558/1.html?3428\"},\"response_charset\":\"utf8\",\"webview\":\"1\" }",
      "listQuery": "//div[@class=\"s_list\"]",
      "nameRule": "/a/text(),@reg:#(.*)：《(.*)》.*#{{$2}}",
      "authorRule": "/a/text(),@reg:#(.*)：《(.*)》.*#{{$1}}",
      "kindRule": "",
      "countRule": "",
      "starRule": "",
      "newChapterRule": "",
      "descRule": "",
      "coverRule": "",
      "detailPageUrlRule": "/a/@href,@reg:#.*#https://mjjxs.net{{$0}}"
    },
    "detailPageRule": {
      "verify": "{\"detailUrl\":\"https://mjjxs.net/117113059.html?t=20230309\",\"tocUrl\":\"https://mjjxs.net/117113059.html?t=20230309\"}",
      "detailQuery": "{\"queryPath\":\"{{detailUrl}}\",\"method\":\"get\",\"request_headers\":{\"User-Agent\": \"Mozilla/5.0 (Linux; Android 13; M2012K11AC Build/TKQ1.220829.002; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/104.0.5112.97 Mobile Safari/537.36\",\"Referer\":\"https://mjjxs.net/chapter/9139558/1.html?3428\"},\"request_body\":{},\"response_charset\":\"utf8\",\"webview\":\"1\" }",
      "nameRule": "",
      "authorRule": "",
      "kindRule": "",
      "countRule": "",
      "starRule": "",
      "newChapterRule": "",
      "descRule": "",
      "coverRule": "",
      "tocRule": "@this"
    },
    "tocRule": {
      "verify": "{\"detailUrl\":\"https://mjjxs.net/117113059.html?t=20230309\",\"tocUrl\":\"https://mjjxs.net/117113059.html?t=20230309\"}",
      "tocQuery": "{\"queryPath\":\"{{tocUrl}}\",\"method\":\"get\",\"request_body\":{},\"request_headers\":{\"User-Agent\": \"Mozilla/5.0 (Linux; Android 13; M2012K11AC Build/TKQ1.220829.002; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/104.0.5112.97 Mobile Safari/537.36\", \"Referer\":\"https://mjjxs.net/chapter/9139558/1.html?3428\"},\"response_charset\":\"utf8\" ,\"webview\":\"1\" }",
      "chapterListQuery": "//ul[@class=\"last9\"]/li",
      "chapterNameRule": "/a/text()",
      "chapterUrlRule": "/a/@href,@reg:#.*#https://mjjxs.net{{$0}}",
      "reverse": "0",
      "updateTimeRule": null,
      "nextChapterUrlRule": null,
      "nextChapterNameRule": null,
      "nextChapterKey": null
    },
    "contentRule": {
      "verify": "{\"contentUrl\":\"https://mjjxs.net/chapter/117113059/1.html?len=3438\"}",
      "contentQuery": "{\"queryPath\":\"{{contentUrl}}\",\"method\":\"get\",\"request_body\":{},\"request_headers\":{\"User-Agent\": \"Mozilla/5.0 (Linux; Android 13; M2012K11AC Build/TKQ1.220829.002; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/104.0.5112.97 Mobile Safari/537.36\",\"Referer\":\"https://mjjxs.net/chapter/9139558/1.html?3428\"},\"response_charset\":\"utf8\" }",
      "webview": "1",
      "contentRule": "//div[@id=\"nr1\"]/p[position()> 3]/text()",
      "contentNextUrlRule": null,
      "contentNextNameRule": null,
      "contentNextKey": null
    }
  }
  """;

  static Map transform(String json) {
    json = json.trim();
    var map = jsonDecode(json);
    final verifyText = map['searchRule']?['verify'] ?? '';
    var verifyMap = {};
    if (verifyText != null && verifyText != '') {
      var decode = jsonDecode(verifyText);
      verifyMap['searchKey'] = decode['key'];
      verifyMap['detailPrefix'] = decode['detailUrlPrefix'];
    }

    String infoType = 'book';
    {
      final bookType = map['bookType'] ?? '2';
      if (bookType == '2') {
        infoType = 'book';
      } else if (bookType == '3') {
        infoType = 'listenbook';
      } else if (bookType == '4') {
        infoType = 'comic';
      }
    }

    var exploreTag = map['exploreTagRule']?['tag'] ?? '';
    if (exploreTag != null && exploreTag != '') {
      exploreTag =
          exploreTag.toString().replaceAll('{{add(page,1)}}', '{{page}}');
    }

    final searchName = map['searchRule']?['nameSearch'];
    var searchNameRule = '';
    if (searchName != null && searchName != '') {
      var decode = jsonDecode(searchName);
      StringBuffer stringBuffer = StringBuffer();
      stringBuffer.write(decode['queryPath']);
      Map<String,Object> map = HashMap();
      if(decode['method'].toString().toLowerCase()  != 'get'){
        map['method'] = decode['method'].toString().toLowerCase();
      }
      var headers = decode['request_headers'];
      if(headers is Map && headers.isNotEmpty){
        map['headers'] = headers;
      }else {
        map['headers'] = {};
      }
      var responseCharset = decode['response_charset'].toString().toLowerCase().trim();
      if(responseCharset == '' || responseCharset == 'utf8' || responseCharset == 'utf-8'){
      }else {
        map['responseCharset'] = responseCharset;
      }
      var requestBody = decode['request_body'];
      if(requestBody is Map && requestBody.isNotEmpty){
        if(requestBody.containsKey('raw-data')){
          map['requestData'] = requestBody['raw-data'].toString().replaceAll('key', '\$.key');
          (map['headers'] as Map)['Content-Type'] = 'application/x-www-form-urlencoded';
        }else {
          map['requestData'] = jsonEncode(requestBody).replaceAll('key', '\$.key');
        }
      }

      if((map['headers'] as Map).isEmpty){
        map.remove('headers');
      }
      if(map.isNotEmpty){
        stringBuffer.write(',');
        stringBuffer.write(jsonEncode(map));
      }
      searchNameRule = stringBuffer.toString().replaceAll('key', '\$.key');
    }

    var mapap = {
      "siteId": map['site'],
      "info": {
        "type": infoType,
        "showName": _HandlerStr(map['showName']),
        "group": "青鸟",
        "desc": _HandlerStr(map['desc']),
        "updateTime": DateTime.now().millisecondsSinceEpoch,
        "versionNumb": 100,
        "supportSearch": true,
        "supportExplore": true,
        "enable": map['disable'] != '1',
        "vpnWebsite": _HandlerStr(map['needVpn'] == '1' ? ' ' : null)
      },
      "exploreUrl": exploreTag,
      "ruleExplore": {
        "bookList": _HandlerStr(map['exploreRule']?['listQuery']),
        "bookName": _HandlerStr(map['exploreRule']?['nameRule']),
        "author": _HandlerStr(map['exploreRule']?['authorRule']),
        "cover": _HandlerStr(map['exploreRule']?['coverRule']),
        "desc": _HandlerStr(map['exploreRule']?['descRule']),
        "tags": _HandlerStr(map['exploreRule']?['kindRule']),
        "newestChapter": _HandlerStr(map['exploreRule']?['newChapterRule']),
        "wordCount": _HandlerStr(map['exploreRule']?['countRule']),
        "star": _HandlerStr(map['exploreRule']?['starRule']),
        "detailUrl": _HandlerStr(map['exploreRule']?['detailPageUrlRule']),
        "tocUrl": "",
      },
      "searchUrl": _HandlerStr(searchNameRule),
      "ruleSearch": {
        "bookList": _HandlerStr(map['searchRule']?['listQuery']),
        "bookName": _HandlerStr(map['searchRule']?['nameRule']),
        "author": _HandlerStr(map['searchRule']?['authorRule']),
        "cover": _HandlerStr(map['searchRule']?['coverRule']),
        "desc": _HandlerStr(map['searchRule']?['descRule']),
        "tags": _HandlerStr(map['searchRule']?['kindRule']),
        "newestChapter": _HandlerStr(map['searchRule']?['newChapterRule']),
        "wordCount": _HandlerStr(map['searchRule']?['countRule']),
        "star": _HandlerStr(map['searchRule']?['starRule']),
        "detailUrl": _HandlerStr(map['searchRule']?['detailPageUrlRule']),
        "tocUrl": "",
      },
      "ruleDetail": {
        "bookName": _HandlerStr(map['detailPageRule']?['nameRule']),
        "author": _HandlerStr(map['detailPageRule']?['authorRule']),
        "cover": _HandlerStr(map['detailPageRule']?['coverRule']),
        "desc": _HandlerStr(map['detailPageRule']?['descRule']),
        "tags": _HandlerStr(map['detailPageRule']?['kindRule']),
        "newestChapter": _HandlerStr(map['detailPageRule']?['newChapterRule']),
        "wordCount": _HandlerStr(map['detailPageRule']?['countRule']),
        "star": _HandlerStr(map['detailPageRule']?['starRule']),
        "detailUrl": null,
        "tocUrl": _HandlerStr(map['detailPageRule']?['tocRule'])
      },
      "ruleToc": {
        "tocList": _HandlerStr(map['tocRule']?['chapterListQuery']),
        "title": _HandlerStr(map['tocRule']?['chapterNameRule']),
        "url": _HandlerStr(map['tocRule']?['chapterUrlRule']),
        "nextTocUrl": _HandlerStr(map['tocRule']?['nextChapterUrlRule'] ??
            map['tocRule']?['nextChapterUrlListRule']),
        "needVip": "0",
        "wordCount": ""
      },
      "ruleContent": {
        "content": _HandlerStr(map['contentRule']?['contentRule']),
        "nextUrls": _HandlerStr(map['contentRule']?['contentNextUrlRule']),
        "nextJoin": ""
      },
      "verify": {
        "searchKey": verifyMap['searchKey'],
        "bookDetailPrefix": verifyMap['detailPrefix']
      }
    };
    return mapap;
  }

  static String? _HandlerStr(dynamic text) {
    if (text == null) {
      return null;
    }
    var string = text.toString();
    if (string.contains(',@reg:')) {
      var split = string.split(",@reg:");
      StringBuffer stringBuffer = StringBuffer();
      stringBuffer.write(split[0]);
      var split2 = split[1].split("&&");
      for (var s in split2) {
        stringBuffer.write(',,,');
        stringBuffer.write(s);
        stringBuffer.write("#");
      }
      return stringBuffer.toString();
    } else {
      return string;
    }
  }


  static void handlerTTS(String text){

    var jsonList = jsonDecode(text.trim());
    for(Map m in jsonList){
      var queryRule = jsonDecode( m['queryRule']);
      // print('queryRule ->$queryRule');
      // print('\n');
      var queryUrl = queryRule['queryPath'];
      var newMap = {};
      newMap['headers'] = queryRule['request_headers'];
      newMap['requestData'] = queryRule['request_body']['raw-data'].toString().replaceFirst("{{speakText}}", '{{\$.speakText}}');
      newMap['method'] = 'post';
      newMap['responseCharset'] = 'bytes';
      m['queryRule'] = queryUrl + ',' + jsonEncode(newMap);
      m['oneday_tts'] = queryRule['oneday_tts'];
    }
    print(JsonEncoder.withIndent('  ').convert(jsonList).replaceAll('{{urlEncode(speakText)}}', '{{QB.urlEncode(\$.speakText)}}'));
  }


}
